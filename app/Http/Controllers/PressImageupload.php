<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PressImages;
use App\press;
use DB;
class PressImageupload extends Controller
{

    public function index(Request $request) {
        return view('dashboard.pages.press');
    }

    public function Pressevents(Request $request)
    {
        $projectupdates = new press();
        $projectupdates->heading = $request->heading;
        $projectupdates->body = $request->body;
        $projectupdates->type = $request->type;
        $projectupdates->save();
       // dd($projectupdates);
        $last_insert_id = $projectupdates->id;
      //  dd($last_insert_id);


        //   $users = DB::table('project_statuses')->where('id',  $last_insert_id)->get();

        //$flights = press::find(['id', $last_insert_id]);
        $users = DB::table('presses')->select('id'=== $last_insert_id)->get();

         dd($users);


      //  return view('dashboard.pages.press-images', ['flights' => $flights]);

    }


    public function store(Request $request)
    {
        $uploadsimages = new PressImages();
        $image = $request->file('file');
        $imageName = time() . '_' . $image->getClientOriginalName();
        $image->move(public_path('project'), $imageName);
        $imgpath = 'project/';
        $imgname = $image->getClientOriginalName();
        $removeimageextinsion = strtok($imgname, '.');
        $finalimageurl = $imgpath . $imageName;
        $replacesextraspaces = str_replace(' ', '_', $finalimageurl);


        //db save
        $uploadsimages->project = $request->project;
        $uploadsimages->parentid = $request->parentid;

        $uploadsimages->date = $request->projectdate;
        $uploadsimages->name = $removeimageextinsion;
        $uploadsimages->url = $replacesextraspaces;
        $uploadsimages->save();

        return response()->json(['success' => $imageName,]);
    }
}
