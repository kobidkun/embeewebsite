<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Illuminate\Support\Facades\Storage;
class CSRActivity extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $a = \App\CSRActivity::all();
        return view('events.csr',['as' => $a]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uploadsimages = new \App\CSRActivity;

        //one

        $image1 =  $request->file('img_1');
        $imageName1 = time() . '_' . $image1->getClientOriginalName();
        $image1->move(public_path('csract'), $imageName1);
        $imgpath1 = 'csract/';
        $finalimageurl1 = $imgpath1 . $imageName1;
        $replacesextraspaces1 = str_replace(' ', '_', $finalimageurl1);


        //oneend
        //
        ///
        ///  //one
        ///


        if($request->hasFile('img_2')) {

            $image2 = $request->file('img_2');
            $imageName2 = time() . '_' . $image2->getClientOriginalName();
            $image2->move(public_path('project'), $imageName2);
            $imgpath2 = 'csr/';
            $finalimageurl2 = $imgpath2 . $imageName2;
            $replacesextraspaces2 = str_replace(' ', '_', $finalimageurl2);
            $uploadsimages->img2 = $replacesextraspaces2;
        }

        //oneend///
        ///  //one
        if($request->hasFile('img_2')) {
            $image3 = $request->file('img_3');
            $imageName3 = time() . '_' . $image3->getClientOriginalName();
            $image3->move(public_path('project'), $imageName3);
            $imgpath3 = 'csr/';
            $finalimageurl3 = $imgpath3 . $imageName3;
            $replacesextraspaces3 = str_replace(' ', '_', $finalimageurl3);
            $uploadsimages->img3 = $replacesextraspaces3;
        }
        //oneend///
        ///  //one
            if($request->hasFile('img_2')) {
                $image4 = $request->file('img_4');
                $imageName4 = time() . '_' . $image4->getClientOriginalName();
                $image4->move(public_path('project'), $imageName4);
                $imgpath4 = 'csr/';
                $finalimageurl4 = $imgpath4 . $imageName4;
                $replacesextraspaces4 = str_replace(' ', '_', $finalimageurl4);
                $uploadsimages->img4 = $replacesextraspaces4;
            }
        //oneend///
        ///  //one
                if($request->hasFile('img_2')) {
                    $image5 = $request->file('img_5');
                    $imageName5 = time() . '_' . $image5->getClientOriginalName();
                    $image5->move(public_path('project'), $imageName5);
                    $imgpath5 = 'csr/';
                    $finalimageurl5 = $imgpath5 . $imageName5;
                    $replacesextraspaces5 = str_replace(' ', '_', $finalimageurl5);
                    $uploadsimages->img5 = $replacesextraspaces5;
                }
        //oneend///
        ///  //one
                    if($request->hasFile('img_2')) {
                        $image6 = $request->file('img_6');
                        $imageName6 = time() . '_' . $image6->getClientOriginalName();
                        $image6->move(public_path('project'), $imageName6);
                        $imgpath6 = 'csr/';
                        $finalimageurl6 = $imgpath6 . $imageName6;
                        $replacesextraspaces6 = str_replace(' ', '_', $finalimageurl6);
                        $uploadsimages->img6 = $replacesextraspaces6;
                    }
        //oneend///







        $uploadsimages->title = $request->title;
        $uploadsimages->desc = $request->desc;
        $uploadsimages->img1 = $replacesextraspaces1;





        $uploadsimages->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
