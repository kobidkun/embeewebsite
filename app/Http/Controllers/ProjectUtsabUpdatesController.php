<?php

namespace App\Http\Controllers;

use App\ProjectUtsabUpdates;
use Illuminate\Http\Request;

class ProjectUtsabUpdatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projectlist = ProjectUtsabUpdates::all();
        //dd($projectlist);
        return view('Projects.Utsab.projectupdates', ['projects' => $projectlist]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.project-updates.utsab');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $projectupdate =  ProjectUtsabUpdates::find(1);
        $projectupdate->date = $request->date;
        $projectupdate->url = $request->url;
        $projectupdate->project = $request->project;

        $projectupdate->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProjectUtsabUpdates  $projectUtsabUpdates
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectUtsabUpdates $projectUtsabUpdates)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectUtsabUpdates  $projectUtsabUpdates
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectUtsabUpdates $projectUtsabUpdates)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectUtsabUpdates  $projectUtsabUpdates
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectUtsabUpdates $projectUtsabUpdates)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectUtsabUpdates  $projectUtsabUpdates
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectUtsabUpdates $projectUtsabUpdates)
    {
        //
    }
}
