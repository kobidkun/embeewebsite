<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Callnow;
use App\contactus;
use App\contactpage;

use Visitor;


class homepagecontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $callnow = Callnow::all();
        $callnow2 = Callnow::all()->take(3);
        $countcall = $callnow->count();


        $contactus = contactus::all();
        $contactus2 = contactus::all()->take(3);
        $contactus_count = $contactus->count();

        $contactpage = contactpage::all();
        $contactpage2 = contactpage::all()->take(3);
        $contactpage_count = $contactpage->count();


        $date_start = $ldate = date('Y-m-d');
        $date_end = date('d.m.Y',strtotime("-1 days"));
       // dd($date_end);

        $visitor =   Visitor::clicks();
      //  $visitor = Tracker::onlineUsers();

       // dd($callnow2);

    // dd($visitor);
$totalcount = $countcall + $contactus_count + $contactpage_count;


    return  view('dashboard.pages.homepage')
        ->with('callnow', $countcall)
        ->with('contactus', $contactus_count)
        ->with('contactpage_count', $contactpage_count)
        ->with('visitor', $visitor)
        ->with('totalcount',  $totalcount)
        ->with('callnow2',  $callnow2)
        ->with('contactus2',  $contactus2)
        ->with('contactpage2',  $contactpage2)
        ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
