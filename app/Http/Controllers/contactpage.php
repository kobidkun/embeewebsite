<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\contactpage as Contact;
use Mail;
class contactpage extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getcontact =  Contact::orderBy('created_at', 'desc')->get();
        //dd($getcontact);
        return view('dashboard.pages.callnow', [ 'contactdatas' => $getcontact]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $savecontact = new Contact();
        $savecontact->name = $request->name;
        $savecontact->email = $request->email;
        $savecontact->mobile = $request->mobile;
        $savecontact->subject = $request->subject;
        $savecontact->message = $request->message;
        $savecontact->save();

        Mail::send('mail.newcontact', ['title' => 'kkk', 'content' => $savecontact], function ($message)
        {

            $message->from('no-reply@embeebuilders.com', 'No Reply');

            $message->to('no-reply@embeebuilders.com');

        });

        return redirect('/success');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
