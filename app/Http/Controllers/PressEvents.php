<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\press;
class PressEvents extends Controller
{
   public function index(Request $request) {
       $pressevents = new press();
       $pressevents->heading = $request->heading;
       $pressevents->body = $request->body;
       $pressevents->save();
       return back();
   }

   public function show(Request $request) {
      // $projectlist = press::all();
       //dd($projectlist);
       return view('events.press');
   }
}
