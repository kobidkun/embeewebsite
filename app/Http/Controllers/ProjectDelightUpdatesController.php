<?php

namespace App\Http\Controllers;

use App\ProjectDelightUpdates;
use Illuminate\Http\Request;


class ProjectDelightUpdatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $projectlist = ProjectDelightUpdates::all();
       // dd($projectlist);
        return view('Projects.Delight.projectupdates', ['projects' => $projectlist]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$routename = 'update.delight.admin';
        return view('dashboard.project-updates.delight');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $projectupdate =  ProjectDelightUpdates::find(1);
        $projectupdate->date = $request->date;
        $projectupdate->url = $request->url;
        $projectupdate->project = $request->project;

        $projectupdate->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProjectDelightUpdates  $projectDelightUpdates
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectDelightUpdates $projectDelightUpdates)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectDelightUpdates  $projectDelightUpdates
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectDelightUpdates $projectDelightUpdates)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectDelightUpdates  $projectDelightUpdates
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectDelightUpdates $projectDelightUpdates)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectDelightUpdates  $projectDelightUpdates
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectDelightUpdates $projectDelightUpdates)
    {
        //
    }
}
