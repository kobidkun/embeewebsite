<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Referal;
use PDF;
use Image;
class ReferalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $ref = Referal::all();
       return view('dashboard.pages.refer', ['refer' => $ref]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('events.formrefer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'image' => 'dimensions:min_width=250,min_height=200'
        ]);


        $image = $request->file('image');
        $image2 = $request->file('sign');
      //  $img = Image::make($image)->resize(10, 20);
        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
        $input['imagename2'] = time().'.'.$image2->getClientOriginalExtension();

        $destinationPath = public_path('/uploads');

       // $img = Image::make($input['imagename'])->resize(300, 200);

        $image->move($destinationPath, $input['imagename']);
        $image2->move($destinationPath, $input['imagename2']);
      //  $img = Image::make('uploads/'.$input['imagename'])->resize(300, 200);

      // $img->move($destinationPath, $input['imagename']);
$imagename= $input['imagename'];
$sign = $input['imagename2'];
      //  dd($image);

        $referral = new Referal();
        $referral->name = $request->name;
        $referral->email = $request->email;
        $referral->mobile = $request->mobile;
        $referral->dob = $request->dob;
        $referral->project = $request->project;
        $referral->flattype = $request->flattype;
        $referral->block = $request->block;
        $referral->floor = $request->floor;
        $referral->flatno = $request->flatno;
        $referral->street = $request->street;
        $referral->locality = $request->locality;
        $referral->city = $request->city;
        $referral->state = $request->state;
        $referral->country = $request->country;
        $referral->ex1 = $imagename;
        $referral->ex2 = $sign;
        $referral->ex3 = $request->ex3;
        $referral->save();



        $pdf = PDF::loadView('pdf.referal', ['data' => $referral]);
        return $pdf->download($referral->name.'embeebuilders'.'-EB-RF-'.$referral->id.'.pdf');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $showref =  Referal::find($id);
        return view('pdf.referal', ['data' => $showref]);
        //echo ($showref);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
