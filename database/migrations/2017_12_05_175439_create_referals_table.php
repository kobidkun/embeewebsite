<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referals', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->text('email');
            $table->text('mobile');
            $table->text('dob')->nullable();
            $table->text('project')->nullable();
            $table->text('flattype')->nullable();
            $table->text('block')->nullable();
            $table->text('floor')->nullable();
            $table->text('flatno')->nullable();
            $table->text('street')->nullable();
            $table->text('locality')->nullable();
            $table->text('city')->nullable();
            $table->text('state')->nullable();
            $table->text('country')->nullable();
            $table->text('ex1')->nullable();
            $table->text('ex2')->nullable();
            $table->text('ex3')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referals');
    }
}
