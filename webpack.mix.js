let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |

 <!-- Libs -->
<script src="js/common.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/chosen.jquery.min.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script src="js/infobox.min.js"></script>

<!-- Template Scripts -->
<script src="js/variables.js"></script>
<script src="js/scripts.js"></script>


 */

/*mix.scripts([
    'public/js/common.js',
    'public/jquery.prettyPhoto.js',
    'public/owl.carousel.min.js',
    'public/js/chosen.jquery.min.js',
    'public/js/infobox.min.js',
    'public/js/variables.js',
    'public/js/scripts.js'

], 'public/js/compiled.js')
   .styles([
       'public/css/bootstrap.min.css',
       'public/css/style.css'

   ], 'public/css/compiled2.css');*/

mix.styles([
    'resources/assets/dashboard/animate.css',
    'resources/assets/dashboard/bootstrap.min.css',
    'resources/assets/dashboard/sidebar-nav.min.css',
    'resources/assets/dashboard/style.css',
    'resources/assets/dashboard/style/icons/material-design-iconic-font2/css/material-design-iconic-font.min.css'
], 'public/dashboard/compiled.min.css');


