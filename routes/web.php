<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('contactform', 'Contactus@store')->name('contact.store');
Route::post('callnowbutton', 'Callnowbutton@store')->name('call.savedata');
Route::post('contactus', 'contactpage@store')->name('contact.page.store');
Route::post('referalstore', 'ReferalController@store')->name('referal.data.store');
Route::post('homepagestore', 'Homequerrycontroller@store')->name('homee.quick.enquiry');
Route::get('submit-referal-form', 'ReferalController@create');



Route::get('/hom', function () {
    return view('welcome');
});





Route::get('/dashboard2', function () {
    return view('dashboard.delete');
});

Route::get('/pressadmin', function () {
    return view('dashboard.press');
});

Route::get('/projectupdates', function () {
    return view('Projects.Embeebuilders.projectupdates');
});



Route::get('/', 'HomepagewebsiteController@index');

Route::get('/contact', function () {
    return view('Pages.contact');
});

Route::get('/faq', function () {
    return view('Pages.faq');
});

Route::get('/nri-faq', function () {
    return view('Pages.nri-faq');
});


Route::get('/embee-builders', function () {
    return view('Projects.Embeebuilders.index');
});

Route::get('/embee-delight', function () {
    return view('Projects.Delight.index');
});

Route::get('/embee-utsab', function () {
    return view('Projects.Utsab.index');
});
//[pagwas
Route::get('/about', function () {
    return view('Pages.about');
});


//embeebuilders
Route::get('/embee-builders/amenities', function () {
    return view('Projects.Embeebuilders.amenities');
});
Route::get('/embee-builders/facilities', function () {
    return view('Projects.Embeebuilders.facilities');
});
Route::get('/embee-builders/floor-plan', function () {
    return view('Projects.Embeebuilders.floorplan');
});
Route::get('/embee-builders/specification', function () {
    return view('Projects.Embeebuilders.specification');
});
Route::get('/embee-builders/isometric-view', function () {
    return view('Projects.Embeebuilders.isometric');
});

Route::get('/embee-builders/testiminial', function () {
    return view('Projects.Embeebuilders.test');
});

//delight
Route::get('/embee-delight/amenities', function () {
    return view('Projects.Delight.amenities');
});
Route::get('/embee-delight/facilities', function () {
    return view('Projects.Delight.facilities');
});
Route::get('/embee-delight/floor-plan', function () {
    return view('Projects.Delight.floorplan');
});
Route::get('/embee-delight/specification', function () {
    return view('Projects.Delight.specification');
});
Route::get('/embee-delight/isometric-view', function () {
    return view('Projects.Delight.isometric');
});

//utsab
Route::get('/embee-utsab/amenities', function () {
    return view('Projects.Utsab.amenities');
});
Route::get('/embee-utsab/facilities', function () {
    return view('Projects.Utsab.facilities');
});
Route::get('/embee-utsab/floor-plan', function () {
    return view('Projects.Utsab.floorplan');
});
Route::get('/embee-utsab/specification', function () {
    return view('Projects.Utsab.specification');
});
Route::get('/embee-utsab/isometric-view', function () {
    return view('Projects.Utsab.isometric');
});

Route::get('/success', function () {
    return view('success.success');
});

Route::get('/fortune-live-events', function () {
    return view('events.live');
});


/*Route::get('/csr', function () {
    return view('events.csr');
});*/


Route::get('/referal-programme', function () {
    return view('events.referal');
});


Route::get('/delight-projectupdates', 'ProjectDelightUpdatesController@index');


Route::get('/utsab-projectupdates', 'ProjectUtsabUpdatesController@index');


Route::get('/pressevents', 'PressEvents@show')->name('index.updates');
Route::get('/projectupdates', 'ProjectUpdatecontroller@index')->name('index.updates');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin/press', 'PressImageupload@index')->name('admin.press')->middleware('auth');

Route::get('/admin/contactpage', 'contactpage@index')->middleware('auth');
Route::get('/admin/sidecontact', 'Contactus@index')->middleware('auth');
Route::get('/admin/callnow', 'Callnowbutton@index')->middleware('auth');
Route::get('/adminpannel', 'homepagecontroller@index')->middleware('auth')->name('admin.dashboard.index');
Route::get('/referview', 'ReferalController@index');
Route::get('/home-quick-enquiry', 'Homequerrycontroller@index');
Route::get('/referview/{id}', 'ReferalController@show')->name('referal.show');

//projectupdayes
Route::get('/projectupdates/delight', 'ProjectDelightUpdatesController@create')->middleware('auth');
Route::post('/projectupdates/delight', 'ProjectDelightUpdatesController@store')->name('update.delight.admin');


Route::get('/projectupdates/utsab', 'ProjectUtsabUpdatesController@create')->middleware('auth');
Route::post('/projectupdates/utsab', 'ProjectUtsabUpdatesController@store')->name('update.utsab.admin');




Route::post('/admin/press', 'PressImageupload@Pressevents')->name('admin.press.store');

Route::post('dash2', 'ProjectUpdatecontroller@create')->name('create.updates');

//csr

Route::get('/csr', 'CSRActivity@index');
//Route::get('/fortune-live-events', 'LiveActivity@index');



//end


Route::get('/dashboard2', function () {
    return view('dashboard.delete');
})->middleware('auth');

Route::get('/pressadmin', function () {
    return view('dashboard.press');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


///new
///
///
///
///
///
///
///
Route::get('/admin/csr', function () {
    return view('dashboard.pages.csr.csr');
})->middleware('auth')->name('dashboard.pages.csr.csr');

Route::post('/admin/csr/post', 'CSRActivity@store')->name('dashboard.pages.csr.csr.post');


Route::get('/admin/live', function () {
    return view('dashboard.pages.live.live');
})->middleware('auth')->name('dashboard.pages.live.live');
Route::post('/admin/live/post', 'LiveActivity@store')->name('dashboard.pages.live.live.post');