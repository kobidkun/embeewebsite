<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 <style type="text/css" media="all">
     @import url(https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic);body,html{height:100%;width:100%;margin:0;padding:0;left:0;top:0;font-size:100%}.center,.container{margin-left:auto;margin-right:auto}*{font-family:Lato,Helvetica,sans-serif;color:#333447;line-height:1.5}h1{font-size:2.5rem}h2{font-size:2rem}h3{font-size:1.375rem}h4{font-size:1.125rem}h5{font-size:1rem}h6{font-size:.875rem}p{font-size:1.125rem;font-weight:200;line-height:1.8}.font-light{font-weight:300}.font-regular{font-weight:400}.font-heavy{font-weight:700}.left{text-align:left}.right{text-align:right}.center{text-align:center}.justify{text-align:justify}.container{width:90%}.row{position:relative;width:100%}.row [class^=col]{float:left;margin:.5rem 2%;min-height:.125rem}.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9{width:96%}.col-1-sm{width:4.33%}.col-2-sm{width:12.66%}.col-3-sm{width:21%}.col-4-sm{width:29.33%}.col-5-sm{width:37.66%}.col-6-sm{width:46%}.col-7-sm{width:54.33%}.col-8-sm{width:62.66%}.col-9-sm{width:71%}.col-10-sm{width:79.33%}.col-11-sm{width:87.66%}.col-12-sm{width:96%}.row::after{content:"";display:table;clear:both}.hidden-sm{display:none}@media only screen and (min-width:33.75em){.container{width:80%}}@media only screen and (min-width:45em){.col-1{width:4.33%}.col-2{width:12.66%}.col-3{width:21%}.col-4{width:29.33%}.col-5{width:37.66%}.col-6{width:46%}.col-7{width:54.33%}.col-8{width:62.66%}.col-9{width:71%}.col-10{width:79.33%}.col-11{width:87.66%}.col-12{width:96%}.hidden-sm{display:block}}@media only screen and (min-width:60em){.container{width:75%;max-width:60rem}}
.subheading{
    font-weight: 500;
}

     .subheading2{
    text-align: center;

}

     .subheading2 span{
    text-align: center;
         font-weight: 500;

}

 </style>
   {{-- <link rel="stylesheet" href=src="{{asset('css/grid.min.css')}}" />--}}
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 style="text-align: center; color: rgba(231,69,88,0.8)">Life Long Bonding</h1>
        </div>


    </div>
    <hr>
    <h2 style="text-align: center; font-weight: 500; color: #e74558">Registration Form</h2>
    <h5 class="subheading2" >Serial No:<span >EB-RS-{{$data->id}}</span></h5>
    <div class="row">

        <div class="col-8">

            <h4>Name: <span class="subheading">{{$data->name}}</span></h4>
            <h4>DATE OF BIRTH: <span class="subheading">{{$data->dob}}</span></h4>
            <h4>PROJECT NAME: <span class="subheading">{{$data->project}}</span></h4>
            <h4>Block: <span class="subheading">{{$data->block}}</span></h4>
            <h4>Floor: <span class="subheading">{{$data->floor}}</span></h4>
            <h4>Flat No: <span class="subheading">{{$data->flatno}}</span></h4>
            <h4>Email ID: <span class="subheading">{{$data->email}}</span></h4>
            <h4>Mobile Number: <span class="subheading">{{$data->mobile}}</span></h4>
            <h4>Address: <span class="subheading">{{$data->street}} {{$data->locality}} {{$data->city}} {{$data->state}}</span></h4>
        </div>

        <div class="col-4">
        <div style="height: 180px; width: 150px; border: dashed">

                <img src="{{url('uploads').'/'.$data->ex1}}" height="180px" width="150px" alt="">
            </div>
        </div>
        <br>




    </div>
    <br>
    <br>

    <div class="row">
        <div class="col-9">
        </div>

        <div class="col-3">
            <img src="{{url('uploads').'/'.$data->ex2}}" height="50px" width="180px" alt=""> <br> <p style="text-align: center">Signature</p>
        </div>





    </div>
</div>
</body>
</html>