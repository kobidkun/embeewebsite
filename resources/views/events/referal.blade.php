@extends('structure')
@section('page_css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css"/>

@stop
@section('content')
    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Embee Builders</h1>

                    <ul class="breadcrumb">
                        <li><a href="/">Home </a></li>
                        <li><a href="#">REFERRAL PROGRAM</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->
    <div class="content">
        <div class="container">
            <div class="row">
                <style>
                    .paddings {
                        padding-top: 10px;
                        padding-bottom: 10px;
                        border: 1px;
                        border-color: rgba(231, 69, 88, 0.89);

                    }
                </style>
                <!-- BEGIN MAIN CONTENT -->
                <div class="col-md-12 col-xs-12">
                    <h1 class="section-title">Life Long Bonding
                    </h1>

                    <h2 class="h2sec">A REFERRAL PROGRAM INITIATED BY EMBEE BUILDERS</h2><br>
                    <h3 class="h2sec">Starting From 1st Jan 2018</h3><br>

                    <style>
                        .h2sec {
                            text-align: center;
                        }

                        .italia {
                            color: #e74558;
                        }
                    </style>
                    <style>
                        .imgdown {
                            background-color: #ff4961;
                            font-size: 22px;
                            color: #ffffff;
                            text-align: center;
                        }

                        .mainimg {
                            align-items: center;
                            align-content: center;
                        }
                    </style>
                    <br>
                    <br>
                    <br>

                    <p>

                       <span class="italia">
                           "Never Forget who was with you from the beginning"
                       </span> with this belief
                        Embee Builders is launching a new program which has been termed as
                        "LIFE LONG BONDING". A step taken by Embee Builders to encourage
                        its existing buyers to be a part of the Embee family not only as buyers /
                        customers but also as an associate with Embee Builders.
                    </p>

                    <p>

                        Every existing buyer who brings a prospective buyer in any of our
                        projects, will be entitled to receive a token of appreciation in the form of
                        gifts. The gifts are mentioned below. Every buyer who brings a referral
                        can claim the gifts when the referral customer completes 50% payment of
                        the purchased flat. <br>
                        We have arranged the most simplest way to carry out the entire process
                        which will be very friendly as well as encouraging to each and every
                        member of the referral group. <br>
                        With the belief of extending a bond or relationship with our buyers, we are
                        ready with open arms to invite every buyer of Embee Builders to be a part
                        of our referral program.
                    </p>


                    <br>
                    <br>
                    <br>
                    <br>
                    <h1 class="section-title">Embee Delight & Embee Fortune
                    </h1>
                    <br>
                    <br>
                    <br>
                    <br>
                    <h1 class="h2sec">Gifts for selling 2 BHK Flats</h1>

                    <div class="col-md-12">


                        <div class="col-md-4 paddings"><a href="{{asset('/images/gift/1.jpg')}}"
                                                          data-fancybox
                                                          data-caption="Eureka forbes water purifier">

                                <img class="img-responsive center-block"
                                     src="{{asset('images/gift/1.jpg')}}"
                                     height="250px" alt=""/>

                            </a>
                            <div class="imgdown">
                                Eureka Purifier
                            </div>
                        </div>
                        <div class="col-md-4 paddings"><a href="{{asset('/images/gift/2.jpg')}}"
                                                          data-fancybox
                                                          data-caption="Nokia 6 Mobile">

                                <img class="img-responsive center-block"
                                     src="{{asset('images/gift/2.jpg')}}"
                                     height="250px" alt=""/>

                            </a>
                            <div class="imgdown">
                                Nokia 6 Mobile
                            </div>
                        </div>
                        <div class="col-md-4 paddings"><a href="{{asset('/images/gift/3.jpg')}}"
                                                          data-fancybox
                                                          data-caption="Yamaha Guitar">

                                <img class="img-responsive center-block"
                                     src="{{asset('images/gift/3.jpg')}}"
                                     height="250px" alt=""/>

                            </a>
                            <div class="imgdown">
                                Yamaha Guitar
                            </div>
                        </div>
                        <div class="col-md-4 paddings"><a href="{{asset('/images/gift/4.jpg')}}"
                                                          data-fancybox
                                                          data-caption="Karcher Vaccum Cleaner ">

                                <img class="img-responsive center-block"
                                     src="{{asset('images/gift/4.jpg')}}"
                                     height="250px" alt=""/>

                            </a>
                            <div class="imgdown">
                                Karcher Vaccum Cleaner
                            </div>
                        </div>
                        <div class="col-md-4 paddings"><a href="{{asset('/images/gift/5.jpg')}}"
                                                          data-fancybox
                                                          data-caption="Elica Chimney">

                                <img class="img-responsive center-block"
                                     src="{{asset('images/gift/5.jpg')}}"
                                     height="250px" alt=""/>

                            </a>
                            <div class="imgdown">
                                Elica Chimney
                            </div>
                        </div>


                    </div>

                    <br>
                    <br>
                    <br>
                    <br>

                    <div class="col-md-12">
                        <br>
                        <br>
                        <br>
                        <br>

                        <h1 class="h2sec">Gifts for selling 3 BHK Flats</h1>


                        <div class="col-md-4 paddings"><a href="{{asset('/images/gift/6.jpg')}}"
                                                          data-fancybox
                                                          data-caption="Sony Bravia TV 32 Inch">

                                <img class="img-responsive center-block"
                                     src="{{asset('images/gift/6.jpg')}}"
                                     height="250px" alt=""/>

                            </a>
                            <div class="imgdown">
                                Sony TV 32in
                            </div>
                        </div>
                        <div class="col-md-4 paddings"><a href="{{asset('/images/gift/7.jpg')}}"
                                                          data-fancybox
                                                          data-caption="Sony Home Theater System">

                                <img class="img-responsive center-block"
                                     src="{{asset('images/gift/7.jpg')}}"
                                     height="250px" alt=""/>

                            </a>
                            <div class="imgdown">
                                Sony Theater
                            </div>
                        </div>
                        <div class="col-md-4 paddings"><a href="{{asset('/images/gift/8.jpg')}}"
                                                          data-fancybox
                                                          data-caption="HP Laptop">

                                <img class="img-responsive center-block"
                                     src="{{asset('images/gift/8.jpg')}}"
                                     height="250px" alt=""/>

                            </a>
                            <div class="imgdown">
                                HP Laptop
                            </div>
                        </div>
                        <div class="col-md-4 paddings"><a href="{{asset('/images/gift/9.jpg')}}"
                                                          data-fancybox
                                                          data-caption="Bosch Washing Machine">

                                <img class="img-responsive center-block"
                                     src="{{asset('images/gift/9.jpg')}}"
                                     height="250px" alt=""/>

                            </a>
                            <div class="imgdown">
                                Bosch Washing Machine
                            </div>
                        </div>
                        <div class="col-md-4 paddings"><a href="{{asset('/images/gift/10.jpg')}}"
                                                          data-fancybox
                                                          data-caption="Llyod Air Conditioner">

                                <img class="img-responsive center-block"
                                     src="{{asset('images/gift/10.jpg')}}"
                                     height="250px" alt=""/>

                            </a>
                            <div class="imgdown">
                                Llyod Air Conditioner
                            </div>
                        </div>


                    </div>


                    <br>
                    <br>
                    <br>
                    <br>

                    <div class="col-md-12">


                        <br>
                        <br>
                        <br>
                        <br>


                        <h1 class="h2sec">Gifts for selling 1/2 BHK Squarewood Utsab</h1>

                        <div class="col-md-12">
                            <div class="col-md-4 paddings"><a href="{{asset('/images/gift/11.jpg')}}"
                                                              data-fancybox
                                                              data-caption="Usha Janome">

                                    <img class="img-responsive center-block"
                                         src="{{asset('images/gift/11.jpg')}}"
                                         height="250px" alt=""/>

                                </a>
                                <div class="imgdown">
                                    Usha Janome
                                </div>
                            </div>
                            <div class="col-md-4 paddings"><a href="{{asset('/images/gift/12.jpg')}}"
                                                              data-fancybox
                                                              data-caption="Bajaj Microwave Oven 20 Ltr">

                                    <img class="img-responsive center-block"
                                         src="{{asset('images/gift/12.jpg')}}"
                                         height="250px" alt=""/>

                                </a>
                                <div class="imgdown">
                                    Bajaj Microwave
                                </div>
                            </div>
                            <div class="col-md-4 paddings"><a href="{{asset('/images/gift/13.jpg')}}"
                                                              data-fancybox
                                                              data-caption="Crompton 20 Lt">

                                    <img class="img-responsive center-block"
                                         src="{{asset('images/gift/13.jpg')}}"
                                         height="250px" alt=""/>

                                </a>
                                <div class="imgdown">
                                    Crompton 20 Lt
                                </div>
                            </div>
                            <div class="col-md-4 paddings"><a href="{{asset('/images/gift/14.jpg')}}"
                                                              data-fancybox
                                                              data-caption="Crompton 20 Lt">

                                    <img class="img-responsive center-block"
                                         src="{{asset('images/gift/14.jpg')}}"
                                         height="250px" alt=""/>

                                </a>
                                <div class="imgdown">
                                    Crompton 20 Lt
                                </div>
                            </div>
                            <div class="col-md-4 paddings"><a href="{{asset('/images/gift/15.jpg')}}"
                                                              data-fancybox
                                                              data-caption="Wanderchef Induction">

                                    <img class="img-responsive center-block"
                                         src="{{asset('images/gift/15.jpg')}}"
                                         height="250px" alt=""/>

                                </a>
                                <div class="imgdown">
                                    Wonderchef Induction
                                </div>
                            </div>
                        </div>


                    </div>


                </div>

                <div style="margin-top: 1000px; font-size: 50px; color: #ffffff;">
                    sasasasasasasa


                </div>

                <h2 class="h2sec" > Registration</h2>

                <div style=" font-size: 30px; color: #ffffff;">
                    sasasasasasasa


                </div>

                <div class="col-md-12">




                    <div class="col-md-6"
style="padding: 5px 5px 5px 5px     "
                    >
                        <a href="/submit-referal-form">
                            <button
                                    style="background-color: #e74558;"
                                    type="button" class="btn btn-danger btn-lg btn-block">
                                 Online Registration </button>
                        </a>



                    </div>


                    <div class="col-md-6"
                         style="padding: 5px 5px 5px 5px     "
                    >

                        <a href="/referal/registration-form.pdf">
                            <button
                                    style="background-color: #e74558;"
                                    type="button" class="btn btn-danger btn-lg btn-block">
                                Download Registration Form </button>
                        </a>

                    </div>




                </div>



                <div class="col-md-12"

                >
                    <div style=" font-size: 30px; color: #ffffff;">
                        sasasasasasasa


                    </div>

                    <h2 class="h2sec">TERMS & CONDITIONS</h2>
                    <div style=" font-size: 30px; color: #ffffff;">
                        sasasasasasasa


                    </div>
                    <p>

                        1. To be a part of the referral program, you have to be a buyer in any of the projects of Embee
                        Builders. <br>
                        2. You can sell as many flats as you can and for every flat booked you are eligible of claiming
                        one gift,
                        which means if you sell 10 flats, you are entitled to claim 10 gifts. <br>
                        3. After your successful registration you will be provided an unique referral code. Every time
                        you send
                        a new buyer, the buyer has to mention your unique ID and name on the visitors form, when the
                        buyer
                        makes his first visit in Site Marketing Office. <br>
                        4. You can only claim the gift after the customer provided by you completes 50% of the payment
                        for the
                        flat purchased by him/her. <br>
                        5. If you are a resident of Siliguri, you can either fill up the form and physically handover
                        the
                        registration form to our Site Office and if you are from outstation please send the registration
                        form to
                        our office address which is " Pusp Niwas, Biswakarma Mandir Road, Khalpara, Siliguri, Pin:
                        734005 ",
                        or you can also complete the registration online at www.embeebuilders.com. <br>
                        6. As soon as your registration form is verified, your Unique ID shall be mailed to you through
                        registered courier, by Email Address and an SMS to your provided mobile number. The entire
                        verification process may take up-to 3 - 5 working days. Kindly provide only one mobile number in
                        the
                        registration form, so that we can make future conversations, to avoid any confusion. <br>
                        7. On the 1st Visit of the customer, he has to clearly mention the name of the referrer and the
                        unique ID.
                        If on the third or the fourth visit, before / after the customer booking, if the customer
                        mentions any
                        referrer name, we shall not entertain it in any cases. <br>
                        8. If one customer is send by multiple referrers, the person sent by the first referrer shall be
                        eligible to
                        claim the gift for the following. The referrer name mentioned in the first visitor form of the
                        customer
                        shall be applicable for the referral process. <br>
                        9. For cautiousness, we would like to mention that our marketing and sales team is totally
                        in-house. No
                        other marketing agency is solely associated with any sales or marketing purposes. We like to
                        believe
                        that we have the best team in-house who will be trying their best to convert your leads as
                        confirmed
                        buyers.
                    </p>


                </div>


            </div>


            <!-- END PROPERTIES ASSIGNED -->

        </div>
        <!-- END MAIN CONTENT -->


    </div>
    </div>
    </div>
@endsection




@section('page_js')
    <script src="js/freewall.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>

@stop