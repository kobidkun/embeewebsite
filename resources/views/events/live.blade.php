@extends('structure')
@section('page_css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css"/>

@stop
@section('content')
    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Embee Builders</h1>

                    <ul class="breadcrumb">
                        <li><a href="/">Home </a></li>
                        <li><a href="/embee-builders">Embee Builders</a></li>
                        <li><a href="#">Live Events</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->
    <div class="content">
        <div class="container">
            <div class="row">
                <style>
                    .paddings {
                        padding-top: 10px;
                        padding-bottom: 10px;
                        border: 1px;
                        border-color: rgba(231, 69, 88, 0.89);

                    }
                </style>
                <!-- BEGIN MAIN CONTENT -->
                {{--start--}}















                {{--end--}}




                <div class="col-md-8 col-xs-12">
                    <h1 class="section-title">
                        CREDAI 2018 GrihaPravesh - Winners of Best Exhibition Stall
                    </h1>
                    <p>
                        Embee Builders and Manakamna Builders jointly participated at CREDAI Griha-Pravesh 2018. The exhibition was held in Swabhumi Grand, Siliguri. It was a real estate meet where all major and upcoming builders participated in the exhibition which was held from 25th to 28th Jan 2018. The footfall was adequate, we have advertised on hoardings and presented news paper ads to attract customers, which was successful. Our exhibition stall was give most attractive and best exhibition stall award for the following event.

                    </p>
                    <style>
                        .imgdown {
                            background-color: #ff4961;
                            font-size: 22px;
                            color: #ffffff;
                            text-align: center;
                        }

                        .mainimg {
                            align-items: center;
                            align-content: center;
                        }
                    </style>
                    <div class="col-md-6 paddings"><a href="{{asset('live/1.png')}}"
                                                      data-fancybox
                                                      data-caption="CREDAI 2018">

                            <img class="img-responsive center-block"
                                 src="{{asset('live/1.png')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CREDAI 2018
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('live/2.jpg')}}"
                                                      data-fancybox
                                                      data-caption="CREDAI 2018">

                            <img class="img-responsive center-block"
                                 src="{{asset('live/2.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CREDAI 2018
                        </div>
                    </div>


                    <div class="col-md-6 paddings"><a href="{{asset('live/3.jpg')}}"
                                                      data-fancybox
                                                      data-caption="CREDAI 2018">

                            <img class="img-responsive center-block"
                                 src="{{asset('live/3.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CREDAI 2018
                        </div>
                    </div>


                    <div class="col-md-6 paddings"><a href="{{asset('live/4.jpg')}}"
                                                      data-fancybox
                                                      data-caption="CREDAI 2018">

                            <img class="img-responsive center-block"
                                 src="{{asset('live/4.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CREDAI 2018
                        </div>
                    </div>




                    {{--next--}}










                    <h1 class="section-title">
                        MARKETING MEET IN DALKHOLA. DATED: 16TH JAN 2016
                    </h1>
                    <style>
                        .imgdown {
                            background-color: #ff4961;
                            font-size: 22px;
                            color: #ffffff;
                            text-align: center;
                        }

                        .mainimg {
                            align-items: center;
                            align-content: center;
                        }
                    </style>
                    <div class="col-md-6 paddings"><a href="{{asset('images/live/1.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/1.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            Marketing meet in Dalkhola
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/2.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/2.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            Marketing meet in Dalkhola
                        </div>
                    </div>


                    <div class="col-md-6 paddings"><a href="{{asset('images/live/4.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/4.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            Marketing meet in Dalkhola
                        </div>
                    </div>




                    {{--next--}}





















                    <h1 class="section-title">
                        EMBEE FORTUNE CELEBRATES 100 SUCCESSFUL BOOKINGS
                    </h1>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/10.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/10.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/11.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/11.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/12.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/12.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/13.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/13.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/14.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/14.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/15.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/15.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/16.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/16.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/17.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/17.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/18.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/18.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/19.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/19.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/20.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/20.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/21.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/21.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/22.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/22.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/23.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/23.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>


                    <div class="col-md-6 paddings"><a href="{{asset('images/live/25.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/25.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/26.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/26.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/27.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/27.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/28.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/28.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/30.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/30.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/31.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/31.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/32.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/32.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/33.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/33.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/34.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/34.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/35.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/35.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/36.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/36.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/37.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/37.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/38.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/38.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/39.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/39.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/40.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/40.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            100 SUCCESSFUL BOOKINGS
                        </div>
                    </div>


                    <h1 class="section-title">
                        EMBEE FORTUNE CELEBRATES ITS FIRST DURGA PUJA
                    </h1>


                    <div class="col-md-6 paddings"><a href="{{asset('images/live/101.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/101.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            Millanpally DURGA PUJA
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/102.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/102.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            Millanpally DURGA PUJA
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/103.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/103.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            Millanpally DURGA PUJA
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/104.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/104.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            Millanpally DURGA PUJA
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/live/105.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/105.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            Millanpally DURGA PUJA
                        </div>
                    </div>

                    <h1 class="section-title">
                        EMBEE FORTUNE CELEBRATING DURGA PUJA WITH ATHLETICO CLUB
                    </h1>


                    <div class="col-md-6 paddings"><a href="{{asset('images/live/1001.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/1001.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            ATHLETICO CLUB DURGA PUJA
                        </div>
                    </div>


                    <div class="col-md-6 paddings"><a href="{{asset('images/live/1002.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/1002.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            ATHLETICO CLUB DURGA PUJA
                        </div>
                    </div>


                    <div class="col-md-6 paddings"><a href="{{asset('images/live/1003.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/1003.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            ATHLETICO CLUB DURGA PUJA
                        </div>
                    </div>


                    <div class="col-md-6 paddings"><a href="{{asset('images/live/1004.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/live/1004.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            ATHLETICO CLUB DURGA PUJA
                        </div>
                    </div>



                </div>
                <div class="col-md-4 col-xs-12">
                    @include('Parts.right')
                </div>


                <!-- END PROPERTIES ASSIGNED -->

            </div>
            <!-- END MAIN CONTENT -->


        </div>
    </div>
    </div>
@endsection




@section('page_js')
    <script src="js/freewall.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>

@stop