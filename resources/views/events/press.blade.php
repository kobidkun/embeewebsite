@extends('structure')
@section('page_css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />

@stop
@section('content')
    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Embee Builders</h1>

                    <ul class="breadcrumb">
                        <li><a href="/">Home </a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="/embee-builders">Embee Builders</a></li>
                        <li><a href="#">Press Release </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->
    <div class="content">
        <div class="container">
            <div class="row">
                <style>
                    .paddings {
                        padding-top: 10px;
                        padding-bottom: 10px;
                        border: 1px;
                        border-color: rgba(231,69,88,0.89);

                    }
                </style>
                <!-- BEGIN MAIN CONTENT -->
                <div class="col-md-8 col-xs-12">
                    <h1 class="section-title">EMBEE FORTUNE CELEBRATES 100 SUCCESSFUL BOOKINGS
                        DATE - 21ST NOV 2015

                    </h1>
                    <style>
                        .imgdown {
                            background-color: #ff4961;
                            font-size: 22px;
                            color: #ffffff;
                            text-align: center;
                        }

                        .mainimg {
                            align-items: center;
                            align-content: center;
                        }
                    </style>
                    <div class="col-md-6 paddings"><a href="{{asset('/press/a.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('/press/a.jpg')}}"
                                 height="250px" alt=""/>

                        </a>

                    </div>


                    <div class="col-md-6 paddings"><a href="{{asset('/press/b.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('/press/b.jpg')}}"
                                 height="250px" alt=""/>

                        </a>

                    </div>
                    <div class="col-md-6 paddings"><a href="{{asset('/press/c.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('/press/c.jpg')}}"
                                 height="250px" alt=""/>

                        </a>

                    </div>


                    <div class="col-md-6 paddings"><a href="{{asset('/press/d.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('/press/d.jpg')}}"
                                 height="250px" alt=""/>

                        </a>

                    </div>
                    <div class="col-md-6 paddings"><a href="{{asset('/press/e.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('/press/e.jpg')}}"
                                 height="250px" alt=""/>

                        </a>

                    </div>


                    <h1 class="section-title">EMBEE FORTUNE GENERAL PRESS RELEASE

                    </h1>



                    <div class="col-md-6 paddings"><a href="{{asset('/press/f.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('/press/f.jpg')}}"
                                 height="250px" alt=""/>

                        </a>

                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('/press/g.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('/press/g.jpg')}}"
                                 height="250px" alt=""/>

                        </a>

                    </div>


                    <div class="col-md-6 paddings"><a href="{{asset('/press/h.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('/press/h.jpg')}}"
                                 height="250px" alt=""/>

                        </a>

                    </div>
                    <div class="col-md-6 paddings"><a href="{{asset('/press/i.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('/press/i.jpg')}}"
                                 height="250px" alt=""/>

                        </a>

                    </div>






                </div>
                <div class="col-md-4 col-xs-12">
                    @include('Parts.right')
                </div>







                <!-- END PROPERTIES ASSIGNED -->

                </div>
                <!-- END MAIN CONTENT -->




            </div>
        </div>
    </div>
@endsection




@section('page_js')
    <script src="js/freewall.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>

@stop