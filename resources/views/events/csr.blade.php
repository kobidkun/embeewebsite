@extends('structure')
@section('page_css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />

@stop
@section('content')
    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Embee Builders</h1>

                    <ul class="breadcrumb">
                        <li><a href="/">Home </a></li>
                        <li><a href="/embee-builders">Embee Builders</a></li>
                        <li><a href="#">CSR Activities</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->
    <div class="content">
        <div class="container">
            <div class="row">
                <style>
                    .paddings {
                        padding-top: 10px;
                        padding-bottom: 10px;
                        border: 1px;
                        border-color: rgba(231,69,88,0.89);

                    }
                </style>
                <!-- BEGIN MAIN CONTENT -->
                <div class="col-md-8 col-xs-12">





                    <h1 class="section-title">
                        Embee Builder initiated CSR Activity in Nrityanjali Vidyapith for their sports event.
                    </h1>
                    <style>
                        .imgdown {
                            background-color: #ff4961;
                            font-size: 22px;
                            color: #ffffff;
                            text-align: center;
                        }

                        .mainimg {
                            align-items: center;
                            align-content: center;
                        }
                    </style>

                    <p>

                        EMBEE BUILDERS WERE ONCE AGAIN DELIGHTED TO PERFORM CORPORATE SOCIAL RESPONSIBILITY BY HELPING NRITYANJALI VIDYAPITH, SPONSORING THE PRIZES FOR 1ST, 2ND, 3RD POSITIONS FOR THE EVENTS CONDUCTED IN THE SPORTS EVENT.  THE EVENT WAS HELD ON 26TH JANUARY 2018.  THE EVENT WAS SUCCESSFUL AS STUDENTS OF THAT SCHOOL ONCE AGAIN ANTICIPATED WITH GREAT ENTHUSIAM AND SPORTING SPIRIT. THIS TIME THE EVENT WAS MUCH BIGGER AND MORE STUDENTS PARTICIPATED IN THE EVENT. IT WAS JOYFUL AND WAS ENJOYABLE AS THE EVENTS WERE MORE AND THE STUDENTS WERE EQUALLY EXCITED AND PASSIONATE WITH THEIR IMMENSE SPORTING SPIRIT.

                        <br>
                        THE EVENT WAS CONDUCTED IN NRITYANJALI VIDYAPITH THAT IS LOCATED IN OPPOSITE OF NORTH BENGAL MEDICAL COLLEGE. THE SCHOOL HAS PRIMARY SECTIONS STARTING FROM PRIMARY LEVEL TO STANDARD 4. IT’S AN UNPRIVILEGED SCHOOL WHO ARE HELPING POOR CHILDREN TO ACHIEVE PRIMARY EDUCATION. EMBEE BUILDERS INITIATED TO EXTEND THEIR HANDS TO HELP THE SCHOOL TO ENCOURGE THEIR WELL-BEING AND THE GOOD CAUSE
                    </p>
                    <div class="col-md-6 paddings"><a href="/csract/105.jpg"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="/csract/105.jpg"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
                    <div class="col-md-6 paddings"><a href="/csract/100.jpg"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="/csract/100.jpg"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
                    <div class="col-md-6 paddings"><a href="/csract/101.jpg"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="/csract/101.jpg"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
                    <div class="col-md-6 paddings"><a href="/csract/102.jpg"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="/csract/102.jpg"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>


                    <div class="col-md-6 paddings"><a href="/csract/103.jpg"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="/csract/103.jpg"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="/csract/104.jpg"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="/csract/104.jpg"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>


                    {{--end main--}}








                    <h1 class="section-title">
                        EMBEE BUILDERS INITIATED CSR ACTIVITY IN NRITYANJALI VIDYAPITH
                    </h1>
                    <style>
                        .imgdown {
                            background-color: #ff4961;
                            font-size: 22px;
                            color: #ffffff;
                            text-align: center;
                        }

                        .mainimg {
                            align-items: center;
                            align-content: center;
                        }
                    </style>

                    <p>

                        Embee Builders once again took an initiative step in encouraging Education by initiating a Drawing Competition in Nrityanjali Vidyapith School. The Nrityanjali Vidyapith School, which has played a vital role in providing education to the childrens belonging from poor families. On 15th August 2017, Embee Builders, delightfully performed a Social Welfare Program, by organizing a drawing competition in Nrityanjali Vidyapith School. The event took place from 11am- 1:30pm and around, 130 students, participated in the event. The students, were categorized into three groups- First group, included students of K.G. and Nursery; The Second group, included students from Class 1 to 3; and The Third group, included students from Class 4 to 6. All the students, were provided Drawing kits, Pencils, Erasers and Sharpners, which was sponsored by Embee Builders. First, Second and Third Prize, were given to the best, from each group. The only motto of Embee Builders, in initiating the event is to motivate students in the field of Art and Extra- Curricular Activitie. The drawing kit was given as consolation prizes. So, that the students are encouraged towards extra- curricular activities. Later, the event was concluded by Snacks, for the students and the Teachers, arranged by Embee Builders. The Event was conducted in Nrityanjali Vidyapith School, in Shushrutanagar, Opp. Of North Bengal Medical College, Siliguri. This school is a primary school, consisting classes from Nursery to Class-6.

                    </p>
                    <div class="col-md-6 paddings"><a href="{{asset('images/csr/1.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/1.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
                    <div class="col-md-6 paddings"><a href="{{asset('images/csr/2.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/2.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
                    <div class="col-md-6 paddings"><a href="{{asset('images/csr/3.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/3.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
                    <div class="col-md-6 paddings"><a href="{{asset('images/csr/4.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/4.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>


                    <div class="col-md-6 paddings"><a href="{{asset('images/csr/5.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/5.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>


                    {{--next--}}

                    <h1 class="section-title">
                        EMBEE BUILDER INITIATED CSR ACTIVITY IN NRITYANJALI VIDYAPITH FOR THEIR SPORTS EVENT.

                    </h1>


                    <p>


                        EMBEE BUILDERS WERE ONCE AGAIN DELIGHTED TO PERFORM CORPORATE SOCIAL RESPONSIBILITY BY HELPING NRITYANJALI VIDYAPITH, SPONSORING THE PRIZES FOR 1ST, 2ND , 3RD POSITIONS FOR THE EVENTS CONDUCTED IN THE SPORTS EVENT. THE EVENT WAS HELD ON 26TH JANUARY 2017. THE EVENT WAS A MERE SUCCESS AS STUDENTS OF THAT SCHOOL ANTICIPATED WITH GREAT ENTHUSIAM AND SPORTING SPIRIT. THIS TIME THE EVENT WAS MUCH BIGGER AND MORE STUDENTS PARTICIPATED IN THE EVENT. IT WAS JOYFUL AND WAS ENJOYABLE AS THE EVENTS WERE MORE AND THE STUDENTS WERE EQUALLY EXCITED AND PASSIONATE WITH THEIR IMMENSE SPORTING SPIRIT. THE EVENT WAS CONDUCTED IN NRITYANJALI VIDYAPITH THAT IS LOCATED IN OPPOSITE OF NORTH BENGAL MEDICAL COLLEGE. THE SCHOOL HAS PRIMARY SECTIONS STARTING FROM PRIMARY LEVEL TO STANDARD 4. IT IS AN UNPRIVILEGED SCHOOL WHO ARE HELPING POOR CHILDREN TO ACHIEVE PRIMARY EDUCATION. EMBEE BUILDERS INITIATED TO EXTEND THEIR HAND TO HELP THE SCHOOL SO AS TO BOOST THEM UP IN THEIR SOCIAL CAUSE
                    </p>


                    <div class="col-md-6 paddings"><a href="{{asset('images/csr/101.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/101.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
<div class="col-md-6 paddings"><a href="{{asset('images/csr/102.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/102.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
<div class="col-md-6 paddings"><a href="{{asset('images/csr/103.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/102.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
<div class="col-md-6 paddings"><a href="{{asset('images/csr/104.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/104.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
<div class="col-md-6 paddings"><a href="{{asset('images/csr/105.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/105.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
<div class="col-md-6 paddings"><a href="{{asset('images/csr/106.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/106.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
<div class="col-md-6 paddings"><a href="{{asset('images/csr/107.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/107.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>

                    <h1 class="section-title">
                        EMBEE BUILDER INITIATED CSR ACTIVITY IN NRITYANJALI VIDYAPITH FOR THEIR SPORTS EVENT.

                    </h1>


                    <p>


                        EMBEE BUILDERS WERE ONCE AGAIN DELIGHTED TO PERFORM CORPORATE SOCIAL RESPONSIBILITY BY HELPING NRITYANJALI VIDYAPITH, SPONSORING THE PRIZES FOR 1ST, 2ND , 3RD POSITIONS FOR THE EVENTS CONDUCTED IN THE SPORTS EVENT. THE EVENT WAS HELD ON 26TH JANUARY 2017. THE EVENT WAS A MERE SUCCESS AS STUDENTS OF THAT SCHOOL ANTICIPATED WITH GREAT ENTHUSIAM AND SPORTING SPIRIT. THIS TIME THE EVENT WAS MUCH BIGGER AND MORE STUDENTS PARTICIPATED IN THE EVENT. IT WAS JOYFUL AND WAS ENJOYABLE AS THE EVENTS WERE MORE AND THE STUDENTS WERE EQUALLY EXCITED AND PASSIONATE WITH THEIR IMMENSE SPORTING SPIRIT. THE EVENT WAS CONDUCTED IN NRITYANJALI VIDYAPITH THAT IS LOCATED IN OPPOSITE OF NORTH BENGAL MEDICAL COLLEGE. THE SCHOOL HAS PRIMARY SECTIONS STARTING FROM PRIMARY LEVEL TO STANDARD 4. IT IS AN UNPRIVILEGED SCHOOL WHO ARE HELPING POOR CHILDREN TO ACHIEVE PRIMARY EDUCATION. EMBEE BUILDERS INITIATED TO EXTEND THEIR HAND TO HELP THE SCHOOL SO AS TO BOOST THEM UP IN THEIR SOCIAL CAUSE
                    </p>


                    <div class="col-md-6 paddings"><a href="{{asset('images/csr/1001.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/1001.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
 <div class="col-md-6 paddings"><a href="{{asset('images/csr/1002.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/1002.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
 <div class="col-md-6 paddings"><a href="{{asset('images/csr/1003.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/1003.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
 <div class="col-md-6 paddings"><a href="{{asset('images/csr/1004.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/1004.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
 <div class="col-md-6 paddings"><a href="{{asset('images/csr/1005.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/1005.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
 <div class="col-md-6 paddings"><a href="{{asset('images/csr/1006.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/1006.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
 <div class="col-md-6 paddings"><a href="{{asset('images/csr/1007.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/1007.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>



                    <h1 class="section-title">
                        EMBEE FORTUNE INITIATES CORPORATE SOCIAL RESPONSIBILITY.

                    </h1>


                    <p>

                        EMBEE FORTUNE initiates Corporate Social Responsibility by helping Nrityanjali Vidyapith in supporting the under-privileged children by providing education in primary level. EMBEE FORTUNE was delighted to sponsor this under-privileged school by offering 1st, 2nd and 3rd prizes for the sports event initiated by the school held in the occasion of 26th January 2016 celebrating Republic Day and Sports Day
                    </p>


                    <div class="col-md-6 paddings"><a href="{{asset('images/csr/10001.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/10001.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
                    <div class="col-md-6 paddings"><a href="{{asset('images/csr/10002.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/10002.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
                    <div class="col-md-6 paddings"><a href="{{asset('images/csr/1003.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/10003.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
                    <div class="col-md-6 paddings"><a href="{{asset('images/csr/10004.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/10004.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
                    <div class="col-md-6 paddings"><a href="{{asset('images/csr/10005.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/10005.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
                    <div class="col-md-6 paddings"><a href="{{asset('images/csr/10006.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/10006.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>
                    <div class="col-md-6 paddings"><a href="{{asset('images/csr/10007.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/csr/10007.jpg')}}"
                                 height="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            CSR Activities
                        </div>
                    </div>












                </div>
                <div class="col-md-4 col-xs-12">
                    @include('Parts.right')
                </div>







                <!-- END PROPERTIES ASSIGNED -->

                </div>
                <!-- END MAIN CONTENT -->




            </div>
        </div>
    </div>
@endsection




@section('page_js')
    <script src="js/freewall.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>

@stop