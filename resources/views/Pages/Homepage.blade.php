@extends('structure')
@section('content')

    <!-- BEGIN HOME SEARCH SECTION -->
@include('Parts.Slider')

    <style>

        @media all and (min-width:481px) and (max-width: 568px) {
            .mobile-topform {
                height: 400px;

            }

            .hideonmobile{
                display: none;
            }
        }

        @media all and (min-width:321px) and (max-width: 480px) {
            .mobile-topform {
                height: 400px;
            }
            .hideonmobile{
                display: none;
            }
        }

        @media all and (min-width:0px) and (max-width: 320px) {
            .mobile-topform {
                height: 400px;
            }
            .hideonmobile{
                display: none;
            }
        }

    </style>
    <div class="action-box mobile-topform">
        <div class="container">
            <div class="row">
                <div class="col-md-1">
                </div>
                <div class="col-md-10 mobile-topform">


                    <form

                            role="form" method="post" action="{{ route('homee.quick.enquiry') }}"
                    >
                        {{ csrf_field() }}
                    <div id="newsletter" class="center">
                        <div class="col-md-12">
                            <h3 style="font-size: 35px;"> <strong>Quick Enquiry</strong></h3> <br>
                            <div class="col-md-3">

                                <input type="text" required placeholder="Name" name="name"  class="form-control"  />
                            </div>
                            <div class="col-md-3">

                                <input type="email" placeholder="Email" name="email"  class="form-control"  />

                            </div>
                            <div class="col-md-3">

                                <input type="text" required placeholder="Contact Number" name="mobile"  class="form-control"  />

                            </div>
                            <div class="col-md-3">


                                <input type="text" placeholder="City" name="city"  class="form-control"  />

                            </div>

                        </div>
                        <br>

                        <div class="col-md-12">

                            <div class="col-md-12">

                                <input type="text" placeholder="Message" name="message"  class="form-control"  />



                            </div>



                        </div>
                        <br>

                        <div class="col-md-12">

                            <div class="col-md-12">

                                <div class="col-md-4">


                                </div>

                                <div class="col-md-4">

                                    <button  class="btn btn-default btn-block" type="submit"><span style="font-weight: 700;font-size: 20px;">Send</span></button>

                                </div>

                                <div class="col-md-4">


                                </div>
                            </div>



                        </div>

                    </div>

                    </form>


                </div>

                <div class="col-md-1">
                </div>
               {{-- <div class="col-md-4">
                    <!-- BEGIN SOCIAL NETWORKS -->
                    <ul class="social-networks">
                        <li><a href="#"><i class="fa fa-phone"></i></a></li>
                        <li><h2> +91 8116600714</h2></li>
                    </ul>
                    <!-- END SOCIAL NETWORKS -->
                </div>--}}
            </div>
        </div>
    </div>
    <!-- END ACTION BOX -->
    <style>
        .pull-up-it {
            margin-top: -80px;
        }
    </style>


    @include('Parts.projectoverview');
    <!-- END PROPERTIES SLIDER WRAPPER -->

    <!-- BEGIN CONTENT WRAPPER -->
    <div class="content colored">
        <div class="container">
            <div class="row">

                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-8 pull-up-it">
                    <h1 class="section-title"
                        data-animation-direction="from-bottom"
                        data-animation-delay="50">
                        About Embee Builders
                    </h1>

                    <div class="col-sm-12" data-animation-direction="from-bottom" data-animation-delay="100">
                        <h4 style="color: rgba(24,24,24,0.86); text-align: center; line-height: 25px;"><span style="color: #EA253C; font-weight: 700">
                                MJB Group of Companies</span>
                             entered the real estate sector with the name of Embee Builders in the year 2015. Embee Builders started his journey with its remarkable and extremely successful project Embee Fortune. Embee Fortune became a benchmark for the company as it was fully loaded with all modern amenities and extreme high end quality project but with an affordable rate which was reachable to all sectors of people
                            <a href="about">
                        <button
                        style="background-color: #ff2e5f"
                        >

                            <span style="color: #ffffff; font-weight: 500">Read more</span>

                        </button>
                            </a>
                        </h4>


                    </div>
                    <style>
                        .center {
                           margin-top: -40px;
                        }
                    </style>



                    @include('Parts.member')


                </div>
                <!-- END MAIN CONTENT -->

                <!-- BEGIN SIDEBAR -->
                <div class="sidebar colored col-sm-4">
                    <!-- BEGIN TESTIMONIALS -->
                    <div id="testimonials" class="col-sm-12" data-animation-direction="from-bottom" data-animation-delay="200">
                        <h2 class="section-title">Testimonials</h2>

                        <div id="testimonials-slider" class="owl-carousel testimonials">
                            <div class="item">
                                <blockquote class="text">
                                    <p>
                                        Best Constuction project & Execellent Customer Support in Siliguri, North Bengal
                                    </p>

                                </blockquote>
                                <div class="author">
                                    <img src="images/face.png" alt="" />
                                    <div>
                                        Rishav Pradhan<br>
                                        <span>Gangtok, Sikkim</span>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <blockquote class="text">
                                    <p>
                                        It is a very nice project in whole Siliguri, and we will love to stay and enjoy the place. It will be thankful for us to be a part of the Embee Fortune group. Staff are very nice & friendly.


                                    </p>
                                </blockquote>
                                <div class="author">
                                    <img src="images/face.png" alt="" />
                                    <div>
                                        Susmita & Sishu Shankar<br>
                                        <span>Shushrutanagar, SIliguri</span>
                                    </div>
                                </div>
                            </div>


                            <div class="item">
                                <blockquote class="text">
                                    <p>
                                        The bonding with Embee Fortune to me is for last 5-6 Months since I visited the office of Embee Fortune to know about the project, even before the booking of flat. The bonding is good and all the members of this group are co-operative & sincere

                                    </p>
                                </blockquote>
                                <div class="author">
                                    <img src="images/face.png" alt="" />
                                    <div>
                                        Tanmoy Das<br>
                                        <span>Ramendrapally, Raiganj

</span>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <blockquote class="text">
                                    <p>
                                        I am relatively new regarding the relationship with Embee Fortune as i have booked a flat only two months back. But I found the place really appropriate for me as it is very near to my hotel and I was looking for a place where I could stay with my family somewhere not far away. The place looks very good and I hope everything goes according to the plan. Not to forget the office staff of Embee Fortune who are friendly and well-behaved.



                                    </p>
                                </blockquote>
                                <div class="author">
                                    <img src="images/face.png" alt="" />
                                    <div>
                                        Satish Sebastian
                                        <br>
                                        <span>Durga Mandir, Kadamtala</span>
                                    </div>
                                </div>
                            </div>





                        </div>
                    </div>
                    <!-- END TESTIMONIALS -->



                </div>
                <!-- END SIDEBAR -->

            </div>

        </div>
    </div>
    <!-- END CONTENT WRAPPER -->

    @include('Parts.referalhome')
    @include('Parts.projectupdates')

    <style>
        @media all and (min-width:128px) and (max-width: 768px) {
           .mobile-view {
               margin-top: 50px;
           }
        }


    </style>
    <div class="parallax pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <h1 class="section-title"
                        style="font-weight: 600; color: #ffffff; font-size: 35px; background-color: #EA253B; padding: 10px 10px 10px 10px;">
                        Project Overview</h1>


                    <div class="col-md-4" >
                        <h1 class="section-title"
                            style="color: #ff2e5f"
                            data-animation-direction="from-bottom"
                            data-animation-delay="50">
                            Embee Fortune
                        </h1>
                        <iframe
                                data-animation-direction="from-bottom"
                                data-animation-delay="100"
                                width="100%" height="315" src="https://www.youtube.com/embed/vTzPWUrFDi0?controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="col-md-4 mobile-view">
                        <h1 class="section-title"
                            style="color: #ff2e5f"
                            data-animation-direction="from-bottom"
                            data-animation-delay="50">
                            SQUAREWOOD UTSAB
                        </h1>
                        <iframe
                                data-animation-direction="from-bottom"
                                data-animation-delay="100"
                                width="100%" height="315" src="https://www.youtube.com/embed/8GRny5tKzfE" frameborder="0" allowfullscreen></iframe>



                    </div>
                    <div class="col-md-4 mobile-view">
                        <h1 class="section-title "
                            style="color: #ff2e5f"
                            data-animation-direction="from-bottom"
                            data-animation-delay="50">
                            Embee Delight
                        </h1>
                        <iframe
                                data-animation-direction="from-bottom"
                                data-animation-delay="100"
                                width="100%" height="315" src="https://www.youtube.com/embed/gETjfghif4s" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('page_js')

    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>


@stop