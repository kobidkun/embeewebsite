@extends('structure')
@section('content')

    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">FAQ</h1>

                    <ul class="breadcrumb">
                        <li><a href="/">Home </a></li>
                        <li><a href="#">FAQ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->


    <!-- BEGIN CONTENT WRAPPER -->
    <div class="content">
        <div class="container">
            <div class="row">

                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-12">

                    <h1 class="section-title">Frequently Asked Questions</h1>



                    <!-- BEGIN ACCORDION 2 -->
                    <h3>For NRI</h3>
                    <div id="accordion2" class="panel-group">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" class="collapsed">
                                        Who is a NRI under the provisions of Foreign Exchange Management Act ?


                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Generally, an Indian Citizen who stays abroad for employment/carrying on business or vocation outside India or stays abroad under circumstances indicating an intention for an uncertain duration of stay abroad or a person who is not resident in India for a period over 182 days is a non-resident Indian. Persons posted in U.N. organisations and officials deputed abroad by Central/State Governments and Public Sector undertakings on temporary assignments are also treated as non-residents
                                </div>
                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo2" class="collapsed">
                                        Who is a person of Indian Origin?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Generally, under the provisions of Foreign Exchange Management Act a person of Indian Origin is an individual (other than a citizen of Pakistan, Bangladesh, Sri Lanka, Afghanistan, China, Iran, Nepal, Bhutan) who
                                    <br>
                                    <ul>
                                        <li>At any time held an Indian passport,</li>
                                        <li>He or his father or his grandfather was a citizen of India by virtue of the Constitution of India or Citizenship Act, 1955 (57 of 1955)
                                            Non-resident foreign citizens of Indian Origin are treated on par with non-resident Indian citizens for the purpose of certain facilities.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseThree2" class="collapsed">
                                        Do NRIs and PIOs require permission of Reserve Bank to acquire residential/commercial property in India?


                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree2" class="panel-collapse collapse">
                                <div class="panel-body">
                                </div>NRIs and POIs do not require permission from RBI to acquire residential / commercial premises in India (other than agricultural land/farm house/plantation property). A person resident outside India acquiring property to carry on business from India has to file with the Reserve Bank a declaration in Form IPI within ninety days from the date of acquisition of immovable property. A citizen of Pakistan, Bangladesh, Sri Lanka, Afghanistan, China, Iran, Nepal or Bhutan cannot acquire immovable property without prior permission of Reserve Bank. However, he can take on lease an immovable property for not more than 5 years.


                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseFour2" class="collapsed">
                                        Can NRIs and PIOs sell residential/commercial premises in India without the permission of Reserve Bank?


                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    A person resident outside India who is a citizen of India is permitted to sell immovable property in India other than agricultural/plantation/farm house to a person resident in India or to an NRI or to a PIO resident outside India. He can also gift residential or commercial property in India to a person resident in India, NRI or to a PIO resident outside India. However, he can gift or sale any agricultural land/farmhouse/plantation property only to a person resident in India who is a citizen in India. A PIO resident outside India is permitted to sell the immovable property other than agricultural land/farmhouse/plantation property to a person resident in India
                                </div>
                            </div>
                        </div>



                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseFour3" class="collapsed">
                                        Can the sales proceeds of residential / commercial premises be remitted out of India?


                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    The repatriation of sale proceeds of immovable property other than agricultural land / farmhouse / plantation property may be remitted out of India on fulfilling the following conditions.
                                    <ul>
                                    <li>The immovable property was acquired by the seller in accordance with the provisions of the foreign exchange law in force at the time of acquisition</li>
                                    <li>The amount to be remitted does not exceed (a) the amount paid in foreign exchange for purchase of the immovable property received through normal banking channels or out of funds held in Foreign Currency Non-Resident account or (b) the foreign currency equivalent as on date of payment made for acquisition of property out of funds held in Non Resident External account</li>
                                    <li>The remittance of sale proceeds in case of residential property is restricted only to two properties</li>

                                    </ul>
                                    Reserve Bank has further liberalised the provisions regarding remittance. Accordingly, Authorised Dealers may allow the repatriation of funds out of balances held by NRIs/PIOs in the Non-Resident Ordinary Rupee (NRO) Accounts up to US$ 1,00,000 per year, representing sale proceeds of immovable property, held by them for a period of not less than 10 years subject to payment of applicable taxes.


                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseFour4" class="collapsed">
                                        Can NRIs obtain loans for acquisition of a house/flat for residential purpose from authorised dealers/financial institutions providing housing finance?




                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour4" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Yes. Repayment of loan should be made within a period not exceeding 15 years out of inward remittances or out of funds held in the borrower's NRE/FCNR/NRO accounts.


                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseFour5" class="collapsed">
                                        Can Indian companies grant loans to their NRI staff?




                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour5" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Reserve Bank permits Indian firms/companies to grant housing loans to their employees deputed abroad and holding Indian passports subject to certain conditions.


                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseFour6" class="collapsed">
                                        Can NRIs and PIOs give a Power of Attorney in favour of a person of their choice in India to complete loan formalities on their behalf?




                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour6" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Yes. Normally it is desirable to appoint a Power of Attorney in India to represent you in dealings in India. The Power of Attorney should be executed as per drafts provided by the housing finance company. The Power of Attorney holder should be a trustworthy person.


                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseFour7" class="collapsed">
                                        Can NRIs and PIOs gift residential / commercial premises to relatives / registered charitable trusts / organisations in India ?




                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour7" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Yes. General permission has been granted by Reserve Bank to non-resident persons (foreign citizens) of Indian origin to transfer by way of gift immovable property held by them in India to relatives and charitable trusts/organisations subject to the condition that the provisions of any other laws, including Foreign Contribution (Regulation) Act, 1976 and stamp duty laws, as applicable, are duly complied with.


                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseFour8" class="collapsed">
                                        Can NRIs and PIOs give residential / commercial premises on rent if not required for immediate use?


                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour8" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Yes. Reserve Bank has granted general permission for letting out any immovable property in India. The rental income or proceeds of any investment of such income are eligible for repatriation subject to payment of taxes and production of a certificate issued by a chartered accountant with the guidance of an Authorised Dealer such as a bank for completion of formalities
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseFour9" class="collapsed">
                                        How should NRIs and PIOs make payment of the consideration for residential / commercial property?




                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour9" class="panel-collapse collapse">
                                <div class="panel-body">
                                    The purchase consideration should be met either out of inward remittances in foreign exchange through normal banking channels or out of funds from NRE/FCNR/NRO accounts maintained with banks in India.


                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- END ACCORDION 2 -->
                </div>
            </div>
        </div>
    </div>



@endsection