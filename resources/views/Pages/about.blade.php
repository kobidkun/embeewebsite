@extends('structure')
@section('content')

    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Embee Builders</h1>

                    <ul class="breadcrumb">
                        <li><a href="/">Home </a></li>
                        <li><a href="#">About</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->

    <!-- BEGIN HIGHLIGHT -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="main col-sm-9">
                    <div class="center">
                        <h2 class="section-highlight" data-animation-direction="from-left" data-animation-delay="50">About Embee Builders</h2>
                        <p class="darker-text" data-animation-direction="from-left" data-animation-delay="250">

                            MJB Group of Companies entered the real estate sector with the name of Embee Builders in the year 2015. Embee Builders started his journey with its remarkable and extremely successful project Embee Fortune. Embee Fortune became a benchmark for the company as it was fully loaded with all modern amenities and extreme high end quality project but with an affordable rate which was reachable to all sectors of people...
                        </p>

                        <p class="darker-text" data-animation-direction="from-left" data-animation-delay="450">
                            Embee Builders made a record performance by selling of 60% of its product with in its initial starting phase. Not only its price factors made it very favourable and loveable by the people, but also its customer service and ease of customer end process made it one of the best and reliable brand for people. Embee Builders has also been recognised by CRISIL and has rated “Embee Fortune” with 5 Star Rating.
                        </p>

                        <p class="darker-text" data-animation-direction="from-left" data-animation-delay="650">
                            Embee Builders is a member of CREDAI, making it a trustable brand. Embee Builders has just started its footsteps in the real estate world, and making a new mark in its every step. Embee Builders has now dedicated its love and gracefulness for the people who endorsed them by providing them with Squarewood Utsab, a residential project which is focused only for LIG Category people with the tagline “Desh Ka Sapna, Ghar Ho Apna”, to provide housing for each and every one. It is a social initiative taken by Embee Builders by joining hands with Squarewood Projects Pvt. Ltd, to make the effort accomplishable.
                        </p>

                        <p class="darker-text" data-animation-direction="from-left" data-animation-delay="850">
                            Embee Builders has also started working on its new project “Embee Delight”, which will be an upgraded version of Embee Fortune. Embee Delight is one of the biggest project in North Bengal comprising 530 residential flats. Making the project successful Embee Builders started the project with a joint venture with Manakamna Builders which is also one of the leading Real Estate Sector in North Bengal.
                        </p>

                        <p class="darker-text" data-animation-direction="from-left" data-animation-delay="1050">
                            Embee Builders has a vision to provide people with luxurious homes but at affordable prices. Embee Builders has a dream to be one of the largest Real Estate Firm all across the world and to make this dream come true all the members associated with Embee Builders are working hard each and everyday and keeping the belief of Customer Satisfaction at its priority Embee Builders are set to achieve their goals in its every step.
                        </p>


                     </div>
                </div>

                <div class="col-sm-3">
                   @include('Parts.right')
                </div>
            </div>
        </div>
    </div>
    <!-- END HIGHLIGHT -->




    <!-- BEGIN PROPERTY TYPE BUTTONS -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="section-title" data-animation-direction="from-bottom" data-animation-delay="50">
                        Our Projects</h1>

                    <ul class="property-large-buttons2 clearfix">
                        <li data-animation-direction="from-bottom" data-animation-delay="250">


                            <br>
                            <h4 style="margin-top: 20%">Embee Fortune</h4>
                            <a href="/embee-fortune" class="btn btn-default">View Project</a>

                        </li>
                        <li data-animation-direction="from-bottom" data-animation-delay="450">

                            <h4 style="margin-top: 20%">Squarewood Utsab</h4>
                            <a href="/embee-utsab" class="btn btn-default">View Project</a>
                        </li>
                        <li data-animation-direction="from-bottom" data-animation-delay="650">

                            <h4 style="margin-top: 20%">Embee Delight</h4>
                            <a href="/embee-delight" class="btn btn-default">View Project</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PROPERTY TYPE BUTTONS -->



@endsection