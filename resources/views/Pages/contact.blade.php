@extends('structure')
@section('content')

    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Contact Us</h1>

                    <ul class="breadcrumb">
                        <li><a href="index.html">Home </a></li>
                        <li><a href="contacts.html">Contacts</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->


    <!-- BEGIN CONTENT WRAPPER -->
    <div class="content contacts">
        <div id="contacts_map"></div>



        <div class="container">
            <div class="row">

                <div id="contacts-overlay" class="col-sm-7">
                    <i id="contacts-overlay-close" class="fa fa-minus"></i>

                    <ul class="col-sm-6">
                        <li><i class="fa fa-map-marker"></i> Pusp Niwas, Biswakarma, Khalpara, East Milanpally, 734005</li>
                        <li><i class="fa fa-envelope"></i> <a href="mailto:youremail@domain.com">fortune@embeebuilders.com</a></li>
                    </ul>

                    <ul class="col-sm-6">
                        <li><i class="fa fa-phone"></i> Tel.: 0353 250 1422</li>
                    </ul>
                </div>

                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-4 col-sm-offset-8">
                    <h2 class="section-title">Contact Form</h2>

                    <form
                            role="form" method="post" action="{{ route('contact.page.store') }}"
                    >

                        {{ csrf_field() }}
                        <div class="col-sm-12">
                            <input type="text" name="name" placeholder="Name" class="form-control required fromName" />

                            <input type="email" name="email" placeholder="Email" class="form-control required fromEmail"  />
                            <input type="text" name="mobile" placeholder="Mobile" class="form-control required fromEmail"  />

                            <input type="text" name="subject" placeholder="Subject" class="form-control required subject"  />
                            <textarea name="message" placeholder="Message" class="form-control required"></textarea>
                        </div>

                        <div class="center">
                            <button type="submit" class="btn btn-default-color btn-lg "><i class="fa fa-envelope"></i> Send Message</button>
                        </div>
                    </form>
                </div>
                <!-- END MAIN CONTENT -->

            </div>
        </div>
    </div>
    <!-- END CONTENT WRAPPER -->


@endsection

@section('page_js')
    <script src="js/bootstrap-checkbox.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyC_ShZQW9a2FzWuW1LAjKGOvDwyAoPerBw&callback=initMap" type="text/javascript"></script>
    <script src="js/infobox.min.js"></script>

    <!-- Template Scripts -->
    <script src="js/variables.js"></script>
    <script src="js/scripts.js"></script>
    <script type="text/javascript">
        var singleMarker = [
            {
                "id": 0,
                "title": "Embee Lifespace LLP",
                "latitude": 26.7017672,
                "longitude": 88.4239458,
                "image": "/images/logo.png",
                "description": "Pusp Niwas, Biswakarma, Khalpara, East Milanpally, 734005 <br> Phone: 0353 250 1422",
                "map_marker_icon": "images/markers/coral-marker-cozy.png"
            }
        ];

        (function ($) {
            "use strict";

            $(document).ready(function () {
                //Create contacts map. Usage: Cozy.contactsMap(marker_JSON_Object, map canvas, map zoom);
                Cozy.contactsMap(singleMarker, 'contacts_map', 14);
            });
        })(jQuery);
    </script>
@stop