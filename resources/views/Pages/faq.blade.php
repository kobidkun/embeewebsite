@extends('structure')
@section('content')

    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">FAQ</h1>

                    <ul class="breadcrumb">
                        <li><a href="index.html">Home </a></li>
                        <li><a href="#">Pages</a></li>
                        <li><a href="faq.html">FAQ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->


    <!-- BEGIN CONTENT WRAPPER -->
    <div class="content">
        <div class="container">
            <div class="row">

                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-12">

                    <h1 class="section-title">Frequently Asked Questions</h1>

                    <!-- BEGIN ACCORDION 1 -->
                    <h3>General</h3>
                    <div id="accordion" class="panel-group">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="collapsed">
                                        How much housing loan can one get ?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse">
                                <div class="panel-body">
                                    It depends on your income and repaying capacity. You can add your spouse’s income to increase the amount of loan. Generally banks give 90% loan for flats upto 30 Lac Rupees.
                                </div>
                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed">
                                        Why is it considered necessary to register a property ?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    What is the purpose of registration ? By registering the transaction of an immovable property, it becomes permanent public record. Title or interest can be acquired
                                    only if the deed is registered, and thus further transfer may occur.
                                </div>
                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed">
                                        Eligibility Criteria for Availing subsidy under Pradhan Mantri Awas Yojana (PMAY)
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                        <li>The beneficiary family should comprise of husband, wife and unmarried children</li>
                                        <li>The family should not own a pucca house anywhere in India
                                        </li>
                                        <li>Families having annual income up to INR 6 lacs are entitled to get maximum subsidy of INR 2.67 lacs credited upfront to the loan account of the beneficiaries.</li>
                                        <li>The woman-member of the family has to be either a sole or joint owner</li>
                                        <li>The Income Proof for claiming subsidy would be “Self Declaration” of Economically Weaker Section certificate or Low Income Group certificate.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="collapsed">
                                        What is the reducing balance method of interest payment ?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse">
                                <div class="panel-body">
                                    In reducing balance you reduce the amount of principal payment already paid by you from the initial loan amount. You pay interest only on principal unpaid till that point of
                                    time and not the entire loan amount.
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END ACCORDION 1 -->


                </div>
            </div>
        </div>
    </div>



@endsection