<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>Manage Services Dreamlink</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('dashboard/compiled.min.css')}}" rel="stylesheet">
    @if (($paymentstatus->status) === 'Success')
        <link href="{{ asset('dashboard/extra.css')}}" rel="stylesheet">
    @else
        <link href="{{ asset('dashboard/error.css')}}" rel="stylesheet">
    @endif



    <link href="{{ asset('css/colors/megna.css')}}" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->logo-word.png
    <!-- ============================================================== -->
    <nav class="navbar navbar-default navbar-static-top m-b-0">
        <div class="navbar-header">
            <div class="top-left-part">
                <!-- Logo -->
                <a class="logo" href="index.html">
                    <!-- Logo icon image, you can use font-icon also --><b>
                        <!--This is dark logo icon-->
                        <img src="{{ asset('img/logo.png')}}" alt="home" class="dark-logo"/>
                        <!--This is light logo icon-->
                        <img src="{{ asset('img/logo-small.png')}}" alt="home" class="light-logo"/>
                    </b>
                    <!-- Logo text image you can use text also --><span class="hidden-xs">
                        <!--This is dark logo text-->
                        <img src="{{ asset('img/logo-word.png')}}" alt="home" class="dark-logo"/>
                        <!--This is light logo text-->
                        <img src="{{ asset('img/logo-word.png')}}" alt="home" class="light-logo"/>
                     </span> </a>
            </div>
            <!-- /Logo -->
            <!-- Search input and Toggle icon -->
            <ul class="nav navbar-top-links navbar-left">
                <li><a href="javascript:void(0)" class="open-close waves-effect waves-light"><i class="ti-menu"></i></a>
                </li>
                <li class="dropdown">


                    <!-- /.dropdown-messages -->
                </li>


            </ul>
            <ul class="nav navbar-top-links navbar-right pull-right">
                <li>
                    <form role="search" class="app-search hidden-sm hidden-xs m-r-10">
                        <input type="text" placeholder="Search..." class="form-control"> <a href=""><i
                                    class="fa fa-search"></i></a></form>
                </li>


                <!-- /.dropdown -->
            </ul>
        </div>
        <!-- /.navbar-header -->
        <!-- /.navbar-top-links -->
        <!-- /.navbar-static-side -->
    </nav>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
@include('dashboard.components.left-nav');
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Dashboard</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <a href="#" target="_blank"
                       class="btn btn-primary pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">
                        Get Support</a>
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->

            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="white-box paper" id="printableArea">


                        <div class="movesenter ">

                            @if (($paymentstatus->status) === 'Success')
                                <div class="checkmark-circle ">
                                    <div class="background"></div>
                                    <div class="checkmark draw"></div>
                                </div>
                            @else
                                <div class="col-md-4" style="height:180px">



                                </div>

                                <div class="col-md-4">
                                <img  class="mx-auto d-block"
                                      src="../../../dashboard/images/failed.png"
                                      height="180px" alt="">
                                </div>

                                <div class="col-md-4" style="height:180px">



                                </div>
                            @endif


                        </div>



                        @if (($paymentstatus->status) === 'Success')
                            <h1 class="heading-payment">Thank you !!</h1>
                        @else
                            <h1 class="heading-payment">Oops !!</h1>
                        @endif
                        <h3 class="heading-payment-2">Your Payment status  is

                            <span
                                    class="heading-payment-span"
                            style="font-weight: 700; "
                            >{{$paymentstatus->status}}</span></h3>

                        <div class="row m-t-30 minus-margin">
                            <div class="col-sm-12 col-sm-6 b-t b-r">
                                <ul class="expense-box">
                                    <li>
                                        <i class="ti-check-box text-info"></i><span><h2>Transactional ID</h2><h4> {{$paymentstatus->txnid}}</h4></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-12 col-sm-6  b-t">
                                <ul class="expense-box">
                                    <li>
                                        <i class="ti-money text-info"></i><span><h2>Amount Paid</h2><h4>₹ {{$paymentstatus->amount}}</h4></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row minus-margin">
                            <div class="col-sm-12 col-sm-6  b-t b-r">
                                <ul class="expense-box">
                                    <li>
                                        <i class=" ti-credit-card text-info"></i><span><h2>Card number</h2><h4>{{$paymentstatus->cardnum}}</h4></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-12 col-sm-6  b-t">
                                <ul class="expense-box">
                                    <li>
                                        <i class="fa fa-university text-info"></i><span><h2>Bank ref no.</h2><h4>{{$paymentstatus->bank_ref_num}}</h4></span>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="row minus-margin">
                            <div class="col-sm-12 col-sm-6  b-t b-r">
                                <ul class="expense-box">
                                    <li>
                                        <i class="  ti-user text-info"></i><span><h2>Your Name</h2><h4>{{$paymentstatus->firstname}}</h4></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-12 col-sm-6  b-t">
                                <ul class="expense-box">
                                    <li>
                                        <i class="fa  ti-mobile text-info"></i><span><h2>Mobile Number</h2><h4>{{$paymentstatus->phone}}</h4></span>
                                    </li>
                                </ul>
                            </div>
                        </div>


                    </div>
                    <div class="white-box">
                        @if (($paymentstatus->status) === 'Success')
                            <input class="btn btn-primary btn-block" type="button" onclick="printDiv('printableArea')" value="Print Recipt" />

                        @else
                            <a href="/home">  <button
                                   class="btn btn-danger btn-block"
                                   type="button"
                           >
                               Retry
                           </button> </a>

                        @endif

                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center">&copy; 2017 <img src="{{ asset('img/logo-tiny.png')}}" alt="">Technologies
                Private Limited
            </footer>
        </div>

        <script>
            function printDiv(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;

                document.body.innerHTML = printContents;

                window.print();

                document.body.innerHTML = originalContents;
            }
        </script>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="{{ asset('dashboard/compiled.min.js')}}"></script>

</body>

</html>