@extends('dashboard.admin');
@section('admin')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Dashboard</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <a href="#" target="_blank" class="btn btn-primary pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">
                    Get Support</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div class="white-box analytics-info">
                    <h3 class="box-title">Total Callnow</h3>
                    <ul class="list-inline two-part">
                        <li>
                            <div id="sparklinedash"></div>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-success"></i>
                            <span class="counter text-success">{{$callnow}}</span></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3 col-sm-3 col-xs-12">
                <div class="white-box analytics-info">
                    <h3 class="box-title">Total Side Contact</h3>
                    <ul class="list-inline two-part">
                        <li>
                            <div id="sparklinedash2"></div>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-purple"></i>
                            <span class="counter text-purple">{{$contactus}}</span></li>
                    </ul>
                </div>
            </div>


            <div class="col-lg-3 col-sm-3 col-xs-12">
                <div class="white-box analytics-info">
                    <h3 class="box-title">Contact Page</h3>
                    <ul class="list-inline two-part">
                        <li>
                            <div id="sparklinedash3"></div>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-info"></i>
                            <span class="counter text-info">{{$contactpage_count}}</span></li>
                    </ul>
                </div>
            </div>


            <div class="col-lg-3 col-sm-3 col-xs-12">
                <div class="white-box analytics-info">
                    <h3 class="box-title">Total Visit</h3>
                    <ul class="list-inline two-part">
                        <li>
                            <div id="sparklinedash4"></div>
                        </li>
                        <li class="text-right"><i class="ti-arrow-down text-danger"></i>
                            <span class="text-danger">{{$visitor}}</span></li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-lg-8 col-sm-12">
                    <div class="white-box">

                      {{--  <div class="row sales-report">
                            <div class="col-md-8 col-sm-6 col-xs-6">
                                <h2>March 2017</h2>
                                <p>SALES REPORT</p>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-6 ">
                                <h1 class="text-right text-info m-t-20">$3,690</h1> </div>
                        </div>--}}
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Mobile Number</th>
                                    <th>Name</th>
                                    <th>Recieved From</th>
                                    <th>DATE</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($callnow2 as $call)
                                    <tr>
                                        <td>{{$call->id}}</td>
                                        <td class="txt-oflo">{{$call->mobile}}</td>
                                        <td class="txt-oflo">N/A</td>
                                        <td><span class="label label-success label-rouded">Chat</span> </td>
                                        <td class="txt-oflo">{{$call->created_at}}</td>

                                    </tr>

                                @endforeach

                                @foreach($contactpage2 as $call)
                                    <tr>
                                        <td>{{$call->id}}</td>
                                        <td class="txt-oflo">{{$call->mobile}}</td>
                                        <td class="txt-oflo">{{$call->name}}</td>
                                        <td><span class="label label-danger label-rouded">Contact Page</span> </td>
                                        <td class="txt-oflo">{{$call->created_at}}</td>

                                    </tr>

                                @endforeach

                                @foreach($contactus2 as $call)
                                    <tr>
                                        <td>{{$call->id}}</td>
                                        <td class="txt-oflo">{{$call->mobile}}</td>
                                        <td class="txt-oflo">{{$call->name}}</td>
                                        <td><span class="label label-info label-rouded">Side Pannel</span> </td>
                                        <td class="txt-oflo">{{$call->created_at}}</td>

                                    </tr>

                                @endforeach





                                </tbody>
                            </table>  </div>
                    </div>
                </div>


                <div class="col-md-12 col-lg-4 col-sm-12">
                    <div class="white-box">
                        <a href="https://www.accuweather.com/en/in/siliguri/191567/weather-forecast/191567" class="aw-widget-legal">

                        </a><div id="awcc1512470376649" class="aw-widget-current"  data-locationkey="191567" data-unit="f" data-language="en-us" data-useip="false" data-uid="awcc1512470376649"></div><script type="text/javascript" src="https://oap.accuweather.com/launch.js"></script>
                    </div>
                    </div>
            </div>



            </div>



</div>
        <!-- /row -->

        <!-- /.row -->

    {{--
    @foreach($callnow2 as $call)
                    <tr>
                        <td>1</td>
                        <td class="txt-oflo">{{$call->mobile}}</td>
                        <td><span class="label label-success label-rouded">Chat</span> </td>
                        <td class="txt-oflo">{{$call->created_at}}</td>

                    </tr>

                    @endforeach
    --}}
        <!-- .row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>

    @endsection

@section('page_js')
    <script src="{{asset('dashboard/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('dashboard/jquery.charts-sparkline.js')}}"></script>
@stop