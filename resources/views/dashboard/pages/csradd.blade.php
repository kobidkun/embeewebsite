@extends('dashboard.admin');
@section('admin')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Dashboard</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <a href="#" target="_blank" class="btn btn-primary pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">
                    Get Support</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="white-box">
                    <form
                            class="form-material form-horizontal"
                            role="form" method="post" action="{{ route('create.updates') }}"
                    >
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="col-md-12">Date </label>
                            <div class="col-md-12">
                                <input type="date"
                                       name="date"
                                       class="form-control form-control-line"
                                > </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">Project </label>
                            <div class="col-md-12">
                                <select id="search_bedrooms" name="project" data-placeholder="Flat Type">
                                    <option value="Fortune">Embee Fortune</option>
                                    <option value="Delight">Embee Delight</option>
                                    <option value="Utsab">Sqiarewood Utsab</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">Youtube Url </label>
                            <div class="col-md-12">
                                <input type="text" required name="url" class="form-control form-control-line" ></div>
                        </div>







                        <button class="btn btn-primary btn-block"><i class="fa fa-credit-card-alt"></i> Save</button>
                    </form>

                </div>
            </div>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>

    @endsection