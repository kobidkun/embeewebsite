@extends('dashboard.admin');
@section('admin')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Contact Page Queries</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <a href="#" target="_blank" class="btn btn-primary pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">
                    Get Support</a>
                <ol class="breadcrumb">
                    <li><a href="#">Contact Page Queries</a></li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">




                    <div class="panel-heading">MANAGE USERS</div>
                    <div class="table-responsive">
                        <table class="table table-hover manage-u-table">
                            <thead>
                            <tr>
                                <th width="70" class="text-center">#</th>
                                <th>NAME</th>
                                <th>EMAIL</th>
                                <th>MOBILE</th>
                                <th width="250">PROJECT</th>
                                <th width="300">FLATTYPE</th>
                                <th>DATE</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($contactdatas as $contact)

                            <tr>

                                <td class="text-center">{{ $contact->id }}</td>
                                <td>{{ $contact->name }}

                                <td>{{ $contact->email }}

                                <td>{{ $contact->mobile }}

                                <td>
                                    {{ $contact->project }}

                                <td>
                                    {{ $contact->flattype }}
                                </td>

                                <td>
                                    {{ $contact->created_at }}
                                </td>

                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /row -->

        <!-- /.row -->
        <!-- .row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>

    @endsection