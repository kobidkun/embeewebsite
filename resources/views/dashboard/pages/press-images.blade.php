@extends('dashboard.admin');
@section('admin')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Dashboard</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <a href="#" target="_blank" class="btn btn-primary pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">
                    Get Support</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="white-box">
                    <form
                            class="form-material form-horizontal"
                            role="form" method="post" action="{{ route('admin.press.store') }}"
                    >
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="col-md-12">Heading </label>
                            <div class="col-md-12">
                                <input type="text"
                                       name="heading"
                                       value="{{$flights->heading}}"
                                       class="form-control form-control-line"
                                > </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">Body </label>
                            <div class="col-md-12">
                                <input type="text"
                                       name="body"
                                       value="{{$flights->body}}"
                                       class="form-control form-control-line"
                                > </div>
                        </div>











                        <button class="btn btn-primary btn-block"><i class="fa fa-credit-card-alt"></i> Save</button>
                    </form>

                </div>
            </div>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>

    @endsection