<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
        <div class="user-profile">
            <div class="dropdown user-pro-body">
                <div><img src="../../../../images/face.png" alt="user-img" class="img-circle"></div>
                <a href="#" class="dropdown-toggle u-dropdown"
                   data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="caret"></span></a>
                <ul class="dropdown-menu animated flipInY">
                    <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                    <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ url('/logout') }}" onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i> Logout
                        </a></li>


                </ul>
            </div>
        </div>
        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
        <ul class="nav" id="side-menu">

            <li> <a href="/adminpannel" class="waves-effect "><i data-icon="Z" class="linea-icon linea-basic fa-fw"></i>
                    <span class="hide-menu"> Home</span></a>


            </li>

            <li> <a href="#" class="waves-effect "><i data-icon="<" class="linea-icon linea-basic fa-fw"></i>
                    <span class="hide-menu"> Project Updates <span class="fa arrow"></span> </span></a>

                <ul class="nav nav-second-level">
                    <li> <a href="/dashboard2"><i class=" fa-fw">1</i><span class="hide-menu">Embee Fortune</span></a> </li>
                    <li> <a href="/projectupdates/delight"><i class=" fa-fw">2</i><span class="hide-menu">Embee Delight</span></a> </li>
                    <li> <a href="/projectupdates/utsab"><i class=" fa-fw">3</i><span class="hide-menu">Squarewood Utsab</span></a> </li>
                </ul>


            </li>

            <li> <a href="{{route('dashboard.pages.csr.csr')}}" class="waves-effect "><i data-icon="&#xe005;" class="linea-icon linea-basic fa-fw"></i>
                    <span class="hide-menu"> CSR Updates</span></a>

            </li>


            <li> <a href="{{route('dashboard.pages.live.live')}}" class="waves-effect "><i data-icon="&#xe005;" class="linea-icon linea-basic fa-fw"></i>
                    <span class="hide-menu"> Live Updates</span></a>

            </li>


            <li> <a href="/admin/callnow" class="waves-effect "><i data-icon="-" class="linea-icon linea-basic fa-fw"></i>
                    <span class="hide-menu"> Call now View Querries</span></a>

            </li>

            <li> <a href="/referview" class="waves-effect "><i data-icon="&#xe002;" class="linea-icon linea-basic fa-fw"></i>
                    <span class="hide-menu">Referral</span></a>

            </li>





            <li> <a href="/admin/contactpage" class="waves-effect "><i data-icon="&#xe01f;" class="linea-icon linea-basic fa-fw"></i>
                    <span class="hide-menu">Contact Page Queries</span></a>

            </li>

            <li> <a href="/admin/sidecontact" class="waves-effect "><i data-icon="&#xe01f;" class="linea-icon linea-basic fa-fw"></i>
                    <span class="hide-menu">Side Page Queries</span></a>

            </li>

            <li> <a href="/home-quick-enquiry" class="waves-effect "><i data-icon="&#xe01f;" class="linea-icon linea-basic fa-fw"></i>
                    <span class="hide-menu">Quick Home Queries</span></a>

            </li>




        </ul>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Left Sidebar -->