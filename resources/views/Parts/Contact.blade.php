<h1 class="section-title">Builder Information</h1>
<!-- BEING AGENT INFORMATION -->
<div class="property-agent-info">
    <div class="agent-detail col-md-5">
        <div class="image">
            <img alt="" src="images/logo.png" />
        </div>

        <div class="info">
            <header>
                <h3>Embee Builders, <small>Siliguri</small></h3>
            </header>

            <ul class="contact-us">
                <li><i class="fa fa-envelope"></i><a href="mailto:fortune@embeebuilders.com">fortune@embeebuilders.com</a></li>
                <li><i class="fa fa-map-marker"></i> Pusp Niwas, Biswakarma Mandir Road
                    Khalpara, Siliguri 734005</li>
                <li><i class="fa fa-phone"></i>+91 8116600714/ 715/ 716</li>
            </ul>
        </div>
    </div>

    <form class="form-style col-md-7"  role="form" method="post" action="{{ route('contact.store') }}">
        {{ csrf_field() }}
        <div class="col-sm-12">
            <input type="text" name="name" placeholder="Name" class="form-control required fromName" />
        </div>

        <div class="col-sm-12">
            <input type="email" name="email" placeholder="Email" class="form-control required fromEmail"  />
        </div>

        <div class="col-sm-12">
            <input type="email" name="email" placeholder="Mobile" class="form-control required fromEmail"  />
        </div>

        <div class="col-sm-12">
            <input type="text" name="mobile" placeholder="Mobile Number" class="form-control required subject"  />
            <textarea name="Message" placeholder="Message" class="form-control required"></textarea>
        </div>

        <div class="center">
            <button type="submit" class="btn btn-default-color submit_form"><i class="fa fa-envelope"></i> Send Message</button>
        </div>
    </form>
</div>
<!-- END AGENT INFORMATION -->