{{--
<div class="parallax pattern-bg grid-style1 clearfix " data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12" style="">
                <h1 class="section-title"
                    style="font-weight: 600; color: #ffffff; font-size: 35px; background-color: #EA253B; padding: 10px 10px 10px 10px;">
                    Construction Updates</h1>
                <div class="col-md-4" >



                        <div class="item col-md-4"><!-- Set width to 4 columns for grid view mode only -->
                            <div class="image">
                                <a href="#">
                                    <span class="btn btn-default"><i class="fa fa-file-o"></i> Read More</span>
                                </a>
                                <img src="images/homepage/2.jpg" alt="" />
                            </div>

                            <div class="info-blog">
                                <ul class="top-info">
                                    <li><i class="fa fa-calendar"></i> Nov 29, 2017</li>
                                </ul>
                                <h3>
                                    <a href="#"> Embee Fortune</a>
                                </h3>

                            </div>
                        </div>



                </div>
                <div class="col-md-4 mobile-view">

                    <div class="item col-md-12"><!-- Set width to 4 columns for grid view mode only -->
                        <div class="image">
                            <a href="/">
                                <span class="btn btn-default"><i class="fa fa-file-o"></i> Read More</span>
                            </a>
                            <img src="/images/homepage/3.jpg" alt="" />
                        </div>

                        <div class="info-blog">
                            <ul class="top-info">
                                <li><i class="fa fa-calendar"></i> Nov 17, 2017</li>
                            </ul>
                            <h3>
                                <a href="#"> Squarewood Utsab</a>
                            </h3>

                        </div>
                    </div>


                </div>
                <div class="col-md-4 mobile-view">

                    <div class="item col-md-12"><!-- Set width to 4 columns for grid view mode only -->
                        <div class="image">
                            <a href="#">
                                <span class="btn btn-default"><i class="fa fa-file-o"></i> Read More</span>
                            </a>
                            <img src="/images/homepage/1.jpg" alt="" />
                        </div>

                        <div class="info-blog">
                            <ul class="top-info">
                                <li><i class="fa fa-calendar"></i> Nov 21, 2017</li>
                            </ul>
                            <h3>
                                <a href="#">Embee Delight</a>
                            </h3>

                        </div>



                </div>
            </div>
        </div>
    </div>
</div>
</div>--}}


<div class="grid-style1 clearfix" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="section-title"
                    style="font-weight: 600; color: #ffffff; font-size: 35px; background-color: #EA253B; padding: 10px 10px 10px 10px;">
                    Construction Updates</h1>

                {{--ref program--}}


                {{--project updates--}}
                <div class="item col-md-4"><!-- Set width to 4 columns for grid view mode only -->
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/{{ $f->url }}"
                            frameborder="0" allowfullscreen></iframe>

                    <div class="info-blog">
                        <ul class="top-info">
                            <li><i class="fa fa-calendar"></i> {{$f->date}}</li>
                        </ul>
                        <h3>
                            <a href="/projectupdates"> Embee Fortune</a>
                        </h3>

                    </div>
                </div>


                <div class="item col-md-4"><!-- Set width to 4 columns for grid view mode only -->
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/{{ $u->url }}"
                            frameborder="0" allowfullscreen></iframe>

                    <div class="info-blog">
                        <ul class="top-info">
                            <li><i class="fa fa-calendar"></i> {{$u->date}}</li>
                        </ul>
                        <h3>
                            <a href="/utsab-projectupdates"> Squarewood Utsab</a>
                        </h3>

                    </div>
                </div>


                <div class="item col-md-4"><!-- Set width to 4 columns for grid view mode only -->
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/{{ $d->url }}"
                            frameborder="0" allowfullscreen></iframe>

                    <div class="info-blog">
                        <ul class="top-info">
                            <li><i class="fa fa-calendar"></i> {{$d->date}}</li>
                        </ul>
                        <h3>
                            <a href="/delight-projectupdates">Embee Delight</a>
                        </h3>

                    </div>
                </div>




            </div>
        </div>
    </div>
</div>
