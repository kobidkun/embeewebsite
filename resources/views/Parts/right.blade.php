<!-- BEGIN SIDEBAR -->
<div class="sidebar gray col-md-12 col-sm-12">

    <!-- BEGIN ADVANCED SEARCH -->
    <h2 class="section-title">Enquire Now</h2>
    <form
            role="form" method="post" action="{{ route('contact.store') }}"
    >

        {{ csrf_field() }}
        <div class="form-group">

            <div class="col-sm-12">
                <input type="text"  required class="form-control" name="name" placeholder="Your Name">
                <input type="email"  class="form-control" name="email" placeholder="Your Email">
                <input type="number" required class="form-control" value="+91 " name="mobile" placeholder="Your Mobile number">

            </div>

            <div class="col-md-12">
                <select  name="project" data-placeholder="Project Name">
                    <option value=""> </option>
                    <option value="Embee Fortune">Embee Fortine</option>
                    <option value="Embee Delight">Embee Delight</option>
                    <option value="Squarewood Utsab">Squarewood Utsab</option>
                </select>
            </div>
            <div class="col-sm-12">
                <select  name="flattype" data-placeholder="Flat Type">
                    <option value=""> </option>
                    <option value="1 BHK">1 BHK</option>
                    <option value="2 BHK">2 BHK</option>
                    <option value="3 BHK NORMAL">3 BHK NORMAL</option>
                    <option value="3 BHK SPACIOUS">3 BHK SPACIOUS</option>
                </select>


            </div>

            <div class="col-sm-12">

                <input type="text" required class="form-control"  name="message" placeholder="Message">

            </div>


            <div class="col-sm-12">


            </div>



            <p>&nbsp;</p>
            <p class="center">
                <button type="submit" class="btn btn-default-color">Submit </button>
            </p>
        </div>
    </form>
    <!-- END ADVANCED SEARCH -->



</div>
<!-- END SIDEBAR -->