<!-- BEGIN FOOTER -->
<footer id="footer">
    <div id="footer-top" class="container">
        <div class="row">
            <div class="block col-sm-2">
                <a href="/"><img src="{{asset('images/logo.png')}}" alt="Cozy Logo" /></a>
                <br><br>

            </div>
            <div class="block col-sm-3">
                <h3>Contact Info</h3>
                <ul class="footer-contacts">
                    <li><i class="fa fa-map-marker"></i> Khalpara, Siliguri 734005</li>
                    <li><i class="fa fa-phone"></i>8116600714/ 715/ 716</li>
                    <li><i class="fa fa-envelope"></i> <a href="mailto:email@yourbusiness.com">fortune@embeebuilders.com</a></li>
                </ul>
            </div>
            <div class="block col-sm-3">
                <h3>Helpful Links</h3>
                <ul class="footer-links">
                    <li><a href="/embee-builders">Embee Fortune</a></li>
                    <li><a href="/embee-delight">Embee Delight</a></li>
                    <li><a href="/embee-utsab">Squarewood Utsab</a></li>
                </ul>
            </div>
            <div class="block col-sm-4">

                <ul class="footer-listings">
                    <div class="fb-page" data-href="https://www.facebook.com/embeebuildersofficial/"
                         data-tabs="timeline"
                         data-width="350"
                         data-height="270"
                         data-small-header="false"
                         data-adapt-container-width="true"
                         data-hide-cover="false"
                         data-show-facepile="true"><blockquote cite="https://www.facebook.com/embeebuildersofficial/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/embeebuildersofficial/">Embee Builders</a></blockquote></div>
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11';
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                </ul>
            </div>
        </div>
    </div>


    <!-- BEGIN COPYRIGHT -->
    <div id="copyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    &copy; 2016 Embeebuilders. All rights reserved.

                 


                    <!-- BEGIN SOCIAL NETWORKS -->
                    <ul class="social-networks">



                        <li><a href="https://www.facebook.com/embeebuildersofficial/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/embeebuilders" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCIBCPev9bMnICt8IR7EVjXw" target="_blank"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="blog" target="_blank"><i class="fa fa-rss"></i></a></li>
                    </ul>
                    <!-- END SOCIAL NETWORKS -->

                </div>
                <br>
                <br>

            </div>
        </div>
    </div>
    <!-- END COPYRIGHT -->

</footer>
<!-- END FOOTER -->