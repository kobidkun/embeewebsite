<!-- BEGIN HEADER -->
<header id="header">
    <div id="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul id="top-info">
                        <li>Phone: 8116600714/ 715/ 716</li>
                        <li>Email: <a href="mailto:fortune@embeebuilders.com">fortune@embeebuilders.com</a></li>
                    </ul>

                    <ul id="top-buttons">
                        <li><a href="https://www.facebook.com/embeebuildersofficial/" target="_blank"><i
                                        class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/embeebuilders" target="_blank"><i
                                        class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCIBCPev9bMnICt8IR7EVjXw" target="_blank"><i
                                        class="fa fa-youtube"></i></a></li>
                        <li><a href="/blog" target="_blank"><i class="fa fa-rss"></i></a></li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div id="nav-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <a href="/" class="nav-logo"><img src="{{asset('images/logo.png')}}" alt="Cozy Logo"/></a>


                    @include('Parts.mainmenu')

                </div>
            </div>
        </div>
    </div>
</header>
<!-- END HEADER -->
<style>
    .flootingform {
        position: fixed;
        -moz-border-radius-topleft: 10px;
        -moz-border-radius-topright: 5px;
        height: 32px;
        width: 240px;
        background: #ea253b;
        z-index: 100;
        bottom: 0;
        right: 10px;
        border-radius: 25px 25px 0px 0px;

    }

    .flootingform p a{
        margin-left: 27px;
        margin-top: 5px;!important;
        text-align: center;
        align-content: center;
        align-items: center;
        color: #ffffff;!important;
        font-size: 18px;
        font-weight: 500;
        text-decoration: none;

    }

    .modalpopup {
        z-index: 1000;
        margin-top: 100px;

    }

    .flootingformright {
        position: fixed;
        -moz-border-radius-topleft: 10px;
        -moz-border-radius-topright: 5px;
        height: 40px;
        width: 200px;
        background: #ea253b;
        z-index: 100;
        bottom: 55%;
        left: -90px;
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);


    }

    .flootingformright p a{
        margin-left: 27px;
        margin-top: 5px;!important;
        text-align: center;
        align-content: center;
        align-items: center;
        color: #ffffff;!important;
        font-size: 18px;
        font-weight: 500;
        text-decoration: none;

    }

    .modal-backdrop {
        z-index: 30;
    }

</style>


<div class="flootingform">
    <div style="height: 3px" ></div>
    <p><a data-toggle="modal" href="#" data-target="#myModal">

            <i class="fa fa-phone" aria-hidden="true"></i> REQUEST CALLBACK</a></p>
</div>


{{--
<div class="flootingformright">
    <div style="height: 3px" ></div>
    <p><a data-toggle="modal" href="#" data-target="#myModal2">

                 LEAVE A MESSAGE</a></p>
</div>--}}




<div id="myModal" class="modal modalpopup " role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content modalpopup">
            <div class="modal-header" style="background-color: #ff4961">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color: #ffffff;">Call Back Request</h4>
            </div>
            <form

                    role="form" method="post" action="{{ route('call.savedata') }}"
            >
                <div class="modal-body">

                    {{ csrf_field() }}
                    <input required type="text" name="mobile" placeholder="Your Mobile Number" class="form-control required"/>
                </div>
                <div class="modal-footer">
                    <button type="submit"
                            class="btn btn-default-color btn-lg">
                        <i class="fa fa-envelope"></i> Request Callback
                    </button>


                </div>
            </form>
        </div>


    </div>
</div>




<div id="myModal2" class="modal modalpopup " role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content modalpopup">
            <div class="modal-header" style="background-color: #ff4961">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color: #ffffff;">Call Back Request</h4>

            </div>
            <form
                    role="form" method="post" action="{{ route('contact.store') }}"
            >

                {{ csrf_field() }}
                <div class="form-group">

                    <div class="col-sm-12">
                        <input type="text"  required class="form-control" name="name" placeholder="Your Name">
                        <input type="email"  class="form-control" name="email" placeholder="Your Email">
                        <input type="text" required class="form-control" value="+91 " name="mobile" placeholder="Your Mobile number">

                    </div>

                    <div class="col-md-12">
                        <select id="search_minarea" name="project" data-placeholder="Project Name">
                            <option value=""> </option>
                            <option value="Embee Fortine">Embee Fortine</option>
                            <option value="Embee Delight">Embee Delight</option>
                            <option value="Squarewood Utsab">Squarewood Utsab</option>
                        </select>
                    </div>
                    <div class="col-sm-12">
                        <select id="search_bedrooms" name="flattype" data-placeholder="Flat Type">
                            <option value=""> </option>
                            <option value="1 BHK">1 BHK</option>
                            <option value="2 BHK">2 BHK</option>
                            <option value="3 BHK NORMAL">3 BHK NORMAL</option>
                            <option value="3 BHK SPACIOUS">3 BHK SPACIOUS</option>
                        </select>


                    </div>

                    <div class="col-sm-12">

                        <input type="text" required class="form-control"  name="message" placeholder="Message">

                    </div>

                    <div class="col-sm-12">


                    </div>



                    <p>&nbsp;</p>
                    <p class="center">
                        <button type="submit" class="btn btn-default-color">Submit </button>
                    </p>
                </div>
            </form>
        </div>


    </div>
</div>
