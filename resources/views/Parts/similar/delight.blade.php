<style>
    .up-image h3{
        margin-bottom: 200px;
    }

    .up-image span{
        margin-bottom: 200px;
    }

    .worddown {
        margin-top: 250px
    }

</style>

<!-- BEGIN SIMILAR PROPERTIES -->
<h1 class="section-title">Our Other Projects</h1>
<div id="similar-properties" class="grid-style1 clearfix">
    <div class="row">

        <div class="item col-md-6">
            <div class="image">
                <a href="/embee-builders" class="up-image">
                    <h3>Embee Fortune</h3>
                    <span class="location">Shivmandir, Siliguri</span>
                </a>
                <img src="{{asset('images/similar/fortune.jpg')}}" alt="" />
            </div>
            <div class="price worddown">

                <span>Embee Fortune</span>
            </div>

        </div>
          <div class="item col-md-6">
            <div class="image">
                <a href="/embee-utsab" class="up-image">
                    <h3>Squarewood Utsab</h3>
                    <span class="location">Shivmandir, Siliguri</span>
                </a>
                <img src="{{asset('images/similar/utsab.jpg')}}" alt="" />
            </div>
            <div class="price worddown">
                <span>Squarewood Utsab</span>
            </div>

        </div>


    </div>
</div>
