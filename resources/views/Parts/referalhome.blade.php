<div class="parallax pattern-bg" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">




                {{--ref program--}}
                <div class="col-md-12" >
                    <h1 class="section-title"
                        style="color: #ff2e5f"
                        data-animation-direction="from-bottom"
                        data-animation-delay="50">
                        Embee Builders Referral Program
                    </h1>

                    <h4
                        style="color: #ff2e5f; font-style: italic"
                        data-animation-direction="from-bottom"
                        data-animation-delay="70" >
                        " Starting from 1st Jan 2018 "
                     </h4>
                    <div
                            data-animation-direction="from-bottom"
                            data-animation-delay="100"
                           >
                        <style>
                            .sq-grid {
                                margin: 20px 0 0 0;
                                padding: 0;
                                list-style: none;
                                display: block;
                                text-align: center;
                                width: 100%;
                            }

                            .sq-grid:after,
                            .sq-item:before {
                                content: '';
                                display: table;
                            }

                            .sq-grid:after {
                                clear: both;
                            }

                            .sq-grid li {
                                width: 200px;
                                height: 200px;
                                display: inline-block;
                                margin: 20px;
                            }

                            .sq-item {
                                width: 100%;
                                height: 100%;
                                border-radius: 50%;
                                position: relative;
                                cursor: default;
                                box-shadow:
                                        inset 0 0 0 16px rgba(234, 37, 59),
                                        0 1px 2px rgba(234, 37, 59, 0.8);

                                -webkit-transition: all 0.4s ease-in-out;
                                -moz-transition: all 0.4s ease-in-out;
                                -o-transition: all 0.4s ease-in-out;
                                -ms-transition: all 0.4s ease-in-out;
                                transition: all 0.4s ease-in-out;
                            }

                            .sq-img-1 {
                                background-image: url(images/referal/ref.jpg);
                            }



                            .sq-info {
                                position: absolute;
                                background: rgba(234, 37, 59, 0.8);
                                width: inherit;
                                height: inherit;
                                border-radius: 50%;
                                opacity: 0;

                                -webkit-transition: all 0.4s ease-in-out;
                                -moz-transition: all 0.4s ease-in-out;
                                -o-transition: all 0.4s ease-in-out;
                                -ms-transition: all 0.4s ease-in-out;
                                transition: all 0.4s ease-in-out;

                                -webkit-transform: scale(0);
                                -moz-transform: scale(0);
                                -o-transform: scale(0);
                                -ms-transform: scale(0);
                                transform: scale(0);

                                -webkit-backface-visibility: hidden;

                            }

                            .sq-info h3 {
                                color: #fff;
                                text-transform: uppercase;
                                letter-spacing: 2px;
                                font-size: 22px;
                                margin: 0 30px;
                                padding: 45px 0 0 0;
                                height: 140px;
                                font-family: 'Open Sans', Arial, sans-serif;
                                text-shadow:
                                        0 0 1px #fff,
                                        0 1px 2px rgba(0,0,0,0.3);
                            }

                            .sq-info p {
                                color: #fff;
                                padding: 10px 5px;
                                font-style: italic;
                                margin: 0 30px;
                                font-size: 12px;
                                border-top: 1px solid rgba(255,255,255,0.5);
                                opacity: 0;
                                -webkit-transition: all 1s ease-in-out 0.4s;
                                -moz-transition: all 1s ease-in-out 0.4s;
                                -o-transition: all 1s ease-in-out 0.4s;
                                -ms-transition: all 1s ease-in-out 0.4s;
                                transition: all 1s ease-in-out 0.4s;
                            }

                            .sq-info p a {
                                display: block;
                                color: #fff;
                                color: rgba(255,255,255,0.7);
                                font-style: normal;
                                font-weight: 700;
                                text-transform: uppercase;
                                font-size: 9px;
                                letter-spacing: 1px;
                                padding-top: 4px;
                                font-family: 'Open Sans', Arial, sans-serif;
                            }

                            .sq-info p a:hover {
                                color: #fff222;
                                color: rgba(255,242,34, 0.8);
                            }

                            .sq-item:hover {
                                box-shadow:
                                        inset 0 0 0 1px rgba(255,255,255,0.1),
                                        0 1px 2px rgba(0,0,0,0.1);
                            }
                            .sq-item:hover .sq-info {
                                -webkit-transform: scale(1);
                                -moz-transform: scale(1);
                                -o-transform: scale(1);
                                -ms-transform: scale(1);
                                transform: scale(1);
                                opacity: 1;
                            }

                            .sq-item:hover .sq-info p {
                                opacity: 1;
                            }</style>

                        <ul class="sq-grid">
                            <li>
                                <div class="sq-item sq-img-1">
                                    <div class="sq-info">
                                        <h3>EMBEE BUILDERS REFERRAL PROGRAM</h3>
                                        <a href="/referal-programme">
                                            <span class="btn btn-default"><i class="fa fa-file-o"></i> Read More</span>
                                        </a>
                                    </div>
                                </div>
                            </li>


                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>