<div class="parallax pattern-bg" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="section-title" data-animation-direction="from-bottom" data-animation-delay="50">Our Projects</h1>

                <div  data-animation-direction="from-bottom" data-animation-delay="250">



                        <div id="assigned-properties" class="grid-style1 clearfix">
                            <div class="row">
                                <div class="item col-md-4">
                                    <div class="image">

                                        <img src="images/homepage/delight2.jpg" alt="" />
                                    </div>
                                    <div class="price">
                                        <i class="fa fa-home"></i>Project
                                        <span>Embee Delight</span>
                                    </div>
                                    <ul class="amenities" style="background-color: #ea253b;  height: 200px">
                                      <p style="color: #ffffff">Embee Delight will be an upgraded version of Embee Fortune,
                                          which will cater MIG and Premium MIG Category. The project is
                                          a premium residential project which will be having 530 flats,
                                          consisting basically 3BHK large, 3 BHK normal and 2 BHK flats.</p>
                                        <a href="/embee-delight" class="btn btn-default">Read More</a>
                                    </ul>
                                </div>


                                <div class="item col-md-4">
                                    <div class="image">

                                        <img src="images/homepage/utsab2.jpg" alt="" />
                                    </div>
                                    <div class="price">
                                        <i class="fa fa-home"></i>Project
                                        <span>Squarewood Utsab</span>
                                    </div>
                                    <ul class="amenities" style="background-color: #ea253b; height: 200px">
                                      <p style="color: #ffffff; ">
                                          Under the company Squarewood Projects Pvt. Ltd.
                                          : Squarewood is a joint initiative of highly talented
                                          individuals possessing extensive entrepreneurship spirit,
                                          knowledge, professional expertise and leadership qualities
                                          of running already established..
                                      </p>
                                        <a href="/embee-utsab" class="btn btn-default">Read More</a>
                                    </ul>
                                </div>


                                <div class="item col-md-4">
                                    <div class="image">

                                        <img src="images/homepage/embee2.jpg" alt="" />
                                    </div>
                                    <div class="price">
                                        <i class="fa fa-home"></i>Project
                                        <span>Embee Fortune</span>
                                    </div>
                                    <ul class="amenities" style="background-color: #ea253b; height: 200px">
                                      <p style="color: #ffffff">
                                          Embee Fortune is one of the most renowned and luxurious project with full of Amenities  in Siliguri. Embee Fortune is the first project of Embee Builders.  It has been rated  5-Star Rating by CRISIL, based on its superior quality and infrastructure
                                      </p>
                                        <a style=" bottom: 3px" href="/embee-builders" class="btn btn-default">Read More</a>
                                    </ul>
                                </div>
                            </div>
                        </div>







                </div>

            </div>
        </div>
    </div>
</div>

{{--
            <div class="item">
                        <div class="image">
                            <a href="/embee-builders"></a>
                            <img src="images/homepage/embee.jpg" alt="" />
                        </div>
                        <div class="price">
                            <i class="fa fa-home"></i>Project
                            <span>Embee Fortune</span>
                        </div>
                        <div class="info">
                            <h3><a href="/embee-builders">Embee Fortune</a></h3>
                            <p>
                                Embee Fortune is one of the most renowned and luxurious project with full of Amenities
                                in Siliguri. Embee Fortune is the first project of Embee Builders.
                                It has been rated  5-Star Rating by CRISIL, based on its superior quality and infrastructure
                            </p>
                            <a href="/embee-builders" class="btn btn-default">Read More</a>
                        </div>
                    </div>

                    <div class="item">
                        <div class="image">
                            <a href="/embee-utsab"></a>
                            <img src="images/homepage/utsab.jpg" alt="" />
                        </div>
                        <div class="price">
                            <i class="fa fa-home"></i>Project
                            <span>Squarewood Utsab</span>
                        </div>
                        <div class="info">
                            <h3><a href="/embee-utsab">Squarewood Utsab</a></h3>
                            <p>
                                Under the company Squarewood Projects Pvt. Ltd. : Squarewood is a joint initiative of highly talented individuals possessing extensive entrepreneurship spirit, knowledge, professional expertise and leadership qualities of running already established Business Houses, along with hands on experience in the Real Estate Industry .
                            </p>
                            <a href="/embee-utsab" class="btn btn-default">Read More</a>
                        </div>
                    </div>
--}}