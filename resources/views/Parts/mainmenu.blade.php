<!-- BEGIN MAIN MENU -->
<nav class="navbar">
    <button id="nav-mobile-btn"><i class="fa fa-bars"></i></button>

    <ul class="nav navbar-nav">
        <li >
            <a class="active" href="/">Home</a>

        </li>

        <li >
            <a href="/about">About</a>

        </li>

        <li class="dropdown">
            <a href="#" data-toggle="dropdown" data-hover="dropdown">Projects<b class="caret"></b></a>
            <ul class="dropdown-menu">

                <li class="dropdown-submenu">
                    <a href="#">Embee Fortune</a>
                    <ul class="dropdown-menu">
                        <li><a href="/embee-builders">Overview</a></li>
                        <li><a href="/embee-builders/amenities">Amenities</a></li>
                        <li><a href="/embee-builders/facilities">Facilities </a></li>
                        <li><a href="/embee-builders/isometric-view">Isometic View</a></li>
                        <li><a href="/embee-builders/specification">Specifications</a></li>
                        <li><a href="/embee-builders/floor-plan"> Floor Plan</a></li>

                    </ul>
                </li>



                <li class="dropdown-submenu">
                    <a href="#">Squarewood Utsab</a>
                    <ul class="dropdown-menu">
                        <li><a href="/embee-utsab">Overview</a></li>
                        <li><a href="/embee-utsab/amenities">Amenities</a></li>
                        <li><a href="/embee-utsab/facilities">Facilities </a></li>
                        <li><a href="/embee-utsab/isometric-view">Isometic View</a></li>
                        <li><a href="/embee-utsab/specification">Specifications</a></li>
                        <li><a href="/embee-utsab/floor-plan"> Floor Plan</a></li>

                    </ul>
                </li>



                <li class="dropdown-submenu">
                    <a href="#">Embee Delight</a>
                    <ul class="dropdown-menu">
                        <li><a href="/embee-delight">Overview</a></li>
                        <li><a href="/embee-delight/amenities">Amenities</a></li>
                        <li><a href="/embee-delight/facilities">Facilities </a></li>
                        <li><a href="/embee-delight/isometric-view">Isometic View</a></li>
                        <li><a href="/embee-delight/specification">Specifications</a></li>
                        <li><a href="/embee-delight/floor-plan"> Floor Plan</a></li>

                    </ul>
                </li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" data-toggle="dropdown" data-hover="dropdown">Project Status<b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a href="/projectupdates">Embee Fortune</a></li>
                <li><a href="/utsab-projectupdates">Squarewood Utsab </a></li>
                <li><a href="/delight-projectupdates">Embee Delight</a></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" data-toggle="dropdown" data-hover="dropdown">Events<b class="caret"></b></a>
            <ul class="dropdown-menu">

                <li >
                    <a href="/csr">CSR Activities</a>

                </li>

                <li >
                    <a href="/referal-programme">Referral Program</a>

                </li>


                <li class="">
                    <a href="/pressevents">Press Release</a>

                </li>

                <li  class="">

                    <a href="/fortune-live-events">Live Events</a>


                </li>

                <li class="">
                    <a href="/embee-builders/testiminial">Client Testimonials</a>


                </li>







            </ul>
        </li>
        <li class="dropdown">
            <a href="#" data-toggle="dropdown" data-hover="dropdown">Downloads & Approval<b class="caret"></b></a>
            <ul class="dropdown-menu">

                <li class="dropdown-submenu">
                    <a href="#">Embee Fortune</a>
                    <ul class="dropdown-menu">
                        <li><a href="/doccuments/fortune/brochure.pdf">Download Brochure</a></li>
                        <li><a href="/doccuments/fortune/aviation.pdf">Aviation Certificate</a></li>
                        <li><a href="/doccuments/fortune/fire-fanction-letter.pdf">Fire Sanction Certificate</a></li>
                        <li><a href="/doccuments/fortune/luc-sanction-letter.pdf">LUC Sanction Letter</a></li>
                        <li><a href="/doccuments/fortune/credai-certificate.pdf">Credai Certificate</a></li>
                        <li><a href="/doccuments/fortune/noc-of-panchayat.pdf">NOC of Panchayat</a></li>
                        <li><a href="/doccuments/fortune/sbi.pdf">SBI Home Loan Approval.</a></li>
                        <li><a href="/doccuments/fortune/embee.pdf">G+7 Approval</a></li>
                        <li><a href="/doccuments/fortune/crisil5.png">CRISIL 5 Star</a></li>

                    </ul>
                </li>



                <li class="dropdown-submenu">
                    <a href="#">Squarewood Utsab</a>
                    <ul class="dropdown-menu">
                        <li><a href="/doccuments/utsab/brochure.pdf">Download Brochure</a></li>

                    </ul>
                </li>



                <li class="dropdown-submenu">
                    <a href="#">Embee Delight</a>
                    <ul class="dropdown-menu">
                        <li><a href="/doccuments/delight/brochure.pdf">Download Brochure</a></li>
                    </ul>
                </li>
            </ul>
        </li>




        <li class="dropdown">
            <a href="#" data-toggle="dropdown" data-hover="dropdown">Faq<b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a href="/faq">General FAQ</a></li>
                <li><a href="/nri-faq">NRI FAQ</a></li>
            </ul>
        </li>


        <li><a href="/contact">Contact Us</a></li>
    </ul>

</nav>
<!-- END MAIN MENU -->