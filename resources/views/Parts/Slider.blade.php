<!-- Revolution Slider CSS settings -->
<style>


    @media (max-width: 480px) {

    }

</style>

<!-- BEGIN HOME SLIDER SECTION -->
<div class="revslider-container">
    <div class="revslider mobileviewslide">
        <ul>
            <li data-transition="fade" data-slotamount="7">
                <img src="/images/slider/delight.jpg" alt="" />
                <div style="background-color: #00A8FF">
                <div class="caption sfr slider-title mobile-slider" data-x="670" data-y="100"
                     data-speed="800" data-start="1300" data-easing="easeOutBack"
                     data-end="9600" data-endspeed="700" data-endeasing="easeInSine" >
                    <span style="background-color: #ea253b; padding: 5px 5px 5px 5px;">Embee Delight</span>
                </div>
                <div class="caption sfl slider-subtitle mobile-slider"
                     data-x="670" data-y="230"
                     data-speed="800" data-start="1500"
                     data-easing="easeOutBack" data-end="9700"
                     data-endspeed="500" data-endeasing="easeInSine"
                >   <span style="background-color: #ea253b; padding: 10px 10px 10px 10px;margin-top: 20px;"> 530 Flats Starting From 19 Lacs </span></div>


                    <a href="/embee-delight" style="border-color: #ea253b; color: #ffffff; background-color: #ea253b"
                       class="caption sfb btn btn-default btn-lg mobile-slider"
                       data-x="670" data-y="300" data-speed="800" data-easing="easeOutBack" data-start="1600" data-end="9800" data-endspeed="500" data-endeasing="easeInSine" >Learn More</a>

                </div>
            </li>

            <li data-transition="fade" data-slotamount="7">
                <img src="images/slider/utsab.jpg" alt="" />
                <div style="background-color: #00A8FF">
                <div class="caption sfr slider-title" data-x="70" data-y="100"
                     data-speed="800" data-start="1300" data-easing="easeOutBack"
                     data-end="9600" data-endspeed="700" data-endeasing="easeInSine" >
                    <span style="background-color: #ea253b; padding: 5px 5px 5px 5px;">Squarewood Utsab</span>
                </div>
                <div class="caption sfl slider-subtitle"
                     data-x="70" data-y="230"
                     data-speed="800" data-start="1500"
                     data-easing="easeOutBack" data-end="9700"
                     data-endspeed="500" data-endeasing="easeInSine"
                >   <span style="background-color: #ea253b; padding: 10px 10px 10px 10px;margin-top: 20px;">
                        All Flats Within 15 Lacs </span></div>


                    <a href="/embee-utsab" style="border-color: #ea253b; color: #ffffff; background-color: #ea253b"
                       class="caption sfb btn btn-default btn-lg"
                       data-x="75" data-y="300" data-speed="800" data-easing="easeOutBack" data-start="1600" data-end="9800" data-endspeed="500" data-endeasing="easeInSine" >Learn More</a>

                </div>
            </li>


            <li data-transition="fade" data-slotamount="7">
                <img src="images/slider/slider-2.jpg" alt="" />
                <div style="background-color: #00A8FF">
                <div class="caption sfr slider-title" data-x="70" data-y="100"
                     data-speed="800" data-start="1300" data-easing="easeOutBack"
                     data-end="9600" data-endspeed="700" data-endeasing="easeInSine" >
                    <span style="background-color: #ea253b; padding: 5px 5px 5px 5px;">Embee Fortune</span>
                </div>
                <div class="caption sfl slider-subtitle"
                     data-x="70" data-y="230"
                     data-speed="800" data-start="1500"
                     data-easing="easeOutBack" data-end="9700"
                     data-endspeed="500" data-endeasing="easeInSine"
                >   <span style="background-color: #ea253b; padding: 10px 10px 10px 10px;margin-top: 20px;">
                        Ready to move from 2018 </span></div>


                    <a href="/embee-builders" style="border-color: #ea253b; color: #ffffff; background-color: #ea253b"
                       class="caption sfb btn btn-default btn-lg"
                       data-x="75" data-y="300" data-speed="800" data-easing="easeOutBack" data-start="1600" data-end="9800" data-endspeed="500" data-endeasing="easeInSine" >Learn More</a>

                </div>
            </li>






        </ul>
    </div>
</div>
<!-- END HOME SLIDER SECTION -->