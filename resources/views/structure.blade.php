
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->


<head>
    <meta charset="utf-8" />

    <!-- Page Title -->
    <title>Welcome to Embee Builders</title>

    <meta name="keywords" content="North Bengal's Leading Real Estate Developer.Affordable Flats with Premium Luxury" />
    <meta name="description" content="North Bengal's Leading Real Estate Developer.Affordable Flats with Premium Luxury." />
    <meta name="author" content="tecion" />

    <!-- Mobile Meta Tag -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" type="image/x-icon" href="fab.png" />
    <link rel="apple-touch-icon" href="images/fab.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="images/fab.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="images/fab.png" />

    <!-- IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Google Web Font -->
    <link href="http://fonts.googleapis.com/css?family=Raleway:300,500,900%7COpen+Sans:400,700,400italic" rel="stylesheet" type="text/css" />

    <!-- Bootstrap CSS -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{asset('rs-plugin/css/settings.css')}}" media="screen" />
    <!-- Template CSS -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet" />
     @yield('page_css')

    <!-- Modernizr -->
    <script src="{{asset('js/modernizr-2.8.1.min.js')}}"></script>
</head>
<body>
<!-- BEGIN WRAPPER -->
<div id="wrapper">

    @include('Parts.Header')


    @yield('content')


    @include('Parts.Footer')

</div>
<!-- END WRAPPER -->


<!-- END TEMPLATE SETTINGS PANEL -->


<!-- Libs -->
<script src="{{asset('js/common.js')}}"></script>

<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/chosen.jquery.min.js')}}"></script>


@yield('page_js')
<!-- jQuery Revolution Slider -->


<!-- Template Scripts -->
<script src="{{asset('js/variables.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>

<!-- Agencies list -->
<script src="{{asset('js/agencies.js')}}"></script>





</body>


</html>