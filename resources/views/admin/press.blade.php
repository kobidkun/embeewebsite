@extends('structure')
@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.7.2/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.7.2/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Embee Fortune</h1>

                    <ul class="breadcrumb">
                        <li><a href="/">Home </a></li>
                        <li><a href="/">Projects</a></li>
                        <li><a href="/embee-builders">Embee Fortune</a></li>
                        <li><a href="#">Specifications</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->

    <div class="content">
        <div class="container">
            <div class="row">

                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-8">
                    <h1 class="section-title">Project Updates</h1>
                    <div id="blog-listing" class="list-style clearfix">



                        @foreach ($projects as $project)



                            <h1 class="blog-title">Project Update on {!! $project->heading !!}
                            </h1>

                            <div class="blog-main-image">


                                {!! $project->body !!}

                 </div>

                            <div class="blog-bottom-info">


                                <div id="post-author"><i class="fa fa-pencil"></i> By Embee Builders</div>
                            </div>
                        @endforeach




                        @include('Projects.Embeebuilders.button')


                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    @include('Parts.right')
                </div>

            </div>
        </div>

        <!-- END PROPERTY LISTING -->
@endsection