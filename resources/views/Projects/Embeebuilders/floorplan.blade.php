@extends('structure')
@section('page_css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css"/>

@stop
@section('content')
    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Embee Fortune</h1>

                    <ul class="breadcrumb">
                        <li><a href="index.html">Home </a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="/embee-builders">Embee Fortune</a></li>
                        <li><a href="/">Floor Plan</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->
    <div class="content">
        <div class="container">
            <div class="row">
                <style>
                    .paddings {
                        padding-top: 10px;
                        padding-bottom: 10px;
                        border: 1px;
                        border-color: rgba(231, 69, 88, 0.89);

                    }
                </style>
                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-12">

                    <div id="property-listing" class="grid-style1 clearfix">
                        <div class="row">

                            <div class="col-md-8 col-xs-12">
                                <h1 class="section-title">AS PER ACTUAL PLAN & MEASUREMENT</h1>
                                <style>
                                    .imgdown {
                                        background-color: #ff4961;
                                        font-size: 22px;
                                        color: #ffffff;
                                        text-align: center;
                                    }

                                    .mainimg {
                                        align-items: center;
                                        align-content: center;
                                    }
                                </style>
                                <div class="col-md-6 paddings"><a href="{{asset('images/projects/embee/floor/1.jpg')}}"
                                                                  data-fancybox
                                                                  data-caption="Aster and Begonia 2nd Floor">

                                        <img class="img-responsive center-block"
                                             src="{{asset('images/projects/embee/floor/1.jpg')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Aster and Begonia 2nd Floor
                                    </div>
                                </div>

                                <div class="col-md-6 paddings"><a href="{{asset('images/projects/embee/floor/1.jpg')}}"
                                                                  data-fancybox data-caption="Camellia Typical Floor">

                                        <img class="img-responsive center-block"
                                             src="{{asset('images/projects/embee/floor/1.jpg')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Camellia Typical Floor
                                    </div>
                                </div>

                                <div class="col-md-6 paddings"><a href="{{asset('images/projects/embee/floor/1.jpg')}}"
                                                                  data-fancybox data-caption="Daffodil TYP Floor">

                                        <img class="img-responsive center-block"
                                             src="{{asset('images/projects/embee/floor/1.jpg')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Daffodil TYP Floor

                                    </div>
                                </div>

                                <div class="col-md-6 paddings"><a href="{{asset('images/projects/embee/floor/1.jpg')}}"
                                                                  data-fancybox data-caption="Orchid Typical Floor">

                                        <img class="img-responsive center-block"
                                             src="{{asset('images/projects/embee/floor/1.jpg')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Orchid Typical Floor
                                    </div>
                                </div>


                                <div class="col-md-6 paddings"><a href="{{asset('images/projects/embee/floor/2.jpg')}}"
                                                                  data-fancybox
                                                                  data-caption="Aster and Begonia Typical 3rd to 7th FLOOR">

                                        <img class="img-responsive center-block"
                                             src="{{asset('images/projects/embee/floor/2.jpg')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Aster and Begonia Typical 3rd to 7th FLOOR
                                    </div>
                                </div>

                                @include('Projects.Embeebuilders.button')
                                @include('Parts.similar.fortune')
                            </div>
                            <div class="col-md-4 col-xs-12">
                                @include('Parts.right')
                            </div>


                        </div>
                    </div>


                    <!-- END PROPERTIES ASSIGNED -->

                </div>
                <!-- END MAIN CONTENT -->


            </div>
        </div>
    </div>
@endsection




@section('page_js')
    <script src="js/freewall.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>

@stop