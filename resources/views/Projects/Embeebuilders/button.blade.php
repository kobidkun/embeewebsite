<style>
    .routing-padding {
        padding-top: 10px;
    }

</style>
<h1 class="section-title">Quick Navigation</h1>
<div class="col-md-4 routing-padding">
    <a href="/embee-builders/amenities"> <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Amenities </button> </a>
</div>

<div class="col-md-4 routing-padding">
    <a href="/embee-builders/facilities"><button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Facilities</button></a>
</div>

<div class="col-md-4 routing-padding">
    <a href="/embee-builders/isometric-view">  <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Isometric View</button> </a>
</div>



<div class="col-md-4 routing-padding">
    <a href="/embee-builders/specification"> <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Specification</button> </a>
</div>

<div class="col-md-4 routing-padding">
    <a href="/embee-builders/floor-plan">   <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Project Plan</button> </a>
</div>

<div class="col-md-4 routing-padding">
    <a href="/embee-builders">
        <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Overview</button> </a>
</div>

<h1 class="section-title"> Download Brochure</h1>
<div class="col-md-12 routing-padding">
    <a href="/doccuments/fortune/brochure.pdf">
        <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Embee Fortune</button> </a>
</div>
<h1 class="section-title">Project Video</h1>
<!-- PROPERTY MAP HOLDER -->
<iframe src="https://www.youtube.com/embed/vTzPWUrFDi0" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe>


<h1 class="section-title">Project Updates</h1>
<!-- PROPERTY MAP HOLDER -->
<iframe src="https://www.youtube.com/embed/N5ncmpiFxS8" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe>
