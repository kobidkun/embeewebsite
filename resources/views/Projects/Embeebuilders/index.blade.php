@extends('structure')
@section('page_css')


    @stop
@section('content')


<!-- BEGIN HOME GRID -->
<div id="home-grid">
    <div id="freewall" class="free-wall">
        <div class="item">
            <a class="info" href="#">

                <h3>AC GYMNASIUM</h3>

            </a>
            <img src="images/projects/embee/facilities/2.jpg" alt="AC GYMNASIUM" />
        </div>
        <div class="item">
            <a class="info" href="#">

                <h3>BADMINTON COURT</h3>

            </a>
            <img src="images/projects/embee/facilities/3.jpg" alt="BADMINTON COURT" />
        </div>
        <div class="item">
            <a class="info" href="#">

                <h3>AERIAL VIEW</h3>
            </a>
            <img src="images/projects/embee/facilities/4.jpg" alt="AERIAL VIEW" />
        </div>
        <div class="item">
            <a class="info" href="#">

                <h3>INDOOR GAMES</h3>

            </a>
            <img src="images/projects/embee/facilities/5.jpg" alt="AERIAL VIEW" />
        </div>
        <div class="item">
            <a class="info" href="#">

                <h3>55% OPEN SPACE</h3>
            </a>
            <img src="images/projects/embee/facilities/6.jpg" alt="55% OPEN SPACE" />
        </div>
        <div class="item">
            <a class="info" href="#">

                <h3>OUTDOOR GAMES</h3>
            </a>
            <img src="images/projects/embee/facilities/7.jpg" alt="OUTDOOR GAMES" />
        </div>
        <div class="item">
            <a class="info" href="#">

                <h3>SWIMMING POOL</h3>
            </a>
            <img src="images/projects/embee/facilities/pool.jpg" alt="SWIMMING POOL" />
        </div>

        <div class="item">
            <a class="info" href="#">

                <h3>LANDSCAPED GARDEN</h3>
            </a>
            <img src="images/projects/embee/facilities/9.jpg" alt="LANDSCAPED GARDEN" />
        </div>
        <div class="item">
            <a class="info" href="#">

                <h3>LANDSCAPED GARDEN</h3>
            </a>
            <img src="images/projects/embee/facilities/10.jpg" alt="LANDSCAPED GARDEN" />
        </div>



    </div>
</div>
<!-- END HOME GRID -->

<!-- BEGIN PAGE TITLE/BREADCRUMB -->
<div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title">Embee Fortune</h1>

                <ul class="breadcrumb">
                    <li><a href="/">Home </a></li>
                    <li><a href="#">Projects</a></li>
                    <li><a href="#">Embee Fortune</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE TITLE/BREADCRUMB -->


<!-- BEGIN CONTENT WRAPPER -->
<div class="content">
    <div class="container">
        <div class="row">

            <!-- BEGIN MAIN CONTENT -->
            <div class="main col-sm-8">

                <h1 class="property-title">Embee Fortune</h1>


                <!-- BEGIN PROPERTY DETAIL SLIDERS WRAPPER -->
                <div id="property-detail-wrapper" class="style1">

                    <div class="price">

                        <span>On the verge of completion</span>
                    </div>

                    <!-- BEGIN PROPERTY DETAIL LARGE IMAGE SLIDER -->
                    <div id="property-detail-large" class="owl-carousel">
                        <div class="item">
                            <img src="/images/projects/embee/index/1.jpg" alt="" />
                        </div>

                        <div class="item">
                            <img src="/images/projects/embee/index/1.jpg" alt="" />
                        </div>

                        <div class="item">
                            <img src="/images/projects/embee/index/2.jpg" alt="" />
                        </div>

                        <div class="item">
                            <img src="/images/projects/embee/index/3.jpg" alt="" />
                        </div>

                        <div class="item">
                            <img src="/images/projects/embee/index/4.jpg" alt="" />
                        </div>

                        <div class="item">
                            <img src="/images/projects/embee/index/5.jpg" alt="" />
                        </div>

                    </div>
                    <!-- END PROPERTY DETAIL LARGE IMAGE SLIDER -->

                    <!-- BEGIN PROPERTY DETAIL THUMBNAILS SLIDER -->
                    <div id="property-detail-thumbs" class="owl-carousel">
                        <div class="item"><img src="/images/projects/embee/index/1.jpg" alt="" /></div>
                        <div class="item"><img src="/images/projects/embee/index/2.jpg" alt="" /></div>
                        <div class="item"><img src="/images/projects/embee/index/3.jpg" alt="" /></div>
                        <div class="item"><img src="/images/projects/embee/index/4.jpg" alt="" /></div>
                        <div class="item"><img src="/images/projects/embee/index/5.jpg" alt="" /></div>
                    </div>
                    <!-- END PROPERTY DETAIL THUMBNAILS SLIDER -->

                </div>

                <p>

                    Embee Fortune is one of the most renowned and luxurious project with full of Amenities  in Siliguri. Embee Fortune is the first project of Embee Builders.  It has been rated  5-Star Rating by CRISIL, based on its superior quality and infrastructure.

                    The project covers 45% Constructed Area and provides 55% Open space. The Open Space is covered by Jogger’s Track, Landscape Garden, Outdoor Kid’s Area and Elderly Sitting Area. The project is an Earthquake Resistant Structural Design Project.

                    Here, ‘The dream of owing a luxurious house is now a Reality’.

                    It provides luxurious Facilities i.e. Swimming Pool with Deck, Badminton Court, AC Community Hall, Indoor Sport Room with Pool Table and Table Tennis, AC Gymnasium,  Yoga Room and Steam & Sauna Room, Café with WI-FI Zone, and Kid’ Activity Centre.

                    The project provides 24 hr Power Back-up, STP (Sewage Treatment Plant) facility, Intercom Facilities, 2 Lift per Block, CCTV cameras, 24 hr water supply, Adequate & Modern Fire Fighting Systems, Free Guest Parking facilities, Convenience store for Daily needs. It is the first initiative of Embee Builders for the advancement and development of North Bengal.

                    Therefore, the project is an extraordinary project with advanced facilities and Amenities.
                    <br>
                    <br>
                    Quick Facts
                <ul>
                <li class="li">Location: SAARC 4 Lane Highway, Kadamtala, Durgamandir, Siliguri</li>
                <li class="li">Land Area: 2.5 Acres</li>
                <li class="li">Total Area: 108,900 sq ft (Approx)</li>
                <li class="li">Total No. of Blocks: 5 Block (G+7)</li>
                <li class="li">Total No. of Flats: 265 Nos</li>
                <li class="li">1BHK Flats,2 BHK Flats and 3 BHK Flats</li>
                </ul>



                </p>


                <style>
                    .property-features  li{

                    }
                </style>


                <h1 class="section-title">Property Features</h1>
                <!-- BEGIN PROPERTY FEATURES LIST -->
                <ul class="property-features col-sm-6">
                    <li><i class="icon-apartment"></i> Attached to 4-Lane Asian Highway</li>
                    <li><i class="icon-pool"></i> Swimming Pool</li>
                    <li><i class="icon-store"></i> Departmental Store</li>
                    <li><i class="icon-security"></i> Earth Quake Resistant Structure</li>
                </ul>

                <ul class="property-features col-sm-6">
                    <li><i class="fa fa-star" ></i> CRISIL Rated 5 Star Project</li>
                    <li><i class="icon-house2"></i> AC Community Hall</li>
                    <li><i class="icon-bathrooms"></i> Steam & Sauna</li>
                    <li><i class="icon-factory"></i> Sewage Treatment Plant</li>
                </ul>
                <!-- END PROPERTY FEATURES LIST -->




                <style>
                    .routing-padding {
                        padding-top: 10px;
                    }

                </style>


                <div class="col-md-4 routing-padding">
                    <a href="embee-builders/amenities"> <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Amenities </button> </a>
                </div>

                <div class="col-md-4 routing-padding">
                    <a href="embee-builders/facilities"><button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Facilities</button></a>
                </div>

                <div class="col-md-4 routing-padding">
                    <a href="embee-builders/isometric-view">  <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Isometric View</button> </a>
                </div>



                <div class="col-md-4 routing-padding">
                    <a href="embee-builders/specification"> <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Specification</button> </a>
                </div>

                <div class="col-md-4 routing-padding">
                    <a href="embee-builders/floor-plan">   <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Project Plan</button> </a>
                </div>

                <div class="col-md-4 routing-padding">
                    <a href="embee-builders">
                        <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Overview</button> </a>
                </div>
                <h1 class="section-title"> Download Brochure</h1>
                <div class="col-md-12 routing-padding">
                    <a href="/doccuments/fortune/brochure.pdf">
                        <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Embee Fortune</button> </a>
                </div>

                <h1 class="section-title">Project Video</h1>
                <!-- PROPERTY MAP HOLDER -->
                <iframe src="https://www.youtube.com/embed/vTzPWUrFDi0" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe>


                <h1 class="section-title">Project Updates</h1>
                <!-- PROPERTY MAP HOLDER -->
                <iframe src="https://www.youtube.com/embed/IGFDFxlf8Yg" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe>






                <h1 class="section-title">Property Location</h1>
                <!-- PROPERTY MAP HOLDER -->
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3564.445205653868!2d88.36736814990066!3d26.69822058313805!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39e446839aeeda77%3A0xcad5d018342adfdc!2sEmbee+Fortune!5e0!3m2!1sen!2sin!4v1510573446734" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

                <div class="share-wraper col-sm-12">
                    <h5>Share this Property:</h5>
                    <ul class="social-networks">
                        <li><a target="_blank"
                               href="http://www.facebook.com/sharer.php?s=100&amp;p%5Burl%5D=http://embeebuilders.com/"><i class="fa fa-facebook"></i></a></li>
                        <li><a
                                    target="_blank"
                                    href="https://twitter.com/intent/tweet?url=http://embeebuilders.com/"><i class="fa fa-twitter"></i></a></li>
                        <li><a
                                    target="_blank"
                                    href="https://plus.google.com/share?url=http://embeebuilders.com/"><i class="fa fa-google"></i></a></li>
                        <li><a
                                    target="_blank"
                                    href="http://pinterest.com/pin/create/button/?url=http://embeebuilders.com/">
                                <i class="fa fa-pinterest"></i></a></li>
                        <li><a
                                    href="mailto:?subject=Check%20out%20this%20property%20I%20found!&amp;body=http://embeebuilders.com/"><i class="fa fa-envelope"></i></a></li>
                    </ul>

                    <a class="print-button" href="javascript:window.print();">
                        <i class="fa fa-print"></i>
                    </a>
                </div>


                @include('Parts.similar.fortune')



            </div>
            <!-- END MAIN CONTENT -->


            <!-- BEGIN SIDEBAR -->
    @include('Parts.rightside')
            <!-- END SIDEBAR -->

        </div>
    </div>
</div>
<!-- END CONTENT WRAPPER -->

    @endsection

@section('page_js')
    <script src="js/freewall.js"></script>
@stop