@extends('structure')
@section('content')
    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Embee Fortune</h1>

                    <ul class="breadcrumb">
                        <li><a href="index.html">Home </a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="properties-detail.html">Embee Fortune</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->

    <style>
        .hover15 figure {
            position: relative;
        }

        .hover15 figure::before {
            position: absolute;
            top: 50%;
            left: 50%;
            z-index: 2;
            display: block;
            content: '';
            width: 0;
            height: 0;
            background: rgba(255, 77, 100, 0.47);
            border-radius: 100%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            opacity: 0;
        }

        .hover15 figure:hover::before {
            -webkit-animation: circle .75s;
            animation: circle .75s;
        }

        @-webkit-keyframes circle {
            0% {
                opacity: 1;
            }
            40% {
                opacity: 1;
            }
            100% {
                width: 200%;
                height: 200%;
                opacity: 0;
            }
        }

        @keyframes circle {
            0% {
                opacity: 1;
            }
            40% {
                opacity: 1;
            }
            100% {
                width: 200%;
                height: 200%;
                opacity: 0;
            }
        }

    </style>

    <div class="content">
        <div class="container">
            <div class="row">

                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-8">

                    <div id="property-listing" class="grid-style1 clearfix">
                        <div class="row">


                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/embee/facilities/pool.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>SWIMMING POOL</p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/embee/facilities/gym.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>AC GYMNASIUM</p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/embee/facilities/badminton.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>BADMINTON COURT</p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/embee/facilities/aerial.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>AERIAL VIEW</p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/embee/facilities/indoor.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>INDOOR SPORT ROOM</p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/embee/facilities/hall.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>AC COMMUNITY HALL</p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/embee/facilities/yoga.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>YOGA ROOM</p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/embee/facilities/kids.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>KIDS ACTIVITY CENTER
                                    </p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/embee/facilities/steam.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>STEAM AND SAUNA ROOM</p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/embee/facilities/cafe.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>CAFE WITH WIFI ZONE</p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/embee/facilities/open-space.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>55% OPEN SPACE
                                    </p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/embee/facilities/kids-area.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>OUTDOOR KID'S AREA</p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img

                                                        src="{{asset('images/projects/embee/facilities/jogger.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>JOGGER'S TRACK</p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/embee/facilities/garden.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>LANDSCAPED GARDEN</p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/embee/facilities/mountain.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>CLEAR MOUNTAIN VIEW</p>
                                </div>

                            </div>



                        </div>
                    </div>

                @include('Projects.Embeebuilders.button')
                @include('Parts.similar.fortune')



                <!-- END PROPERTIES ASSIGNED -->

                </div>
                <!-- END MAIN CONTENT -->


                @include('Parts.rightside')

            </div>
        </div>
    </div>


@endsection