@extends('structure')
@section('content')
    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Embee Builders</h1>

                    <ul class="breadcrumb">
                        <li><a href="/">Home </a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="/">Embee Builders</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->

    <style>
        .hover15 figure {
            position: relative;
        }

        .hover15 figure::before {
            position: absolute;
            top: 50%;
            left: 50%;
            z-index: 2;
            display: block;
            content: '';
            width: 0;
            height: 0;
            background: rgba(255, 77, 100, 0.47);
            border-radius: 100%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            opacity: 0;
        }

        .hover15 figure:hover::before {
            -webkit-animation: circle .75s;
            animation: circle .75s;
        }

        @-webkit-keyframes circle {
            0% {
                opacity: 1;
            }
            40% {
                opacity: 1;
            }
            100% {
                width: 200%;
                height: 200%;
                opacity: 0;
            }
        }

        @keyframes circle {
            0% {
                opacity: 1;
            }
            40% {
                opacity: 1;
            }
            100% {
                width: 200%;
                height: 200%;
                opacity: 0;
            }
        }

    </style>

    <div class="content">
        <div class="container">
            <div class="row">

                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-8">

                    <div id="property-listing" class="grid-style1 clearfix">
                        <div class="row">



                            <div id="agents" class="col-sm-12">
                                <h1 class="section-title"
                                    data-animation-direction="from-left" data-animation-delay="50">
                                    TESTIMONIALS</h1>

                                <ul class="agency-detail-agents clearfix">

                                    <li class="col-lg-6" data-animation-direction="from-left" data-animation-delay="250">
                                        <a href="#"><img src="../../images/avtar/avtar.png" alt="" /></a>
                                        <div class="info">
                                            <a href="#"><h3>Tanmoy Das</h3></a>
                                            <span class="location">Ramendrapally, Raiganj</span>
                                            <p>

                                                The bonding with Embee Fortune to me is for last
                                                5-6 Months since I visited the office of Embee
                                                Fortune to know about the project, even before
                                                the booking of flat. The bonding is good and all the members
                                                of this group are co-operative & sincere

                                            </p>

                                        </div>
                                    </li>



                                    <li class="col-lg-6" data-animation-direction="from-left" data-animation-delay="250">
                                        <a href="#"><img src="../../images/avtar/avtar.png" alt="" /></a>
                                        <div class="info">
                                            <a href="#"><h3>Rajeev Parik</h3></a>
                                            <span class="location">M.G Road, Jalpaiguri</span>
                                            <p>
                                                I felt Embee Fortune Project quite trustworthy and I assure to have a good bonding with this project in the upcoming future time.
                                            </p>

                                        </div>
                                    </li>









                                </ul>



                                <ul class="agency-detail-agents clearfix">

                                    <li class="col-lg-6" data-animation-direction="from-left" data-animation-delay="250">
                                        <a href="#"><img src="../../images/avtar/avtar.png" alt="" /></a>
                                        <div class="info">
                                            <a href="#"><h3>Satish Sebastian</h3></a>
                                            <span class="location">Durga Mandir, Kadamtala</span>
                                            <p>

                                                I am relatively new regarding the relationship with Embee Fortune as i have booked a flat only two months back. But I found the place really appropriate for me as it is very near to my hotel and I was looking for a place where I could stay with my family somewhere not far away. The place looks very good and I hope everything goes according to the plan. Not to forget the office staff of Embee Fortune who are friendly and well-behaved.


                                            </p>

                                        </div>
                                    </li>

                                    <li class="col-lg-6" data-animation-direction="from-left" data-animation-delay="250">
                                        <a href="#"><img src="../../images/avtar/avtar.png" alt="" /></a>
                                        <div class="info">
                                            <a href="#"><h3>Susmita & Sishu Sankar</h3></a>
                                            <span class="location">Shushrutanagar, Siliguri</span>
                                            <p>
                                                It is a very nice project in whole Siliguri, and we will love to stay and enjoy the place. It will be thankful for us to be a part of the Embee Fortune group. Staff are very nice & friendly.

                                            </p>

                                        </div>
                                    </li>











                                </ul>

                                <div class="col-lg-12 center">

                                </div>
                                <br>
                            </div>


                        </div>
                    </div>





                <!-- END PROPERTIES ASSIGNED -->

                </div>
                <!-- END MAIN CONTENT -->


                @include('Parts.rightside')

            </div>
        </div>
    </div>


@endsection