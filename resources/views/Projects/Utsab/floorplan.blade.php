@extends('structure')
@section('page_css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />

@stop
@section('content')
    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Squarewood Utsab</h1>
                    <p style="text-align: left">[ A Joint Initiative Of Embee Builders & Squarewood Builders ]</p>

                    <ul class="breadcrumb">
                        <li><a href="/">Home </a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="/">Squarewood Utsab</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->
    <div class="content">
        <div class="container">
            <div class="row">
                <style>
                    .paddings {
                        padding-top: 10px;
                        padding-bottom: 10px;
                        border: 1px;
                        border-color: rgba(231, 69, 88, 0.89);

                    }
                </style>
                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-12">

                    <div id="property-listing" class="grid-style1 clearfix">
                        <div class="row">

                            <div class="col-md-8 col-xs-12">
                                <h1 class="section-title">AS PER ACTUAL PLAN & MEASUREMENT</h1>
                                <style>
                                    .imgdown {
                                        background-color: #ff4961;
                                        font-size: 22px;
                                        color: #ffffff;
                                        text-align: center;
                                    }

                                    .mainimg {
                                        align-items: center;
                                        align-content: center;
                                    }
                                </style>
                                <div class="col-md-6 paddings"><a href="{{asset('/images/projects/utsab/floorplan/1.JPG')}}"
                                                                  data-fancybox
                                                                  data-caption="Roof Plan Block 1, 2, 3, 4">

                                        <img class="img-responsive center-block"
                                             src="{{asset('/images/projects/utsab/floorplan/1.JPG')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Roof Plan Block 1, 2, 3, 4
                                    </div>
                                </div>

                                <div class="col-md-6 paddings"><a href="{{asset('/images/projects/utsab/floorplan/2.JPG')}}"
                                                                  data-fancybox data-caption="Roof Plan Block 5, 6, 7">

                                        <img class="img-responsive center-block"
                                             src="{{asset('/images/projects/utsab/floorplan/2.JPG')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Roof Plan Block 5, 6, 7
                                    </div>
                                </div>

                                <div class="col-md-6 paddings"><a href="{{asset('/images/projects/utsab/floorplan/3.JPG')}}"
                                                                  data-fancybox data-caption="Block 1, 2">

                                        <img class="img-responsive center-block"
                                             src="{{asset('/images/projects/utsab/floorplan/3.JPG')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Block 1, 2

                                    </div>
                                </div>

                                <div class="col-md-6 paddings"><a href="{{asset('/images/projects/utsab/floorplan/4.JPG')}}"
                                                                  data-fancybox data-caption="Block 3, 4">

                                        <img class="img-responsive center-block"
                                             src="{{asset('/images/projects/utsab/floorplan/4.JPG')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Block 3, 4
                                    </div>
                                </div>


                                <div class="col-md-6 paddings"><a href="{{asset('/images/projects/utsab/floorplan/5.JPG')}}"
                                                                  data-fancybox
                                                                  data-caption="Block 5, 6, 7">

                                        <img class="img-responsive center-block"
                                             src="{{asset('/images/projects/utsab/floorplan/5.JPG')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Block 5, 6, 7
                                    </div>
                                </div>

                                @include('Projects.Utsab.button')
                                @include('Parts.similar.utsab')
                            </div>
                            <div class="col-md-4 col-xs-12">
                                @include('Parts.right')
                            </div>


                        </div>
                    </div>


                    <!-- END PROPERTIES ASSIGNED -->

                </div>
                <!-- END MAIN CONTENT -->


            </div>
        </div>
    </div>





@endsection




@section('page_js')
    <script src="js/freewall.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>

@stop