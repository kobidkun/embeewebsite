<style>
    .routing-padding {
        padding-top: 10px;
    }

</style>
<h1 class="section-title">Quick Navigation</h1>

<div class="col-md-4 routing-padding">
    <a href="/embee-utsab/amenities"> <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Amenities </button> </a>
</div>

<div class="col-md-4 routing-padding">
    <a href="/embee-utsab/facilities"><button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Facilities</button></a>
</div>

<div class="col-md-4 routing-padding">
    <a href="/embee-utsab/isometric-view">  <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Isometric View</button> </a>
</div>



<div class="col-md-4 routing-padding">
    <a href="/embee-utsab/specification"> <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Specification</button> </a>
</div>

<div class="col-md-4 routing-padding">
    <a href="/embee-utsab/floor-plan">   <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Project Plan</button> </a>
</div>

<div class="col-md-4 routing-padding">
    <a href="/embee-utsab">   <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Overview</button> </a>
</div>

<h1 class="section-title"> Download Brochure</h1>
<div class="col-md-12 routing-padding">
    <a href="/doccuments/utsab/brochure.pdf">
        <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Squarewood Utsab</button> </a>
</div>

<h1 class="section-title">Project Video</h1>
<!-- PROPERTY MAP HOLDER -->
<iframe src="https://www.youtube.com/embed/8GRny5tKzfE" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe>


<h1 class="section-title">Construction Updates</h1>
<!-- PROPERTY MAP HOLDER -->
<iframe src="https://www.youtube.com/embed/P4hr0TPBmjY" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe>
