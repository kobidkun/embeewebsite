@extends('structure')
@section('page_css')


    @stop
@section('content')

    <style>
        .backutsab{
            background-image: url("images/project/utsabheader.jpg");
            height: 450px;
        }
    </style>
<!-- BEGIN HOME GRID -->
<div>
    <div class="backutsab">


<h1 class="section-title" style=" font-weight:500;
color: #ffffff;
font-size: 35px;
 background-color: #ff4961;
  width: 450px; text-align: center; padding-top: 19px">Squarewood Utsab</h1>


    </div>
</div>
<!-- END HOME GRID -->

<!-- BEGIN PAGE TITLE/BREADCRUMB -->
<div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title">Squarewood Utsab</h1>
                <p style="text-align: left">[ A Joint Initiative Of Embee Builders & Squarewood Builders ]</p>

                <ul class="breadcrumb">
                    <li><a href="/">Home </a></li>
                    <li><a href="#">Projects</a></li>
                    <li><a href="/embee-utsab">Squarewood Utsab</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE TITLE/BREADCRUMB -->


<!-- BEGIN CONTENT WRAPPER -->
<div class="content">
    <div class="container">
        <div class="row">

            <!-- BEGIN MAIN CONTENT -->
            <div class="main col-sm-8">

                <h1 class="property-title">Squarewood Utsab</h1>


                <!-- BEGIN PROPERTY DETAIL SLIDERS WRAPPER -->
                <div id="property-detail-wrapper" class="style1">

                    <div class="price">

                        <span>Booking Started</span>
                    </div>

                    <!-- BEGIN PROPERTY DETAIL LARGE IMAGE SLIDER -->
                    <div id="property-detail-large" class="owl-carousel">
                        <div class="item">
                            <img src="images/projects/utsab/gallery/3.jpg" alt="" />
                        </div>
                        <div class="item">
                            <img src="images/projects/utsab/gallery/4.jpg" alt="" />
                        </div>
                        <div class="item">
                            <img src="images/projects/utsab/gallery/5.jpg" alt="" />
                        </div>
                        <div class="item">
                            <img src="images/projects/utsab/gallery/1.jpg" alt="" />
                        </div>
                        <div class="item">
                            <img src="images/projects/utsab/gallery/2.jpg" alt="" />
                        </div>
                        <div class="item">
                            <img src="images/projects/utsab/gallery/6.jpg" alt="" />
                        </div>
                        <div class="item">
                            <img src="images/projects/utsab/gallery/7.jpg" alt="" />
                        </div>
                        <div class="item">
                            <img src="images/projects/utsab/gallery/8.jpg" alt="" />
                        </div>



                    </div>
                    <!-- END PROPERTY DETAIL LARGE IMAGE SLIDER -->
ph
                    <!-- BEGIN PROPERTY DETAIL THUMBNAILS SLIDER -->
                    <div id="property-detail-thumbs" class="owl-carousel">
                        <div class="item"><img src="images/projects/utsab/gallery/3.jpg" alt="" /></div>
                        <div class="item"><img src="images/projects/utsab/gallery/4.jpg" alt="" /></div>
                        <div class="item"><img src="images/projects/utsab/gallery/5.jpg" alt="" /></div>
                        <div class="item"><img src="images/projects/utsab/gallery/1.jpg" alt="" /></div>
                        <div class="item"><img src="images/projects/utsab/gallery/2.jpg" alt="" /></div>
                        <div class="item"><img src="images/projects/utsab/gallery/6.jpg" alt="" /></div>
                        <div class="item"><img src="images/projects/utsab/gallery/7.jpg" alt="" /></div>
                        <div class="item"><img src="images/projects/utsab/gallery/8.jpg" alt="" /></div>
                    </div>
                    <!-- END PROPERTY DETAIL THUMBNAILS SLIDER -->

                </div>

                <p>
                    Under the company Squarewood Projects Pvt. Ltd. : Squarewood is a joint initiative of highly talented individuals possessing extensive entrepreneurship spirit, knowledge, professional expertise and leadership qualities of running already established Business Houses, along with hands on experience in the Real Estate Industry .

                    The company is focused in the business model of social housing as because the same would account to almost 60% of the Real Estate sector itself.

                    TO VIEW LATEST portfolio’s, click www.squarewood.in
                    <br>
                    <br>

                    Quick Facts
<ui>
    <li>Location: NBMC, Kalamjote, kadamtala, Shiv mandir, SIliguri</li>
    <li>Land Mark: Near North Bengal Medical College (NBMC)
        Land</li>
    <li>Area: 2 Acres</li>
    <li>Total Area: 89,500 sq ft (Approx)
        Total</li>
    <li>1BHK Flat | 2 BHK Flat</li>
</ui>
                    Location: NBMC, Kalamjote, kadamtala, Shiv mandir, SIliguri

                    Total Area: 89,500 sq ft (Approx)
                    Total No. of Blocks: 7 Blocks (G+3)
                    Total No. of Flats: 150 Nos
                    1BHK Flat | 2 BHK Flat
                    Completion: September 2019 onwards

                </p>






                <h1 class="section-title">Property Features</h1>
                <!-- BEGIN PROPERTY FEATURES LIST -->
                <ul class="property-features col-sm-6">
                    <li><i class="icon-garage"></i> FREE PARKING AREA</li>
                    <li><i class="icon-key"></i> UNDER PMAY SCHEME</li>
                    <li><i class="icon-area"></i> LIFT FACILITY</li>
                    <li><i class="icon-security"></i> EARTHQUAKE RESISTANT STRUCTURE </li>
                </ul>

                <ul class="property-features col-sm-6">
                    <li><i class="icon-pets"></i> CHILDREN PLAY ZONE</li>
                    <li><i class="icon-house2"></i> AC COMMUNITY HALL</li>
                    <li><i class="icon-rooms"></i> BADMINTON COURT</li>
                    <li><i class="icon-factory"></i> POWER BACKUP FOR COMMON AREAS

                    </li>
                </ul>
                <!-- END PROPERTY FEATURES LIST -->


                <div class="share-wraper col-sm-12">
                    <h5>Share this Property:</h5>
                    <ul class="social-networks">
                        <li><a target="_blank"
                               href="http://www.facebook.com/sharer.php?s=100&amp;p%5Burl%5D=http://embeebuilders.com/"><i class="fa fa-facebook"></i></a></li>
                        <li><a
                                    target="_blank"
                                    href="https://twitter.com/intent/tweet?url=http://embeebuilders.com/"><i class="fa fa-twitter"></i></a></li>
                        <li><a
                                    target="_blank"
                                    href="https://plus.google.com/share?url=http://embeebuilders.com/"><i class="fa fa-google"></i></a></li>
                        <li><a
                                    target="_blank"
                                    href="http://pinterest.com/pin/create/button/?url=http://embeebuilders.com/">
                                <i class="fa fa-pinterest"></i></a></li>
                        <li><a
                                    href="mailto:?subject=Check%20out%20this%20property%20I%20found!&amp;body=http://embeebuilders.com/"><i class="fa fa-envelope"></i></a></li>
                    </ul>

                    <a class="print-button" href="javascript:window.print();">
                        <i class="fa fa-print"></i>
                    </a>
                </div>
                <style>
                    .routing-padding {
                        padding-top: 10px;
                    }

                </style>


                <div class="col-md-4 routing-padding">
                    <a href="embee-utsab/amenities"> <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Amenities </button> </a>
                </div>

                <div class="col-md-4 routing-padding">
                    <a href="embee-utsab/facilities"><button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Facilities</button></a>
                </div>

                <div class="col-md-4 routing-padding">
                    <a href="embee-utsab/isometric-view">  <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Isometric View</button> </a>
                </div>



                <div class="col-md-4 routing-padding">
                    <a href="embee-utsab/specification"> <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Specification</button> </a>
                </div>

                <div class="col-md-4 routing-padding">
                    <a href="embee-utsab/floor-plan">   <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Project Plan</button> </a>
                </div>

                <div class="col-md-4 routing-padding">
                    <a href="embee-utsab">   <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">Overview</button> </a>
                </div>
                <h1 class="section-title"> Download Brochure</h1>
                <div class="col-md-12 routing-padding">
                    <a href="/doccuments/utsab/brochure.pdf">
                        <button style="background-color: #e74558;"  type="button" class="btn btn-danger btn-lg btn-block">
                            Squarewood Utsab</button> </a>
                </div>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <iframe src="https://www.youtube.com/embed/8GRny5tKzfE" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe>


                <h1 class="section-title">Construction Updates</h1>
                <!-- PROPERTY MAP HOLDER -->
                <iframe src="https://www.youtube.com/embed/P4hr0TPBmjY" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe>

                <h1 class="section-title">Property Location</h1>
                <!-- PROPERTY MAP HOLDER -->
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3564.4113310092703!2d88.37717205012687!3d26.699303283137642!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39e446992ceed41b%3A0x6ea4821f201ed9a2!2sSquarewood+Utsav!5e0!3m2!1sen!2sin!4v1510314527133" width="100%" height="250px" frameborder="0" style="border:0" allowfullscreen></iframe>




                @include('Parts.similar.utsab')









            </div>
            <!-- END MAIN CONTENT -->


            <!-- BEGIN SIDEBAR -->
        @include('Parts.rightside')
            <!-- END SIDEBAR -->


        </div>


    </div>
</div>
<!-- END CONTENT WRAPPER -->

    @endsection

@section('page_js')
   <script src="js/freewall.js"></script>


@stop