@extends('structure')
@section('content')
    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Squarewood Utsab</h1>
                    <p style="text-align: left">[ A Joint Initiative Of Embee Builders & Squarewood Builders ]</p>

                    <ul class="breadcrumb">
                        <li><a href="/">Home </a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="/">Squarewood Utsab</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->

    <style>
        .hover15 figure {
            position: relative;
        }

        .hover15 figure::before {
            position: absolute;
            top: 50%;
            left: 50%;
            z-index: 2;
            display: block;
            content: '';
            width: 0;
            height: 0;
            background: rgba(255, 77, 100, 0.47);
            border-radius: 100%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            opacity: 0;
        }

        .hover15 figure:hover::before {
            -webkit-animation: circle .75s;
            animation: circle .75s;
        }

        @-webkit-keyframes circle {
            0% {
                opacity: 1;
            }
            40% {
                opacity: 1;
            }
            100% {
                width: 200%;
                height: 200%;
                opacity: 0;
            }
        }

        @keyframes circle {
            0% {
                opacity: 1;
            }
            40% {
                opacity: 1;
            }
            100% {
                width: 200%;
                height: 200%;
                opacity: 0;
            }
        }

    </style>

    <div class="content">
        <div class="container">
            <div class="row">

                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-8">

                    <div id="property-listing" class="grid-style1 clearfix">
                        <div class="row">


                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/utsab/facilities/new/aerial.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Areal View </p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/utsab/facilities/new/badminton.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>SIDE VIEW </p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/utsab/facilities/new/ft.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Rooftop Gym (Provisions) </p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/utsab/facilities/new/gym.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Adda Zone </p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/utsab/facilities/new/hall.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Community Hall </p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/utsab/facilities/new/jogger.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Joggers park </p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/utsab/facilities/new/open-space.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p> Badminton Court</p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/utsab/facilities/new/top.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p> Top View</p>
                                </div>

                            </div>
                            @include('Projects.Utsab.button')
                            @include('Parts.similar.utsab')




                        </div>
                    </div>





                <!-- END PROPERTIES ASSIGNED -->

                </div>
                <!-- END MAIN CONTENT -->


                @include('Parts.rightside')

            </div>
        </div>
    </div>


@endsection