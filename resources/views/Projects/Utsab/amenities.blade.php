@extends('structure')
@section('content')
    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Squarewood Utsab</h1>
                    <p style="text-align: left">[ A Joint Initiative Of Embee Builders & Squarewood Builders ]</p>

                    <ul class="breadcrumb">
                        <li><a href="/">Home </a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="/">Squarewood Utsab</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->

    <!-- BEGIN CONTENT WRAPPER -->
    <div class="content">
        <div class="container">
            <div class="row">

                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-8">

                    <div id="property-listing" class="grid-style1 clearfix">
                        <div class="row">
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">



                                    <img  src={{asset('images/projects/Delight/amenities/power.jpg')}} alt="" />
                                </div>
                                <div class="price">

                                    <p>24X7 POWER BACK UP</p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">



                                    <img  src={{asset('images/projects/Delight/amenities/parking.jpg')}} alt="" />
                                </div>
                                <div class="price">

                                    <p>FREE BIKE PARKING AREA</p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">



                                    <img  src={{asset('images/projects/Delight/amenities/lift.jpg')}} alt="" />
                                </div>
                                <div class="price">

                                    <p>1 LIFT PER BLOCK</p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">



                                    <img  src={{asset('images/projects/Delight/amenities/earthquake.jpg')}} alt="" />
                                </div>
                                <div class="price">

                                    <p>EARTHQUAKE PROOF</p>
                                </div>

                            </div>


                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">



                                    <img  src={{asset('images/projects/Delight/amenities/lightiting.jpg')}} alt="" />
                                </div>
                                <div class="price">

                                    <p>ADEQUATE & MODERN FIRE-FIGHTING STRUCTURE
                                    </p>
                                </div>

                            </div>




                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">



                                    <img  src="{{asset('images/projects/Delight/amenities/intercom.jpg')}}" alt="" />
                                </div>
                                <div class="price">

                                    <p>PROVISION FOR INVENTOR</p>
                                </div>

                            </div>

                        </div>
                    </div>

                @include('Projects.Utsab.button')
                @include('Parts.similar.utsab')








                <!-- END PROPERTIES ASSIGNED -->

                </div>
                <!-- END MAIN CONTENT -->


                @include('Parts.rightside')

            </div>
        </div>
    </div>
    <!-- END CONTENT WRAPPER -->
@endsection