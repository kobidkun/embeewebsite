@extends('structure')
@section('content')
    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Squarewood Utsab</h1>
                    <p style="text-align: left">[ A Joint Initiative Of Embee Builders & Squarewood Builders ]</p>

                    <ul class="breadcrumb">
                        <li><a href="/">Home </a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="/">Squarewood Utsab</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->
    <div class="content">
        <div class="container">
            <div class="row">

                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-8">
                    <h1 class="section-title">Specifications</h1>
                    <div id="blog-listing" class="list-style clearfix">
                        <div class="row">
                            <div class="item col-md-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">

                                    <img src="{{asset('/images/projects/embee/specifications/foundation.jpg')}}" alt="" />
                                </div>
                                <div class="tag"><i class="fa fa-building"></i></div>

                                <div class="info-blog">
                                    <h3 style="margin-top: -15px;">Structure</h3>

                                    <p>
                                    <ul>

                                        <li>
                                            Construction will be with Earthquake-resistant
                                            RCC framework with standard Brickwork
                                            designed by our Architect/Engineer

                                        </li>
                                    </ul>



                                    </p>
                                </div>
                            </div>

                            <div class="item col-md-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">

                                    <img src="{{asset('/images/projects/embee/specifications/room.jpg')}}" alt="" />
                                </div>
                                <div class="tag"><i class="fa fa-building"></i></div>
                                <div class="info-blog">

                                    <h3 >Rooms</h3>
                                    <p>
                                        <li>   Bedrooms & Living room: Vitrified tiles</li>
                                        <li>   Kitchen: Vitrified tiles</li>
                                        <li>   Bath & WC: Anti-skid tiles</li>
                                        <li>   Balcony: Mosaic or Cement tiles</li>
                                        <li>  Staircase & Terrace: Marble /
                                          Stone Tile / Mosaic</li>

                                    </p>
                                </div>
                            </div>

                            <div class="item col-md-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">

                                    <img src="{{asset('/images/projects/embee/specifications/toilet.jpg')}}" alt="" />
                                </div>
                                <div class="tag"><i class="fa fa-building"></i></div>
                                <div class="info-blog">

                                    <h3 >Toilets</h3>
                                    <p>
                                    <ul>
                                        <li>  Floor ceramic tiles.</li>
                                        <li>   Dado glazed tiles upto door height.</li>
                                        <li>   Sanitary ware European type commode of Jaquar/ Hindware or Equivalent.</li>
                                        <li>   CP Fittings Jaquar/ Hindware or Equivalent</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>

                            <div class="item col-md-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">

                                    <img src="{{asset('/images/projects/embee/specifications/kitchen.jpg')}}" alt="" />
                                </div>
                                <div class="tag"><i class="fa fa-building"></i></div>
                                <div class="info-blog">

                                    <h3 >Kitchen</h3>
                                    <p>
                                    <ul>
                                        <li>
                                            Floor Ceramic / Vitrified tiles.

                                        </li>
                                        <li>
                                            Counter Black counter top.
                                        </li>

                                        <li>
                                            Sink Stainless steel sink
                                        </li>

                                        <li>
                                            Dado Ceramic tiles dado upto 2 feet above the counter
                                        </li>
                                    </ul>

                                    </p>
                                </div>
                            </div>

                            <div class="item col-md-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">

                                    <img src="{{asset('/images/projects/embee/specifications/fitting.jpg')}}" alt="" />
                                </div>
                                <div class="tag"><i class="fa fa-building"></i></div>
                                <div class="info-blog">

                                    <h3 >Fittings</h3>
                                    <p>
                                    <ul>

                                        <li> CP fittings of standard brand</li>
                                        <li> Steel sink of standard brand</li>
                                        <li> European type commode & basin of standard
                                        brand</li>

                                    </ul>
                                    </p>
                                </div>
                            </div>

                            <div class="item col-md-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">

                                    <img src="{{asset('/images/projects/utsab/sprc/exterior.jpg')}}" alt="" />
                                </div>
                                <div class="tag"><i class="fa fa-building"></i></div>
                                <div class="info-blog">

                                    <h3 >Exterior</h3>
                                    <p>
                                    <ul>
                                        <li>Elegant looks with superior quality weather coat paint & water proofing compound</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>

                            <div class="item col-md-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">

                                    <img src="{{asset('/images/projects/embee/specifications/electrical.jpg')}}" alt="" />
                                </div>
                                <div class="tag"><i class="fa fa-building"></i></div>
                                <div class="info-blog">

                                    <h3 >Electrical</h3>

                                    <p>
                                    <ul>
                                        <li> All concealed wiring with ISI marked wire</li>
                                        <li>  All modular switches of standard brand</li>
                                        <li>   Provision for AC point in master bedroom</li>
                                        <li>   Provision for Geyser, Refrigerator, Water Purifier,
                                        <li>   Washing Machine & Telephone point, one in
                                           each flat</li>
                                        <li> Provision for TV plug point in living room
                                        & all bedrooms</li>

                                    </ul>
                                    </p>
                                </div>
                            </div>
                            <div class="item col-md-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">

                                    <img src="{{asset('images/projects/utsab/sprc/lighting.jpg')}}" alt="" />
                                </div>
                                <div class="tag"><i class="fa fa-building"></i></div>
                                <div class="info-blog">

                                    <h3 >Rooms</h3>
                                    <p>
                                    <ul>
                                        <li>Compound overhead illumination with street lighting.
                                        </li>
                                        <li>Lift lobbies and stair case lighting to match décor</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>


                        </div>
                        @include('Projects.Utsab.button')



                    </div>
                    @include('Parts.similar.utsab')
                </div>
                <div class="col-md-4 col-xs-12">
                    @include('Parts.right')
                </div>

            </div>
        </div>

        <!-- END PROPERTY LISTING -->
@endsection

