@extends('structure')
@section('content')
    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Embee Delight</h1>

                    <ul class="breadcrumb">
                        <li><a href="/">Home </a></li>
                        <li><a href="/">Projects</a></li>
                        <li><a href="/embee-builders">Embee Delight</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->

    <div class="content">
        <div class="container">
            <div class="row">

                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-8">
                    <h1 class="section-title">Project Updates</h1>
    <div id="blog-listing" class="list-style clearfix">







        @foreach ($projects as $project)



            <h1 class="blog-title">Project Update on {{ $project->date }}
            </h1>

            <div class="blog-main-image">


                <iframe width="100%" height="315" src="https://www.youtube.com/embed/{{ $project->url }}"
                        frameborder="0" allowfullscreen></iframe>

            </div>

            <div class="blog-bottom-info">
                <ul>
                    <li><i class="fa fa-calendar"></i> {{ $project->date }}</li>
                    <li><i class="fa fa-tags"></i> {{ $project->project }}</li>
                </ul>

                <div id="post-author"><i class="fa fa-pencil"></i> By Embee Builders</div>
            </div>
        @endforeach









    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    @include('Parts.right')
                </div>

            </div>
        </div>

    <!-- END PROPERTY LISTING -->
@endsection