@extends('structure')
@section('content')
    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Embee Delight </h1>

                    <p style="text-align: left">[ A Joint Initiative Of Embee Builders & Manakamna Builders ]</p>
                    <ul class="breadcrumb">
                        <li><a href="index.html">Home </a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="/">Embee Delight</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->

    <div class="content">
        <div class="container">
            <div class="row">

                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-8">
                    <h1 class="section-title">Specifications</h1>
                    <div id="blog-listing" class="list-style clearfix">
                        <div class="row">
                            <div class="item col-md-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">

                                    <img src="{{asset('/images/projects/embee/specifications/foundation.jpg')}}" alt="" />
                                </div>
                                <div class="tag"><i class="fa fa-building"></i></div>

                                <div class="info-blog">
                                    <h3 style="margin-top: -15px;">Structure</h3>

                                    <p>
                                    <ul>
                                        <li>
                                            Earthquake resistant RCC framed structure
                                        </li>
                                        <li>
                                            Our buildings are designed as per IS 1893 seismic code. Siliguri is located in Zone 4 of Indian Earthquake map and our building is designed as per Zone 4. All the requirement of IS 13920 for ductility detailing to be followed in design and construction of structure.


                                        </li>
                                    </ul>



                                    </p>
                                </div>
                            </div>

                            <div class="item col-md-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">

                                    <img src="{{asset('/images/projects/embee/specifications/room.jpg')}}" alt="" />
                                </div>
                                <div class="tag"><i class="fa fa-building"></i></div>
                                <div class="info-blog">

                                    <h3 >Rooms</h3>
                                    <p>
                                    <li>  Floors Vitrified tiles in living/dining area and bedrooms</li>

                                    <li>   Walls Wall putty including one coat primer.</li>
                                    </p>
                                </div>
                            </div>

                            <div class="item col-md-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">

                                    <img src="{{asset('/images/projects/embee/specifications/toilet.jpg')}}" alt="" />
                                </div>
                                <div class="tag"><i class="fa fa-building"></i></div>
                                <div class="info-blog">

                                    <h3 >Toilets</h3>
                                    <p>
                                    <ul>
                                        <li>  Floor ceramic tiles.</li>
                                        <li>   Dado glazed tiles upto door height.</li>
                                        <li>   Sanitary ware European type commode of Jaquar/ Hindware or Equivalent.</li>
                                        <li>   CP Fittings Jaquar/ Hindware or Equivalent</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>

                            <div class="item col-md-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">

                                    <img src="{{asset('/images/projects/embee/specifications/kitchen.jpg')}}" alt="" />
                                </div>
                                <div class="tag"><i class="fa fa-building"></i></div>
                                <div class="info-blog">

                                    <h3 >Kitchen</h3>
                                    <p>
                                    <ul>
                                        <li>
                                            Floor Ceramic / Vitrified tiles.

                                        </li>
                                        <li>
                                            Counter Black counter top.
                                        </li>

                                        <li>
                                            Sink Stainless steel sink
                                        </li>

                                        <li>
                                            Dado Ceramic tiles dado upto 2 feet above the counter
                                        </li>
                                    </ul>

                                    </p>
                                </div>
                            </div>

                            <div class="item col-md-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">

                                    <img src="{{asset('/images/projects/embee/specifications/fitting.jpg')}}" alt="" />
                                </div>
                                <div class="tag"><i class="fa fa-building"></i></div>
                                <div class="info-blog">

                                    <h3 >Fittings</h3>
                                    <p>
                                    <ul>
                                        <li>Doors Flush doors with superior quality wooden frames</li>
                                        <li>Windows fully glazed aluminium sliding window with clear glass</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>

                            <div class="item col-md-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">

                                    <img src="{{asset('images/projects/Delight/spec/exterior.jpg')}}" alt="" />
                                </div>
                                <div class="tag"><i class="fa fa-building"></i></div>
                                <div class="info-blog">

                                    <h3 >Exterior</h3>
                                    <p>
                                    <ul>
                                        <li>Elegant looks with superior quality weather coat paint & water proofing compound</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>

                            <div class="item col-md-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">

                                    <img src="{{asset('/images/projects/embee/specifications/electrical.jpg')}}" alt="" />
                                </div>
                                <div class="tag"><i class="fa fa-building"></i></div>
                                <div class="info-blog">

                                    <h3 >Electrical</h3>

                                    <p>
                                    <ul>
                                        <li>Concealed wiring with ISI marked copper wire, modular switches of reputed brand</li>
                                        <li>Provision for geyser, water purifier and inverter line.
                                        </li>
                                        <li>AC point in 1 bedroom for 2 BHK & 2 bedrooms for 3 BHK.
                                        </li>
                                        <li>TV and Telephone points in living and master bedrooms</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>
                            <div class="item col-md-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">

                                    <img src="{{asset('images/projects/Delight/spec/lighting.jpg')}}" alt="" />
                                </div>
                                <div class="tag"><i class="fa fa-building"></i></div>
                                <div class="info-blog">

                                    <h3 >Rooms</h3>
                                    <p>
                                    <ul>
                                        <li>Compound overhead illumination with street lighting.
                                        </li>
                                        <li>Lift lobbies and stair case lighting to match décor</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>


                        </div>


                        @include('Projects.Delight.button')




                    </div>
                    @include('Parts.similar.delight')
                </div>
                <div class="col-md-4 col-xs-12">
                    @include('Parts.right')
                </div>

            </div>
        </div>

        <!-- END PROPERTY LISTING -->
@endsection
