@extends('structure')
@section('content')
    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Embee Delight </h1>
                    <p style="text-align: left">[ A Joint Initiative Of Embee Builders & Manakamna Builders ]</p>

                    <ul class="breadcrumb">
                        <li><a href="index.html">Home </a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="properties-detail.html">Embee Delight</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->

    <style>
        .hover15 figure {
            position: relative;
        }

        .hover15 figure::before {
            position: absolute;
            top: 50%;
            left: 50%;
            z-index: 2;
            display: block;
            content: '';
            width: 0;
            height: 0;
            background: rgba(255, 77, 100, 0.47);
            border-radius: 100%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            opacity: 0;
        }

        .hover15 figure:hover::before {
            -webkit-animation: circle .75s;
            animation: circle .75s;
        }

        @-webkit-keyframes circle {
            0% {
                opacity: 1;
            }
            40% {
                opacity: 1;
            }
            100% {
                width: 200%;
                height: 200%;
                opacity: 0;
            }
        }

        @keyframes circle {
            0% {
                opacity: 1;
            }
            40% {
                opacity: 1;
            }
            100% {
                width: 200%;
                height: 200%;
                opacity: 0;
            }
        }

    </style>

    <div class="content">
        <div class="container">
            <div class="row">

                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-8">

                    <div id="property-listing" class="grid-style1 clearfix">
                        <div class="row">


                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/Delight/Facilities/13.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Yoga Room</p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/Delight/Facilities/14.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Elderly Sitting Area</p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/Delight/Facilities/15.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Cricket Nets</p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/Delight/Facilities/16.jpeg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Basketball Court</p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/Delight/Facilities/17.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>2 Community Halls</p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/Delight/Facilities/18.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Badminton Court</p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/Delight/Facilities/19.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Outdoor Kids Area</p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/Delight/Facilities/20.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Jogging Area</p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/Delight/Facilities/21.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Landscaped Garden</p>
                                </div>

                            </div>

                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/Delight/Facilities/22.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Fitness Center</p>
                                </div>

                            </div>




                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/Delight/Facilities/24.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Squash Court</p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/Delight/Facilities/25.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Swimming Pool</p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/Delight/Facilities/26.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Student Library</p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/Delight/Facilities/27.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>Kids outdoor playing Area</p>
                                </div>

                            </div>
                            <div class="item col-sm-6"><!-- Set width to 4 columns for grid view mode only -->
                                <div class="image">
                                    <div class="hover15 column">
                                        <div>
                                            <figure><img
                                                        src="{{asset('images/projects/Delight/Facilities/28.jpg')}}"/>
                                            </figure>

                                        </div>


                                    </div>
                                </div>
                                <div class="price">

                                    <p>LOUNGE ROOM </p>
                                </div>

                            </div>

                        </div>
                    </div>



                @include('Projects.Delight.button')
                @include('Parts.similar.delight')



            <!-- END PROPERTIES ASSIGNED -->

            </div>
            <!-- END MAIN CONTENT -->


            @include('Parts.rightside')

        </div>
    </div>
    </div>


@endsection