@extends('structure')
@section('page_css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />

@stop
@section('content')
    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Embee Delight </h1>
                    <p style="text-align: left">[ A Joint Initiative Of Embee Builders & Manakamna Builders ]</p>

                    <ul class="breadcrumb">
                        <li><a href="/">Home </a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="/">Embee Delight</a></li>
                        <li><a href="#">Isometric View</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->
    <div class="content">
        <div class="container">
            <div class="row">
                <style>
                    .paddings {
                        padding-top: 10px;
                        padding-bottom: 10px;
                        border: 1px;
                        border-color: rgba(231,69,88,0.89);

                    }
                </style>
                <!-- BEGIN MAIN CONTENT -->
                <div class="col-md-8 col-xs-12">
                    <h1 class="section-title">Coming Soon</h1>
                    <style>
                        .imgdown {
                            background-color: #ff4961;
                            font-size: 22px;
                            color: #ffffff;
                            text-align: center;
                        }

                        .mainimg {
                            align-items: center;
                            align-content: center;
                        }
                    </style>
                    {{--<div class="col-md-6 paddings"><a href="{{asset('images/projects/embee/iso/iso1.jpg')}}"
                                                      data-fancybox
                                                      data-caption="3-BHK FLAT | FLAT A">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/projects/embee/iso/iso1s.jpg')}}"
                                 width="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            3-BHK FLAT | FLAT A
                        </div>
                    </div>

                    <div class="col-md-6 paddings"><a href="{{asset('images/projects/embee/iso/iso2.jpg')}}"
                                                      data-fancybox
                                                      data-caption="2-BHK FLAT | FLAT D">

                            <img class="img-responsive center-block"
                                 src="{{asset('images/projects/embee/iso/iso2s.jpg')}}"
                                 width="250px" alt=""/>

                        </a>
                        <div class="imgdown">
                            2-BHK FLAT | FLAT D
                        </div>
                    </div>--}}



                    @include('Projects.Delight.button')
                    @include('Parts.similar.delight')
                </div>
                <div class="col-md-4 col-xs-12">
                    @include('Parts.right')
                </div>







                <!-- END PROPERTIES ASSIGNED -->

            </div>
            <!-- END MAIN CONTENT -->




        </div>
    </div>
    </div>
@endsection




@section('page_js')
    <script src="js/freewall.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>

@stop