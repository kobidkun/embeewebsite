@extends('structure')
@section('page_css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />

@stop
@section('content')
    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Embee Delight </h1>
                    <p style="text-align: left">[ A Joint Initiative Of Embee Builders & Manakamna Builders ]</p>

                    <ul class="breadcrumb">
                        <li><a href="">Home </a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="/">Embee Delight</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->
    <div class="content">
        <div class="container">
            <div class="row">
                <style>
                    .paddings {
                        padding-top: 10px;
                        padding-bottom: 10px;
                        border: 1px;
                        border-color: rgba(231, 69, 88, 0.89);

                    }
                </style>
                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-12">

                    <div id="property-listing" class="grid-style1 clearfix">
                        <div class="row">

                            <div class="col-md-8 col-xs-12">
                                <h1 class="section-title">AS PER ACTUAL PLAN & MEASUREMENT</h1>
                                <style>
                                    .imgdown {
                                        background-color: #ff4961;
                                        font-size: 22px;
                                        color: #ffffff;
                                        text-align: center;
                                    }

                                    .mainimg {
                                        align-items: center;
                                        align-content: center;
                                    }
                                </style>

                            <div class="col-md-12">
                                <div class="col-md-6 paddings"><a href="{{asset('images/projects/Delight/floorplan/7.png')}}"
                                                                        data-fancybox
                                                                        data-caption="Master Plan">

                                        <img class="img-responsive center-block"
                                             src="{{asset('images/projects/Delight/floorplan/sml/7.png')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Master Plan
                                    </div>
                                </div>
                                <div class="col-md-6 paddings"><a href="{{asset('images/projects/Delight/floorplan/8.png')}}"
                                                                  data-fancybox
                                                                  data-caption="Typical Floor Plan (Aura, Bliss, Euphoria, Frolic)">

                                        <img class="img-responsive center-block"
                                             src="{{asset('images/projects/Delight/floorplan/sml/8.png')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Typical Floor Plan (Aura, Bliss, Euphoria, Frolic)
                                    </div>
                                </div>
                                <div class="col-md-6 paddings"><a href="{{asset('images/projects/Delight/floorplan/9.png')}}"
                                                                  data-fancybox
                                                                  data-caption="Typical Floor Plan (Carol)">

                                        <img class="img-responsive center-block"
                                             src="{{asset('images/projects/Delight/floorplan/sml/9.png')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Typical Floor Plan (Carol)
                                    </div>
                                </div>

                                </div>



                                <div class="col-md-12">




                                <br>
                                <div class="col-md-6 paddings"><a href="{{asset('images/projects/Delight/floorplan/10.png')}}"
                                                                  data-fancybox
                                                                  data-caption="Unit Blue Print">

                                        <img class="img-responsive center-block"
                                             src="{{asset('images/projects/Delight/floorplan/sml/10.png')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Unit Blue Print
                                    </div>
                                </div>
                                <div class="col-md-6 paddings"><a href="{{asset('images/projects/Delight/floorplan/11.png')}}"
                                                                  data-fancybox
                                                                  data-caption="Unit Blue Print">

                                        <img class="img-responsive center-block"
                                             src="{{asset('images/projects/Delight/floorplan/sml/11.png')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Unit Blue Print
                                    </div>
                                </div>
                                <div class="col-md-6 paddings"><a href="{{asset('images/projects/Delight/floorplan/12.png')}}"
                                                                   data-fancybox
                                                                   data-caption="Unit Blue Print">

                                        <img class="img-responsive center-block"
                                             src="{{asset('images/projects/Delight/floorplan/sml/12.png')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Unit Blue Print
                                    </div>
                                </div>


                                </div>


                                <div class="col-md-6 paddings"><a href="{{asset('images/projects/Delight/floorplan/13.png')}}"
                                                                  data-fancybox
                                                                  data-caption="Block Delicia">

                                        <img class="img-responsive center-block"
                                             src="{{asset('images/projects/Delight/floorplan/sml/13.png')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Block Delicia
                                    </div>
                                </div>
                                <div class="col-md-6 paddings"><a href="{{asset('images/projects/Delight/floorplan/14.png')}}"
                                                                  data-fancybox
                                                                  data-caption="Block Delicia (1st Floor)">

                                        <img class="img-responsive center-block"
                                             src="{{asset('images/projects/Delight/floorplan/sml/14.png')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Block Delicia (1st Floor)
                                    </div>
                                </div>


                                <div class="col-md-6 paddings"><a href="{{asset('images/projects/Delight/floorplan/15.png')}}"
                                                                  data-fancybox
                                                                  data-caption="Block Delicia (2nd Floor)">

                                        <img class="img-responsive center-block"
                                             src="{{asset('images/projects/Delight/floorplan/sml/15.png')}}"
                                             width="250px" alt=""/>

                                    </a>
                                    <div class="imgdown">
                                        Block Delicia (2nd Floor)
                                    </div>
                                </div>




                                @include('Projects.Delight.button')
                                @include('Parts.similar.delight')
                            </div>
                            <div class="col-md-4 col-xs-12">
                                @include('Parts.right')
                            </div>


                        </div>
                    </div>


                    <!-- END PROPERTIES ASSIGNED -->

                </div>
                <!-- END MAIN CONTENT -->


            </div>
        </div>
    </div>
@endsection




@section('page_js')
    <script src="js/freewall.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>

@stop