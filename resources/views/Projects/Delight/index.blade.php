@extends('structure')
@section('page_css')


@stop
@section('content')


    <!-- BEGIN HOME GRID -->
    <div id="home-grid">
        <div id="freewall" class="free-wall">
            <div class="item">
                <a class="info" href="#">

                    <h3>Luxury Bedroom</h3>

                </a>
                <img src="images/projects/Delight/topimg/1.jpg" alt="" />
            </div>
            <div class="item">
                <a class="info" href="#">

                    <h3>Cafeteria</h3>

                </a>
                <img src="/images/projects/Delight/topimg/2.jpg" alt="" />
            </div>
            <div class="item">
                <a class="info" href="#">

                    <h3>2 Community Halls </h3>

                </a>
                <img src="/images/projects/Delight/topimg/3.jpg" alt="" />
            </div>

            <div class="item">
                <a class="info" href="#">

                    <h3>Departmental with medical Shop</h3>

                </a>
                <img src="/images/projects/Delight/topimg/4.jpg" alt="" />
            </div>
            <div class="item">
                <a class="info" href="#">

                    <h3>Pool Room</h3>

                </a>
                <img src="/images/projects/Delight/topimg/5.jpg" alt="" />
            </div>
            <div class="item">
                <a class="info" href=#">

                    <h3> Gym</h3>

                </a>
                <img src="/images/projects/Delight/topimg/6.jpg" alt="" />
            </div>

            <div class="item">
                <a class="info" href="#">

                    <h3>Play area</h3>

                </a>
                <img src="/images/projects/Delight/topimg/7.jpg" alt="" />
            </div>
            <div class="item">
                <a class="info" href="#">

                    <h3>Kitchen</h3>

                </a>
                <img src="/images/projects/Delight/topimg/8.jpg" alt="" />
            </div>
            <div class="item">
                <a class="info" href="#">

                    <h3>Kitty Party Room</h3>

                </a>
                <img src="/images/projects/Delight/topimg/9.jpg" alt="" />
            </div>
            <div class="item">
                <a class="info" href="#">

                    <h3>Elderly Sitting Area </h3>

                </a>
                <img src="/images/projects/Delight/topimg/10.jpg" alt="" />
            </div>
            <div class="item">
                <a class="info" href="#">

                    <h3>Study Room</h3>

                </a>
                <img src="/images/projects/Delight/topimg/11.jpg" alt="" />
            </div>
            <div class="item">
                <a class="info" href="#">

                    <h3>Living room</h3>

                </a>
                <img src="/images/projects/Delight/topimg/12.jpg" alt="" />
            </div>




        </div>
    </div>
    <!-- END HOME GRID -->

    <!-- BEGIN PAGE TITLE/BREADCRUMB -->
    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Embee Delight </h1>
                    <p style="text-align: left">[ A Joint Initiative Of Embee Builders & Manakamna Builders ]</p>

                    <ul class="breadcrumb">
                        <li><a href="index.html">Home </a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="/embee-delight">Embee Delight</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE/BREADCRUMB -->


    <!-- BEGIN CONTENT WRAPPER -->
    <div class="content">
        <div class="container">
            <div class="row">

                <!-- BEGIN MAIN CONTENT -->
                <div class="main col-sm-8">

                    <h1 class="property-title">Embee Delight</h1>


                    <!-- BEGIN PROPERTY DETAIL SLIDERS WRAPPER -->
                    <div id="property-detail-wrapper" class="style1">

                        <div class="price">

                            <span>Booking Started</span>
                        </div>

                        <!-- BEGIN PROPERTY DETAIL LARGE IMAGE SLIDER -->
                        <div id="property-detail-large" class="owl-carousel">
                            <div class="item">
                                <img src="/images/projects/Delight/mainpage/1.jpg" alt="" />
                            </div>
                            <div class="item">
                                <img src="/images/projects/Delight/mainpage/2.jpg" alt="" />
                            </div>
                            <div class="item">
                                <img src="/images/projects/Delight/mainpage/3.jpg" alt="" />
                            </div>
                            <div class="item">
                                <img src="/images/projects/Delight/mainpage/4.jpg" alt="" />
                            </div>
                            <div class="item">
                                <img src="/images/projects/Delight/mainpage/5.jpg" alt="" />
                            </div>

                        </div>
                        <!-- END PROPERTY DETAIL LARGE IMAGE SLIDER -->

                        <!-- BEGIN PROPERTY DETAIL THUMBNAILS SLIDER -->
                        <div id="property-detail-thumbs" class="owl-carousel">
                            <div class="item"><img src="/images/projects/Delight/mainpage/1.jpg" alt="" /></div>
                            <div class="item"><img src="/images/projects/Delight/mainpage/2.jpg" alt="" /></div>
                            <div class="item"><img src="/images/projects/Delight/mainpage/3.jpg" alt="" /></div>
                            <div class="item"><img src="/images/projects/Delight/mainpage/4.jpg" alt="" /></div>
                            <div class="item"><img src="/images/projects/Delight/mainpage/5.jpg" alt="" /></div>
                        </div>
                        <!-- END PROPERTY DETAIL THUMBNAILS SLIDER -->

                    </div>

                    <p>
                        After a successful residential project “Embee Fortune”, Embee Builders are once again determined
                        to make a new mark in the real estate market with its new upcoming project “Embee Delight”.

                        Embee Delight will be an upgraded version of Embee Fortune, which will cater MIG
                        and Premium MIG Category. The project is a premium residential project which will be
                        having 530 flats, consisting basically 3BHK large, 3 BHK normal and 2 BHK flats.
                        The project will be providing all kinds of Luxurious amenities including Swimming Pool,
                        Squash Court, AC Community Hall and many more which will make living comforting and fun.

                        Quick Facts
                        <ul>
                        <li>Location: Shiv Mandir, Kadamtala, SIliguri</li>
                        <li>Land Mark: Beside Embee Fortune.</li>
                        <li>Total No. of Flats: 530 Nos</li>
                        <li>3BHK Large Flats, 3 BHK Normal Flats and 2 BHK Flats</li>
                        <li>Commencement: The projected is expected to begin from Feb 2018.</li></ul>



                        &nbsp;</p>





                    <h1 class="section-title">Property Features</h1>
                    <!-- BEGIN PROPERTY FEATURES LIST -->
                    <ul class="property-features col-sm-6">
                        <li><i class="icon-apartment"></i> Attached to 4-Lane Asian Highway</li>
                        <li><i class="icon-pool"></i> Swimming Pool</li>
                        <li><i class="icon-land-percent"></i> 30% Green Cover</li>
                        <li><i class="icon-land"></i> 65% Open Space</li>
                    </ul>

                    <ul class="property-features col-sm-6">
                        <li><i class="icon-garden"></i> Exclusive Club House</li>
                        <li><i class="icon-store"></i> Departmental & Medical Store</li>
                        <li><i class="icon-bedrooms"></i> Seperate Guests Rooms</li>
                        <li><i class="icon-garage"></i> Covered & Semi-Covered Parking</li>
                    </ul>
                    <!-- END PROPERTY FEATURES LIST -->




                    @include('Projects.Delight.button')
                    @include('Parts.similar.delight')

                    <h1 class="section-title">Property Location</h1>
                    <!-- PROPERTY MAP HOLDER -->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3564.4760818764526!2d88.36690134990064!3d26.697233683138432!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39e44683beb4be6b%3A0xc0965267a689c368!2sEmbee+Delight!5e0!3m2!1sen!2sin!4v1510573559612" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

                    <div class="share-wraper col-sm-12">
                        <h5>Share this Property:</h5>
                        <ul class="social-networks">
                            <li><a target="_blank"
                                   href="http://www.facebook.com/sharer.php?s=100&amp;p%5Burl%5D=http://embeebuilders.com/"><i class="fa fa-facebook"></i></a></li>
                            <li><a
                                        target="_blank"
                                        href="https://twitter.com/intent/tweet?url=http://embeebuilders.com/"><i class="fa fa-twitter"></i></a></li>
                            <li><a
                                        target="_blank"
                                        href="https://plus.google.com/share?url=http://embeebuilders.com/"><i class="fa fa-google"></i></a></li>
                            <li><a
                                        target="_blank"
                                        href="http://pinterest.com/pin/create/button/?url=http://embeebuilders.com/">
                                    <i class="fa fa-pinterest"></i></a></li>
                            <li><a
                                        href="mailto:?subject=Check%20out%20this%20property%20I%20found!&amp;body=http://embeebuilders.com/"><i class="fa fa-envelope"></i></a></li>
                        </ul>

                        <a class="print-button" href="javascript:window.print();">
                            <i class="fa fa-print"></i>
                        </a>
                    </div>






                    <!-- END PROPERTIES ASSIGNED -->

                </div>
                <!-- END MAIN CONTENT -->


@include('Parts.rightside')

            </div>
        </div>
    </div>
    <!-- END CONTENT WRAPPER -->

@endsection

@section('page_js')
    <script src="js/freewall.js"></script>
@stop